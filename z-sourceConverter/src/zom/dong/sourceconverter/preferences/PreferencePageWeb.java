package zom.dong.sourceconverter.preferences;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import zom.dong.sourceconverter.Activator;

public class PreferencePageWeb extends FieldEditorPreferencePage implements IWorkbenchPreferencePage  {
	
	public PreferencePageWeb() {
        super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("A demonstration of a preference page implementation");
	 }
	
	 public void createFieldEditors() {
		 
		 Group prefsGroup = new Group(getFieldEditorParent(), SWT.NULL);
		 prefsGroup.setText("import Class"); 
          
          //import 설정
          ListEditor extensionsEditor = new ListEditor("webImportClass", 
        		  "", prefsGroup) {
  			
  			@Override
  			protected String getNewInputObject() {
  				InputDialog dialog = new InputDialog(getShell(), 
  						"Add import Class Extension", "import Class Extension", "", null);
  				if (dialog.open() == InputDialog.OK) return dialog.getValue();
  				else return null;
  			}
  			
  			@Override
  			protected String[] parseString(String stringList) {
  				return stringList.split(";");
  			}
  			
  			@Override
  			protected String createList(String[] items) {
  				if (items.length == 0) return "";
			    StringBuffer buffer = new StringBuffer(items[0]);
			    for (int i=1; i<items.length; i++) {
			    	buffer.append(";");
			    	buffer.append(items[i]);
			    }
			    return buffer.toString();	
  			}
  		};
  		
          addField(extensionsEditor);
          
          new Label(getFieldEditorParent(), SWT.NULL).setLayoutData(new GridData(
    				SWT.FILL, SWT.NULL, true, false, 3, 1));
          
            Group miscGroup = new Group(getFieldEditorParent(), SWT.NULL);
          
          //class extends 설정
          addField(new StringFieldEditor("webClassExtends", "Class Extends:"
          		, miscGroup));
          
          new Label(getFieldEditorParent(), SWT.NULL).setLayoutData(new GridData(
  				SWT.FILL, SWT.NULL, true, false, 3, 1));
          
        //네이밍 룰
	        Group sqlGroup = new Group(getFieldEditorParent(), SWT.NULL);
	        sqlGroup.setText("web Naming Role");
	        
	        addField(new StringFieldEditor("webNameList", "목록 : ", sqlGroup));
	        addField(new StringFieldEditor("webNameGet", "객체 : ", sqlGroup));
	        addField(new StringFieldEditor("webNameCreate", "등록 : ", sqlGroup));
	        addField(new StringFieldEditor("webNameUpdate", "수정 : ", sqlGroup));
	        addField(new StringFieldEditor("webNameDelete", "삭제 : ", sqlGroup));
	        
	        doLayoutAndData(prefsGroup, 2, 300);
	        doLayoutAndData(miscGroup, 2, 300);
	        doLayoutAndData(sqlGroup, 2, 300);

	 }
	 
	 public void init(IWorkbench workbench) {}
	 
	 private void doLayoutAndData(Group group, int numColumns, int widthHint) {
			GridLayout gl = new GridLayout(numColumns, false);
			group.setLayout(gl);
			GridData gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalSpan = 1;
			gd.widthHint = widthHint;
			group.setLayoutData(gd);
		}

}
		
