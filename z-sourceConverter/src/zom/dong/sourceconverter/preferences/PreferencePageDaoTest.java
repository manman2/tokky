package zom.dong.sourceconverter.preferences;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import zom.dong.sourceconverter.Activator;

public class PreferencePageDaoTest extends FieldEditorPreferencePage implements IWorkbenchPreferencePage  {
	
	public PreferencePageDaoTest() {
        super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("A demonstration of a preference page implementation");
	 }
	
	 public void createFieldEditors() {
          
		 Group fe = new Group(getFieldEditorParent(), SWT.NULL);
		 fe.setText("import Class"); 
		 
          //import 설정
          ListEditor extensionsEditor = new ListEditor("daoTestImportClass", 
        		  "", fe) {
  			
  			@Override
  			protected String getNewInputObject() {
  				InputDialog dialog = new InputDialog(getShell(), 
  						"Add import Class Extension", "import Class Extension", "", null);
  				if (dialog.open() == InputDialog.OK) return dialog.getValue();
  				else return null;
  			}
  			
  			@Override
  			protected String[] parseString(String stringList) {
  				return stringList.split(";");
  			}
  			
  			@Override
  			protected String createList(String[] items) {
  				if (items.length == 0) return "";
			    StringBuffer buffer = new StringBuffer(items[0]);
			    for (int i=1; i<items.length; i++) {
			    	buffer.append(";");
			    	buffer.append(items[i]);
			    }
			    return buffer.toString();	
  			}
  		};
  		
          addField(extensionsEditor);
          
          new Label(getFieldEditorParent(), SWT.NULL).setLayoutData(new GridData(
    				SWT.FILL, SWT.NULL, true, false, 2, 1));
            
            Group ffGroup = new Group(getFieldEditorParent(), SWT.NULL);
            
          
          //class extends 설정
          addField(new StringFieldEditor("daoTestClassExtends", "Class Extends:"
          		, ffGroup));
          
          doLayoutAndData(fe, 2, 300);
          doLayoutAndData(ffGroup, 2, 300);
	 }
	 
	 public void init(IWorkbench workbench) {}

	 private void doLayoutAndData(Group group, int numColumns, int widthHint) {
			GridLayout gl = new GridLayout(numColumns, false);
			group.setLayout(gl);
			GridData gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalSpan = 1;
			gd.widthHint = widthHint;
			group.setLayoutData(gd);
		}

}
		
