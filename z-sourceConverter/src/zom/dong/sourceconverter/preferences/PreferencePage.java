package zom.dong.sourceconverter.preferences;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.jdom.Element;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.util.UtilUnit;

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage  {
	
	
	
	public PreferencePage() {
        super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("경로는 slqxml 기준으로 적어야함. \n 파일은 모댈 뒤에 붙는 네이밍만 적어주세요.\n 문의 : loverfairy@gmail.com");
	 }
	
	 public void createFieldEditors() {
		 
		 Group topGroup = new Group(getFieldEditorParent(), SWT.NULL);
         
		 Group pv = new Group(topGroup, SWT.SHADOW_ETCHED_IN);
		 pv.setText("경로");
		 Group sv = new Group(topGroup, SWT.SHADOW_OUT);
		 sv.setText("파일이름");
		 
		 doLayoutAndData(pv, 2, 100);
		 doLayoutAndData(sv, 2, 100);
		 
		 //경로
          addField(new StringFieldEditor("daoPath", "daoPath : ", pv));
          
          addField(new StringFieldEditor("daoImplPath", "daoImplPath : ", pv));
          
          addField(new StringFieldEditor("servicePath", "servicePath : ", pv));
          
          addField(new StringFieldEditor("serviceImplPath", "serviceImplPath : ", pv));
          
          addField(new StringFieldEditor("modelPath", "modelPath : ", pv));
          
          addField(new StringFieldEditor("webPath", "webPath : ", pv));
          
          addField(new StringFieldEditor("daoTestPath", "testDaoPath : ", pv));
          
          addField(new StringFieldEditor("serviceTestPath", "testServicePath : ", pv));
          
          //파일이름
          addField(new StringFieldEditor("daoFile", "daoFile : ", sv));
          
          addField(new StringFieldEditor("daoImplFile", "daoImplFile : ", sv));
          
          addField(new StringFieldEditor("serviceFile", "serviceFile : ", sv));
          
          addField(new StringFieldEditor("serviceImplFile", "serviceImplFile : ", sv));
          
          addField(new StringFieldEditor("modelFile", "modelFile : ", sv));
          
          addField(new StringFieldEditor("webFile", "webFile : ", sv));
          
          addField(new StringFieldEditor("daoTestFile", "daoTestFile : ", sv));
          
          addField(new StringFieldEditor("serviceTestFile", "serviceTestFile : ", sv));
          
          new Label(getFieldEditorParent(), SWT.NULL).setLayoutData(new GridData(
  				SWT.FILL, SWT.NULL, true, false, 4, 1));
          
          Group ffGroup = new Group(getFieldEditorParent(), SWT.NULL);
          
          FileFieldEditor ff = new FileFieldEditor("preferenceFile",
  				"Import preference File:", ffGroup);
  		  ff.setFileExtensions(new String[] { "*.xml"});	
		
  		  addField(ff);
          
          new Label(getFieldEditorParent(), SWT.NULL).setLayoutData(new GridData(
  				SWT.FILL, SWT.NULL, true, false, 6, 1));
          
          Group buGroup = new Group(getFieldEditorParent(), SWT.NULL);
          
          Button text = new Button(buGroup, SWT.WRAP);
          text.setText("Export preference File");

          text.addSelectionListener(new SelectionAdapter() {
        	  
              @Override
              public void widgetSelected(SelectionEvent event) {
                  
                  FileDialog dialog = new FileDialog(getFieldEditorParent().getShell(), SWT.OPEN);
                  dialog.setFilterExtensions(new String[] { "*.xml"});
                  dialog.setFileName("preference.xml");
                  dialog.setOverwrite(false);
                  String fileName = dialog.open();
                  if (fileName != null) {
                	  
                	  try {
	                	  if(fileName.indexOf(".xml") == -1){
	                		  fileName += ".xml";
	                	  }
	                	  File file = new File(fileName);
	                	  
	                	  BufferedWriter bw = new BufferedWriter(new FileWriter(file), 1024);
	              		
		      				// 타이틀을 파일에 저장 합니다.
		      				bw.write(getPreferenceXml().toString());
		      		
		      				bw.close();
	      				
                	  } catch (IOException e) {
  						// TODO Auto-generated catch block
  						e.printStackTrace();
  					}
                  }
              }
          });
          
          doLayoutAndData(topGroup,4, 300);
          doLayoutAndData(ffGroup, 3, 300);
          doLayoutAndData(buGroup, 1, 300);
          
          
	 }
	 
	 public void init(IWorkbench workbench) {}
	 
	 private void doLayoutAndData(Group group, int numColumns, int widthHint) {
			GridLayout gl = new GridLayout(numColumns, false);
			group.setLayout(gl);
			GridData gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalSpan = 1;
			gd.widthHint = widthHint;
			group.setLayoutData(gd);
		}

	 
	 @Override
	 public void performApply() {
		 
		 super.performApply();
		 savePerferenceXml();
		 
		 initialize();
        
     }
	 
	 @Override
	 public boolean performOk() {
		 
		 super.performOk();
		 savePerferenceXml();
        
		 return true;
     }
	 
	 /**
	  * xml perference 저장
	  */
	 private void savePerferenceXml(){
		 
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
    	  
        String preferenceFile= store.getString("preferenceFile").trim();
        
        if(StringUtils.isNotEmpty(preferenceFile)){
    		try {
				Element root =  UtilUnit.getXml(new File(preferenceFile));
				@SuppressWarnings("unchecked")
				List<Element> children = root.getChildren();
				
				 for(Element element : children){
					 
					 @SuppressWarnings("unchecked")
					List<Element> childrenList =  element.getChildren();
					 
					 for(Element elementChild : childrenList){
						 if (elementChild.getValue() != null){
							 store.setValue(elementChild.getName(), elementChild.getValue());
						 }
					 }
					 
				 }
			} catch (Exception e) {
				e.printStackTrace();
			}
    		
        }
	 }
	 
	 private StringBuffer getPreferenceXml(){
		 
		 StringBuffer sb = new StringBuffer();
		
		 IPreferenceStore store = Activator.getDefault().getPreferenceStore();
   	  
   	  	 String daoPath= store.getString("daoPath").trim();
	   	 String daoImplPath= store.getString("daoImplPath").trim();
	   	String servicePath= store.getString("servicePath").trim();
	   	String serviceImplPath= store.getString("serviceImplPath").trim();
	   	String modelPath= store.getString("modelPath").trim();
	   	String webPath= store.getString("webPath").trim();
	   	String daoTestPath= store.getString("daoTestPath").trim();
	   	String serviceTestPath= store.getString("serviceTestPath").trim();
	   	
	   	String daoFile= store.getString("daoFile").trim();
	   	String daoImplFile= store.getString("daoImplFile").trim();
	   	String serviceFile= store.getString("serviceFile").trim();
	   	String serviceImplFile= store.getString("serviceImplFile").trim();
	   	String webFile= store.getString("webFile").trim();
	   	String daoTestFile= store.getString("daoTestFile").trim();
	   	String serviceTestFile= store.getString("serviceTestFile").trim();
	   	
	   	String daoImplImportClass= store.getString("daoImplImportClass").trim();
	   	String daoImportClass= store.getString("daoImportClass").trim();
	   	String daoTestImportClass= store.getString("daoTestImportClass").trim();
	   	String serviceImplImportClass= store.getString("serviceImplImportClass").trim();
	   	String serviceImportClass= store.getString("serviceImportClass").trim();
	   	String serviceTestImportClass= store.getString("serviceTestImportClass").trim();
	   	String webImportClass= store.getString("webImportClass").trim();
	   	
	   	String daoImplClassExtends= store.getString("daoImplClassExtends").trim();
	   	String daoClassExtends= store.getString("daoClassExtends").trim();
	   	String daoTestClassExtends= store.getString("daoTestClassExtends").trim();
	   	String serviceImplClassExtends= store.getString("serviceImplClassExtends").trim();
	   	String serviceClassExtends= store.getString("serviceClassExtends").trim();
	   	String serviceTestClassExtends= store.getString("serviceTestClassExtends").trim();
	   	String webClassExtends= store.getString("webClassExtends").trim();
	   	
	   	String daoImplSqlInsert= store.getString("daoImplSqlInsert").trim();
	   	String daoImplSqlUpdate= store.getString("daoImplSqlUpdate").trim();
	   	String daoImplSqlDelete= store.getString("daoImplSqlDelete").trim();
	   	String daoImplSqlSelectForList= store.getString("daoImplSqlSelectForList").trim();
	   	String daoImplSqlSelectForListOfObject= store.getString("daoImplSqlSelectForListOfObject").trim();
	   	String daoImplSqlSelectForObject= store.getString("daoImplSqlSelectForObject").trim();
	   	
	   	String sqlNameList= store.getString("sqlNameList").trim();
	   	String sqlNameGet= store.getString("sqlNameGet").trim();
	   	String sqlNameCreate= store.getString("sqlNameCreate").trim();
	   	String sqlNameUpdate= store.getString("sqlNameUpdate").trim();
	   	String sqlNameDelete= store.getString("sqlNameDelete").trim();
	   	String sqlNameCount= store.getString("sqlNameCount").trim();
	   	String sqlNameExists= store.getString("sqlNameExists").trim();
	   	
	   	String daoNameList= store.getString("daoNameList").trim();
	   	String daoNameGet= store.getString("daoNameGet").trim();
	   	String daoNameCreate= store.getString("daoNameCreate").trim();
	   	String daoNameUpdate= store.getString("daoNameUpdate").trim();
	   	String daoNameDelete= store.getString("daoNameDelete").trim();
	   	String daoNameCount= store.getString("daoNameCount").trim();
	   	
	   	String serviceNameList= store.getString("serviceNameList").trim();
	   	String serviceNameGet= store.getString("serviceNameGet").trim();
	   	String serviceNameCreate= store.getString("serviceNameCreate").trim();
	   	String serviceNameUpdate= store.getString("serviceNameUpdate").trim();
	   	String serviceNameDelete= store.getString("serviceNameDelete").trim();
	   	String serviceNameCount= store.getString("serviceNameCount").trim();
	   	
	   	String webNameList= store.getString("webNameList").trim();
		String webNameGet= store.getString("webNameGet").trim();
		String webNameCreate= store.getString("webNameCreate").trim();
		String webNameUpdate= store.getString("webNameUpdate").trim();
		String webNameDelete= store.getString("webNameDelete").trim();
		String webNameCount= store.getString("webNameCount").trim();
		 
		 sb.append("<xml>\n");
   	  	 sb.append("<default>\n");
   	  	 sb.append("	<daoPath><![CDATA["+daoPath+"]]></daoPath>\n");
   	  	 sb.append("	<daoImplPath><![CDATA["+daoImplPath+"]]></daoImplPath>\n");
   	 	 sb.append("	<daoTestPath><![CDATA["+daoTestPath+"]]></daoTestPath>\n");
	   	 sb.append("	<servicePath><![CDATA["+servicePath+"]]></servicePath>\n");
	   	 sb.append("	<serviceImplPath><![CDATA["+serviceImplPath+"]]></serviceImplPath>\n");
	   	 sb.append("	<serviceTestPath><![CDATA["+serviceTestPath+"]]></serviceTestPath>\n");
	   	 sb.append("	<modelPath><![CDATA["+modelPath+"]]></modelPath>\n");
	   	 sb.append("	<webPath><![CDATA["+webPath+"]]></webPath>\n");
	   	 
	   	 sb.append("	<daoFile>"+daoFile+"</daoFile>\n");
	   	 sb.append("	<daoImplFile>"+daoImplFile+"</daoImplFile>\n");
	   	 sb.append("	<serviceFile>"+serviceFile+"</serviceFile>\n");
	   	 sb.append("	<serviceImplFile>"+serviceImplFile+"</serviceImplFile>\n");
	   	 sb.append("	<webFile>"+webFile+"</webFile>\n");
	   	 sb.append("	<daoTestFile>"+daoTestFile+"</daoTestFile>\n");
	   	 sb.append("	<serviceTestFile>"+serviceTestFile+"</serviceTestFile>\n");
	   	 
   	  	 sb.append("</default>\n");
   	  	 
   	  	 sb.append("<sqlXml>\n");
   	  	 sb.append("	<sqlNameList>"+sqlNameList+"</sqlNameList>\n");
	   	 sb.append("	<sqlNameGet>"+sqlNameGet+"</sqlNameGet>\n");
	   	 sb.append("	<sqlNameCreate>"+sqlNameCreate+"</sqlNameCreate>\n");
	   	 sb.append("	<sqlNameUpdate>"+sqlNameUpdate+"</sqlNameUpdate>\n");
	   	 sb.append("	<sqlNameDelete>"+sqlNameDelete+"</sqlNameDelete>\n");
	   	 sb.append("	<sqlNameCount>"+sqlNameCount+"</sqlNameCount>\n");
	   	 sb.append("	<sqlNameExists>"+sqlNameExists+"</sqlNameExists>\n");
	  	 sb.append("</sqlXml>\n");
	  	 
   	  	 sb.append("<dao>\n");
   	  	 sb.append("	<daoImportClass>"+daoImportClass+"</daoImportClass>\n");
   	  	 sb.append("	<daoClassExtends><![CDATA["+daoClassExtends+"]]></daoClassExtends>\n");
   	  	 
	   	 sb.append("	<daoNameList>"+daoNameList+"</daoNameList>\n");
	   	 sb.append("	<daoNameGet>"+daoNameGet+"</daoNameGet>\n");
	   	 sb.append("	<daoNameCreate>"+daoNameCreate+"</daoNameCreate>\n");
	   	 sb.append("	<daoNameUpdate>"+daoNameUpdate+"</daoNameUpdate>\n");
	   	 sb.append("	<daoNameDelete>"+daoNameDelete+"</daoNameDelete>\n");
	   	 sb.append("	<daoNameCount>"+daoNameCount+"</daoNameCount>\n");
   	  	 sb.append("</dao>\n");
   	  	 
   	  	 sb.append("<daoImpl>\n");
   	  	 sb.append("	<daoImplImportClass>"+daoImplImportClass+"</daoImplImportClass>\n");
	  	 sb.append("	<daoImplClassExtends><![CDATA["+daoImplClassExtends+"]]></daoImplClassExtends>\n");
	  	 
	   	 sb.append("	<daoImplSqlInsert>"+daoImplSqlInsert+"</daoImplSqlInsert>\n");
	   	 sb.append("	<daoImplSqlUpdate>"+daoImplSqlUpdate+"</daoImplSqlUpdate>\n");
	   	 sb.append("	<daoImplSqlDelete>"+daoImplSqlDelete+"</daoImplSqlDelete>\n");
	   	 sb.append("	<daoImplSqlSelectForList>"+daoImplSqlSelectForList+"</daoImplSqlSelectForList>\n");
	   	 sb.append("	<daoImplSqlSelectForListOfObject>"+daoImplSqlSelectForListOfObject+"</daoImplSqlSelectForListOfObject>\n");
	   	 sb.append("	<daoImplSqlSelectForObject>"+daoImplSqlSelectForObject+"</daoImplSqlSelectForObject>\n");
	  	 sb.append("</daoImpl>\n");
	  	 
	  	 sb.append("<daoTest>\n");
   	  	 sb.append("	<daoTestImportClass>"+daoTestImportClass+"</daoTestImportClass>\n");
	  	 sb.append("	<daoTestClassExtends><![CDATA["+daoTestClassExtends+"]]></daoTestClassExtends>\n");
	  	 sb.append("</daoTest>\n");
	  	 
	  	 sb.append("<service>\n");
	  	 sb.append("	<serviceImportClass>"+serviceImportClass+"</serviceImportClass>\n");
   	  	 sb.append("	<serviceClassExtends><![CDATA["+serviceClassExtends+"]]></serviceClassExtends>\n");
   	  	 
	   	 sb.append("	<serviceNameList>"+serviceNameList+"</serviceNameList>\n");
	   	 sb.append("	<serviceNameGet>"+serviceNameGet+"</serviceNameGet>\n");
	   	 sb.append("	<serviceNameCreate>"+serviceNameCreate+"</serviceNameCreate>\n");
	   	 sb.append("	<serviceNameUpdate>"+serviceNameUpdate+"</serviceNameUpdate>\n");
	   	 sb.append("	<serviceNameDelete>"+serviceNameDelete+"</serviceNameDelete>\n");
	   	 sb.append("	<serviceNameCount>"+serviceNameCount+"</serviceNameCount>\n");
	  	 sb.append("</service>\n");
	  	 
	  	 sb.append("<serviceImpl>\n");
   	  	 sb.append("	<serviceImplImportClass>"+serviceImplImportClass+"</serviceImplImportClass>\n");
	  	 sb.append("	<serviceImplClassExtends><![CDATA["+serviceImplClassExtends+"]]></serviceImplClassExtends>\n");
	  	 sb.append("</serviceImpl>\n");
	  	 
	  	 sb.append("<serviceTest>\n");
   	  	 sb.append("	<serviceTestImportClass>"+serviceTestImportClass+"</serviceTestImportClass>\n");
	  	 sb.append("	<serviceTestClassExtends><![CDATA["+serviceTestClassExtends+"]]></serviceTestClassExtends>\n");
	  	 sb.append("</serviceTest>\n");
	  	 
	  	 sb.append("<web>\n");
   	  	 sb.append("	<webImportClass>"+webImportClass+"</webImportClass>\n");
	  	 sb.append("	<webClassExtends><![CDATA["+webClassExtends+"]]></webClassExtends>\n");
	  	 
	  	 sb.append("	<webNameList>"+webNameList+"</webNameList>\n");
	   	 sb.append("	<webNameGet>"+webNameGet+"</webNameGet>\n");
	   	 sb.append("	<webNameCreate>"+webNameCreate+"</webNameCreate>\n");
	   	 sb.append("	<webNameUpdate>"+webNameUpdate+"</webNameUpdate>\n");
	   	 sb.append("	<webNameDelete>"+webNameDelete+"</webNameDelete>\n");
	   	 sb.append("	<webNameCount>"+webNameCount+"</webNameCount>\n");
	  	 sb.append("</web>\n");
   	  	 
   	  	 sb.append("</xml>\n");
   	  	 
		 return sb;
		 
	 }

}
		
