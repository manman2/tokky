package zom.dong.sourceconverter.preferences;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;

public class PreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	List<String> keyList = new ArrayList<String>();
	IPreferenceStore store = Activator.getDefault().getPreferenceStore();
	
	@Override
	public void initializeDefaultPreferences() {
		
		
		setDefault("daoPath", "../../../../dao");
		setDefault("daoImplPath", "../../../../dao/impl");
		setDefault("servicePath", "../../../../service");
		setDefault("serviceImplPath", "../../../../service/impl");
		setDefault("modelPath", "../../../../model");
		setDefault("webPath", "../../../../web");
		setDefault("daoTestPath", "../../../../../../test/dao");
		setDefault("serviceTestPath", "../../../../../../test/Service");
		
		setDefault("daoFile", "Dao");
		setDefault("daoImplFile", "DaoImpl");
		setDefault("serviceFile", "Service");
		setDefault("serviceImplFile", "ServiceImpl");
		setDefault("webFile", "Controll");
		setDefault("daoTestFile", "DaoTestCase");
		setDefault("serviceTestFile", "ServiceTestCase");
		
		setDefault("daoImplImportClass", "java.util.HashMap;java.util.List;java.util.Map;java.util.Date;org.springframework.stereotype.Repository");
		setDefault("daoImportClass", "java.util.HashMap;java.util.List;java.util.Map;java.util.Date;");
		setDefault("daoTestImportClass", "static org.junit.Assert.*;org.junit.Before;org.junit.Test;org.springframework.beans.factory.annotation.Autowired;java.util.Date;java.util.HashMap;java.util.List;java.util.Map;");
		setDefault("serviceImplImportClass", "java.util.HashMap;java.util.List;java.util.ArrayList;java.util.Map;java.util.Date;org.springframework.beans.factory.annotation.Autowired;org.springframework.stereotype.Service");
		setDefault("serviceImportClass", "java.util.HashMap;java.util.List;java.util.Map;java.util.Date;org.springframework.transaction.annotation.Transactional;");
		setDefault("serviceTestImportClass", "static org.junit.Assert.*;org.junit.Before;org.junit.Test;org.springframework.beans.factory.annotation.Autowired;java.util.Date;java.util.HashMap;java.util.List;java.util.Map;");
		
		String webClass = "java.util.Map;java.util.HashMap;java.util.List;java.util.ArrayList;java.util.Properties;";
			webClass += "org.springframework.beans.factory.annotation.Autowired;org.springframework.stereotype.Controller;org.springframework.ui.ModelMap;";
			webClass += "org.springframework.validation.BindingResult;org.springframework.web.bind.annotation.ModelAttribute;org.springframework.web.bind.annotation.RequestMapping;";
			webClass += "org.springframework.web.bind.annotation.RequestMethod;org.springframework.web.bind.annotation.RequestParam;org.springframework.web.bind.annotation.ResponseBody;";
			webClass += "org.springframework.web.bind.annotation.ResponseStatus;org.springframework.web.bind.annotation.SessionAttributes;";
			webClass += "org.springframework.web.bind.support.SessionStatus;org.springframework.web.servlet.ModelAndView;org.apache.commons.lang.StringUtils;java.util.Date;";
		
		setDefault("webImportClass", webClass);
		
		//class extends
		setDefault("daoImplClassExtends", "GenericDaoSqlmap<{model}, String>");
		setDefault("daoClassExtends", "GenericDao<{model}, String>");
		setDefault("daoTestClassExtends", "BaseDaoTestCase");
		setDefault("serviceImplClassExtends", "GenericServiceImpl<{model}, String>");
		setDefault("serviceClassExtends", "GenericService<{model}, String>");
		setDefault("serviceTestClassExtends", "BaseServiceTestCase");
		setDefault("webClassExtends", "BaseController");
		
		//dao impl sql method
		setDefault("daoImplSqlInsert", "sqlInsert");
		setDefault("daoImplSqlUpdate", "sqlUpdate");
		setDefault("daoImplSqlDelete", "sqlDelete");
		setDefault("daoImplSqlSelectForList", "sqlSelectForList");
		setDefault("daoImplSqlSelectForListOfObject", "sqlSelectForListOfObject");
		setDefault("daoImplSqlSelectForObject", "sqlSelectForObject");
		
		//네이밍 룰
		setDefault("sqlNameList", "list");
		setDefault("sqlNameGet", "get");
		setDefault("sqlNameCreate", "create");
		setDefault("sqlNameUpdate", "update");
		setDefault("sqlNameDelete", "remove");
		setDefault("sqlNameCount", "count");
		setDefault("sqlNameExists", "exists");
		
		setDefault("daoNameList", "list");
		setDefault("daoNameGet", "get");
		setDefault("daoNameCreate", "create");
		setDefault("daoNameUpdate", "update");
		setDefault("daoNameDelete", "remove");
		setDefault("daoNameCount", "count");
		
		setDefault("serviceNameList", "list");
		setDefault("serviceNameGet", "get");
		setDefault("serviceNameCreate", "create");
		setDefault("serviceNameUpdate", "update");
		setDefault("serviceNameDelete", "remove");
		setDefault("serviceNameCount", "count");
		
		setDefault("webNameList", "list");
		setDefault("webNameGet", "get");
		setDefault("webNameCreate", "create");
		setDefault("webNameUpdate", "update");
		setDefault("webNameDelete", "remove");
		setDefault("webNameCount", "count");
		
		store.setDefault("keyList", StringUtils.join(keyList, ";"));
		
	}
	
	
	private void setDefault(String key, String value){
		
		
		store.setDefault(key, value);
		keyList.add(key);
	}
}
