package zom.dong.sourceconverter.preferences;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import zom.dong.sourceconverter.Activator;

public class PreferencePageSqlXml extends FieldEditorPreferencePage implements IWorkbenchPreferencePage  {
	
	public PreferencePageSqlXml() {
        super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("A demonstration of a preference page implementation");
	 }
	
	 public void createFieldEditors() {
		 
          //네이밍 룰
		 Group sqlGroup = new Group(getFieldEditorParent(), SWT.NULL);
         sqlGroup.setText("sql Naming Role");
           
         addField(new StringFieldEditor("sqlNameList", "목록 : ", sqlGroup));
         addField(new StringFieldEditor("sqlNameGet", "객체 : ", sqlGroup));
         addField(new StringFieldEditor("sqlNameCreate", "등록 : ", sqlGroup));
         addField(new StringFieldEditor("sqlNameUpdate", "수정 : ", sqlGroup));
         addField(new StringFieldEditor("sqlNameDelete", "삭제 : ", sqlGroup));
         addField(new StringFieldEditor("sqlNameExists", "있는지 : ", sqlGroup));
	        
        doLayoutAndData(sqlGroup, 2, 300);
        
	 }
	 
	 public void init(IWorkbench workbench) {}

	 private void doLayoutAndData(Group group, int numColumns, int widthHint) {
		GridLayout gl = new GridLayout(numColumns, false);
		group.setLayout(gl);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 1;
		gd.widthHint = widthHint;
		group.setLayoutData(gd);
	}
}
		
