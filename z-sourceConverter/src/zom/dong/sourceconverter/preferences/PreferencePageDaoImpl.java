package zom.dong.sourceconverter.preferences;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import zom.dong.sourceconverter.Activator;

public class PreferencePageDaoImpl extends FieldEditorPreferencePage implements IWorkbenchPreferencePage  {
	
	public PreferencePageDaoImpl() {
        super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("A demonstration of a preference page implementation");
	 }
	
	 public void createFieldEditors() {
          
		 /*
		 addField(new DirectoryFieldEditor("PATH",
                             "&Directory preference:", getFieldEditorParent()));
          
          addField(new BooleanFieldEditor(
                    		"BOOLEAN_VALUE",
                             "An example of a boolean preference",
                             getFieldEditorParent()));


          addField(new RadioGroupFieldEditor(
        		  		"CHOICE",
	                    "An example of a multiple-choice preference",
	                    1,
	                    new String[][] { { "Choice 1", "choice1" }, {
	                             "Choice 2", "choice2" }
                         }, getFieldEditorParent()));
			*/

		// rl.justify = true;
		 //pv.setLayout(rl);
		 //sv.setLayout(rl);
		 
		 
		 Group prefsGroup = new Group(getFieldEditorParent(), SWT.NULL);
		 prefsGroup.setText("import Class"); 
		 
          //import 설정
          ListEditor extensionsEditor = new ListEditor("daoImplImportClass", 
        		  "", prefsGroup) {
  			
  			@Override
  			protected String getNewInputObject() {
  				InputDialog dialog = new InputDialog(getShell(), 
  						"Add import Class Extension", "import Class Extension", "", null);
  				if (dialog.open() == InputDialog.OK) return dialog.getValue();
  				else return null;
  			}
  			
  			@Override
  			protected String[] parseString(String stringList) {
  				return stringList.split(";");
  			}
  			
  			@Override
  			protected String createList(String[] items) {
  				if (items.length == 0) return "";
			    StringBuffer buffer = new StringBuffer(items[0]);
			    for (int i=1; i<items.length; i++) {
			    	buffer.append(";");
			    	buffer.append(items[i]);
			    }
			    return buffer.toString();	
  			}
  		};
  		
  				
          addField(extensionsEditor);
          
          new Label(getFieldEditorParent(), SWT.NULL).setLayoutData(new GridData(
    				SWT.FILL, SWT.NULL, true, false, 3, 1));
          
          Group miscGroup = new Group(getFieldEditorParent(), SWT.NULL);
          
          //class extends 설정
          addField(new StringFieldEditor("daoImplClassExtends", "Class Extends : "
          		, miscGroup));
          
          new Label(getFieldEditorParent(), SWT.NULL).setLayoutData(new GridData(
  				SWT.FILL, SWT.NULL, true, false, 3, 1));
          
          Group sqlGroup = new Group(getFieldEditorParent(), SWT.NULL);
          sqlGroup.setText("dao SQl method");
          
          addField(new StringFieldEditor("daoImplSqlInsert", "sqlInsert:"
            		, sqlGroup));
          
          addField(new StringFieldEditor("daoImplSqlUpdate", "sqlUpdate:"
          		, sqlGroup));
          
          addField(new StringFieldEditor("daoImplSqlDelete", "sqlDelete:"
            		, sqlGroup));

          addField(new StringFieldEditor("daoImplSqlSelectForList", "sqlSelectForList:"
          		, sqlGroup));
          
          addField(new StringFieldEditor("daoImplSqlSelectForObject", "sqlSelectForObject:"
            		, sqlGroup));
          
          doLayoutAndData(prefsGroup, 2, 300);
          doLayoutAndData(miscGroup, 2, 300);
          doLayoutAndData(sqlGroup, 2, 300);
	 }
	 
	 public void init(IWorkbench workbench) {}
	 
	private void doLayoutAndData(Group group, int numColumns, int widthHint) {
		GridLayout gl = new GridLayout(numColumns, false);
		group.setLayout(gl);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 1;
		gd.widthHint = widthHint;
		group.setLayoutData(gd);
	}
	
}
		
