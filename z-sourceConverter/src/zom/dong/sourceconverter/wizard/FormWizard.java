package zom.dong.sourceconverter.wizard;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class FormWizard extends Wizard {

	protected FormPageOne one;
	protected FormPageTwo two;
	protected IFile handlerChainFile;
	protected GetSourceInfo sourceInfo;
	ParseSqlXml sqlXml; 
	List<SqlMapVo> newMethodList = new ArrayList<SqlMapVo>();		//dao에 없는 생성할 메소드명
	private List<WizardResultModel> checkList = new ArrayList<WizardResultModel>();
	private String selectedText;
	  
	public FormWizard(){
		//IDialogSettings fs = FavoritesPlugin.
		
	}
	
	public void init(GetSourceInfo sourceInfo, IFile handlerChainFile, String selectedText){
		
		
		this.handlerChainFile = handlerChainFile;
		this.sourceInfo = sourceInfo;
		this.selectedText = selectedText;
		
		setNeedsProgressMonitor(true);
	}
	
	public void addPages(){
		
		one = new FormPageOne(sourceInfo);
		two = new FormPageTwo();
		
	    addPage(one);
	    addPage(two);

	}
	
	public boolean performFinish() {
		
		Shell shell = two.getShell();
		
		Map<String, String> selectedMap = two.setChecked();
		
		boolean isDul = false;
		String isDulName = "";
		checkList.clear();
		
		for(String keyVal : selectedMap.keySet()){
			
			if(keyVal.lastIndexOf("-0") == -1){continue;}
			
			String nameVal = selectedMap.get(keyVal);
			
			WizardResultModel resultModel = new WizardResultModel();
			
			String type = "";
			boolean isPaging = false;
			boolean isFile = false;
			boolean isEditer = false;
			boolean isIdAuto = false;
			boolean isParamObject = false;
			
			if(selectedMap.containsKey(nameVal+"-1")){
				type = "create";
			} else if(selectedMap.containsKey(nameVal+"-2")){
				type = "update";
			} else if(selectedMap.containsKey(nameVal+"-3")){
				type = "delete";
			} else if(selectedMap.containsKey(nameVal+"-4")){
				type = "get";
			} else if(selectedMap.containsKey(nameVal+"-5")){
				type = "exists";
			} else if(selectedMap.containsKey(nameVal+"-6")){
				type = "count";
			} else if(selectedMap.containsKey(nameVal+"-7")){
				type = "list";
			} 
			
			if(selectedMap.containsKey(nameVal+"-8")){
				isIdAuto = true;
			} 
			if(selectedMap.containsKey(nameVal+"-9")){
				isPaging = true;
			}
			if(selectedMap.containsKey(nameVal+"-10")){
				isFile = true;
			}
			if(selectedMap.containsKey(nameVal+"-11")){
				isEditer = true;
			}
			if(selectedMap.containsKey(nameVal+"-12")){
				isParamObject = true;
			}
			
			if(StringUtils.isEmpty(type)){
				isDul = true;
				isDulName= nameVal;
				break;
			}
			
			resultModel.name = nameVal;
			resultModel.type = type;
			resultModel.isPaging = isPaging;
			resultModel.isFile = isFile;
			resultModel.isEditer = isEditer;
			resultModel.isIdAuto = isIdAuto;
			resultModel.isParamObject = isParamObject;
			
			 checkList.add(resultModel);
			
		}
		
		
		if(isDul){
			 MessageDialog.openInformation(shell, "Information", "("+isDulName+") : create, update, delete, get, exists, count, list 중 하나를 선택해 주십시오.");
				return false;
		}
		
		if(checkList.size() == 0){
			 MessageDialog.openInformation(shell, "Information", "name을 하나이상 선택해 주십시오.");
			return false;
		}
	
		
		try {
			getContainer().run(true, true, new IRunnableWithProgress(){
				public void run(IProgressMonitor monitor) throws InterruptedException {
					//performOperation(selected, monitor);
				}
			});
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		
		return true;
	}
	
	private void performOperation(FormModel[] selected, IProgressMonitor monitor) throws InterruptedException{
		
		//ExtractedString[] extracted = 
		/*
		 monitor.beginTask("Extracting Strings", selected.length);
	      for (int i = 0; i < selected.length; i++) {
	         // Replace sleep with actual work
	         Thread.sleep(1000);
	         if (monitor.isCanceled())
	            throw new InterruptedException("Canceled by user");
	         monitor.worked(1);
	      }
	      monitor.done();
	      */
	}
	
	public void setFileSelectList(){
		one.setSelectList();
	}
	
	public List<Integer> getFileSelectList(){
		return one.getSelectList();
	}
	
	public void parseSqlXml(){
		
		one.saveInput();
		
		try {
			sqlXml = new ParseSqlXml(handlerChainFile, one.getSourceInfo().getModelPath());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
				
	}
	
	public ParseSqlXml getSqlXml(){
		return this.sqlXml;
	}
	
	public void convertNewMethod(){
		List<String> methodList = UtilUnit.getMethodJava(sourceInfo.getDaoImplPath()+"/"+sourceInfo.getDaoImplFile()+".java");		
		newMethodList.clear();
		
		//dao에 없는 sql ID만 표시
		for(SqlMapVo sqlMapVo : sqlXml.getVoList()){
			
			if(!methodList.contains(sqlMapVo.getId())){
				newMethodList.add(sqlMapVo);
			}
		}
	}
	
	public List<SqlMapVo> getNewMethodList(){
		
		return this.newMethodList;
	}
	
	/**
	 * 마법사 2번째 페이지에서 체크된 목록 
	 * @return
	 */
	public List<WizardResultModel> getCheckList(){
		return this.checkList;
	}
	
	public String getSelectedText(){
		return this.selectedText;
	}
	
	

}
