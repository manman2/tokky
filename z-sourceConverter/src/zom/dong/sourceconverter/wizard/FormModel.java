package zom.dong.sourceconverter.wizard;

import java.util.ArrayList;
import java.util.List;

public class FormModel {
	
	public String name;
	public FormModel[] children = new FormModel[0];
	public FormModel parent = null;
	private List<FormModel> modeles= new ArrayList<FormModel>();
	
	FormModel(){
		modeles= new ArrayList<FormModel>();
	}
	
	public FormModel(String name){
		this.name = name;
	}

	public void addModel(FormModel formModel){
		modeles.add(formModel);
	}
	
	public List<FormModel> getFormModeles(){
		return this.modeles;
	}
	
	

}
