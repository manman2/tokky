package zom.dong.sourceconverter.wizard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;

import org.eclipse.swt.SWT;

import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.events.KeyListener;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;

import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.swt.widgets.Label;

import org.eclipse.swt.widgets.Text;

import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class FormPageTwoNew extends WizardPage {

	private Button[] buttons;
	private List<String> checkList = new ArrayList<String>();
	private List<SqlMapVo> daoMethodList = new ArrayList<SqlMapVo>();
	private Table table;
	private CheckboxTableViewer checkboxTableViewer;
	private Composite container;
	
	public void setDaoMethodList(List<SqlMapVo> daoMethodList){
		this.daoMethodList = daoMethodList;
	}
	
	public FormPageTwoNew() {

		super("Check Source");
		setTitle("Check Source");
		setDescription("생성할 소스 체크해 주십시오.");
		//setControl(text1);

	}
	
	public void setVisible(boolean visible){
		
		if(visible){
			
			FormWizard formWizard = (FormWizard)getWizard();
			
			formWizard.setFileSelectList();
			
			formWizard.parseSqlXml();
			
			formWizard.convertNewMethod();
			
			this.daoMethodList = formWizard.getNewMethodList();
			
			if(this.daoMethodList.size() < 1){
				setMessage("추가된 메소드가 없습니다.");
				setPageComplete(false);
			} else {
				
				FormModel formModel= new FormModel();
				int i = 0;
				Integer selectedIdx = null;
				for(SqlMapVo method : daoMethodList ){
					
					String id = method.getId();
					formModel.addModel(new FormModel(id));
					
					if(id.equals(formWizard.getSelectedText())){
						selectedIdx = i;
					}
					
					i++;
				}
				
				checkboxTableViewer.setInput(formModel.getFormModeles());
				
				checkedInput(selectedIdx);
				
				
				setPageComplete(true);
			}
			
		} else {
			setPageComplete(false);
		}
		
		super.setVisible(visible);
		
	}
	
	
	public void createControl(Composite parent) {
		
		  container = new Composite(parent, SWT.NULL);
	      container.setLayout(new FormLayout());
	      setControl(container);

	      checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER);
	      checkboxTableViewer.setContentProvider(new FormContentProvider());
	      checkboxTableViewer.setLabelProvider(new FormLabelProvider());
	      final Table table = checkboxTableViewer.getTable();
	      
	      final FormData formData = new FormData();
	      formData.bottom = new FormAttachment(100, 0);
	      formData.right = new FormAttachment(100, 0);
	      formData.top = new FormAttachment(0, 0);
	      formData.left = new FormAttachment(0, 0);
	      table.setLayoutData(formData);
	      table.setHeaderVisible(true);

	      final TableColumn tableColumn = new TableColumn(table, SWT.NONE);
	      tableColumn.setWidth(200);
	      tableColumn.setText("name");
	      
	      tableColumn.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent event) {
				//	TableColumn t = (TableColumn)event.getSource();
					allChack();
				}
			});
		
		setPageComplete(false);

	}
	
	public FormModel[] setChecked(){
		
		Object[] checked = checkboxTableViewer.getCheckedElements();
		int count = checked.length;
		
		FormModel[] selected = new FormModel[count];
		
		System.arraycopy(checked, 0, selected, 0, count);
		
		return selected;
		 
	}
	
	
	
	/**
	 * 체크된 데이터들
	 * @return
	 */
	public List<String> getChecked() {
		
		return checkList;
	}
	
	/**
	 * 체크박스 ALL
	 * @param text
	 */
	private boolean isItemCheck = false;
	
	private void allChack() {
		
		 TableItem[] tableItem = checkboxTableViewer.getTable().getItems();
		 if(isItemCheck){
			 isItemCheck = false;
		 } else {
			 isItemCheck = true;
		 }
		 
		 for(TableItem t :tableItem){
			 t.setChecked(isItemCheck);
		 }
		 
	}
	
	/**
	 * 최초 미리 선택
	 * @param idx
	 */
	private void checkedInput(Integer idx) {
		
		if(idx == null) return;
		
		 TableItem[] tableItem = checkboxTableViewer.getTable().getItems();
		 
		 int i=0;
		 for(TableItem t :tableItem){
			 
			 if(i == idx){
				 t.setChecked(true);
			 }
			 i++;
		 }
		 
	}
	
	
}
