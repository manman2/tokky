package zom.dong.sourceconverter.wizard;


public class WizardResultModel {
	
	public String name;
	
	public String type;
	
	public boolean isPaging;
	
	public boolean isFile;
	
	public boolean isEditer;
	
	public boolean isIdAuto;
	
	public boolean isParamObject;
}
