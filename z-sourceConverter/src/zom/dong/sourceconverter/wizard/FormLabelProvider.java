package zom.dong.sourceconverter.wizard;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import zom.dong.sourceconverter.Activator;

public class FormLabelProvider extends LabelProvider implements ITableLabelProvider {

	 //private static final Image UNCHECKED = getImage("ic_check_off.png");
	 private static final Image UNCHECKED = Activator.getImageDescriptor("img/ic_check_off.png").createImage();

	  
	public Image getColumnImage(Object element, int index) {
		
		switch (index) {
			case 1 :
				return UNCHECKED;
			case 2 :
				return UNCHECKED;
			case 3 :
				return UNCHECKED;
			case 4 :
				return UNCHECKED;
			case 5 :
				return UNCHECKED;
			case 6 :
				return UNCHECKED;
			case 7 :
				return UNCHECKED;
			case 8 :
				return UNCHECKED;
			case 9 :
				return UNCHECKED;
			case 10 :
				return UNCHECKED;
			case 11 :
				return UNCHECKED;
			case 12 :
				return UNCHECKED;
			default :
				break;
		}
		return null;
	}

	public String getColumnText(Object element, int index) {
		FormModel formModel = (FormModel) element;
		switch (index) {
			case 0 :
				return formModel.name;
			default :
				break;
		}
		return "";
	}
	

}
