package zom.dong.sourceconverter.wizard;

public class SelectedString {
	  private String key;
	   private String value;

	   public SelectedString(String key, String value) {
	      this.key = key;
	      this.value = value;
	   }

	   public String getKey() {
	      return key;
	   }

	   public String getValue() {
	      return value;
	   }
}
