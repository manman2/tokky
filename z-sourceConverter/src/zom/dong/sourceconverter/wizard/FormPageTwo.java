package zom.dong.sourceconverter.wizard;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;

import org.eclipse.swt.SWT;

import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;

import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.swt.widgets.Label;

import org.eclipse.swt.widgets.Text;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class FormPageTwo extends WizardPage {

	private Button[] buttons;
	private List<String> checkList = new ArrayList<String>();
	private List<SqlMapVo> daoMethodList = new ArrayList<SqlMapVo>();
	private CheckboxTableViewer checkboxTableViewer;
	TableViewer viewer;
	Composite container;
	Shell shell;
	Map<String, String> tableMap = new HashMap<String, String>();
	boolean allChk = false;
	
	 private static final Image UNCHECKED = Activator.getImageDescriptor("img/ic_check_off.png").createImage();
	 private static final Image CHECKED = Activator.getImageDescriptor("img/ic_check_on.png").createImage();
	
	public void setDaoMethodList(List<SqlMapVo> daoMethodList){
		this.daoMethodList = daoMethodList;
	}
	
	public FormPageTwo() {

		super("Check Source");
		setTitle("Check Source");
		setDescription("생성할 소스 체크해 주십시오.");
		//setControl(text1);

	}
	
	public void setVisible(boolean visible){
		
		if(visible){
			
			FormWizard formWizard = (FormWizard)getWizard();
			
			formWizard.setFileSelectList();
			
			formWizard.parseSqlXml();
			
			formWizard.convertNewMethod();
			
			this.daoMethodList = formWizard.getNewMethodList();
			
			if(this.daoMethodList.size() < 1){
				setMessage("추가된 메소드가 없습니다.");
				setPageComplete(false);
			} else {
				
				FormModel formModel= new FormModel();
				//int i = 0;
				//Integer selectedIdx = null;
				for(SqlMapVo method : daoMethodList ){
					
					String id = method.getId();
					formModel.addModel(new FormModel(id));
					
					if(id.equals(formWizard.getSelectedText())){
						//selectedIdx = i;
						
						tableMap.put(id+"-0",id);
					}
					
					//i++;
				}
				
				//checkboxTableViewer.setInput(formModel.getFormModeles());
				
				 viewer.setInput(formModel.getFormModeles());
				 
				 IPreferenceStore store = Activator.getDefault().getPreferenceStore();
					
				 String listNaming = store.getString("sqlNameList").trim();
				 String sqlNameGet = store.getString("sqlNameGet").trim();
				 String sqlNameCreate = store.getString("sqlNameCreate").trim();
				 String sqlNameUpdate = store.getString("sqlNameUpdate").trim();
				 String sqlNameDelete = store.getString("sqlNameDelete").trim();
				 String sqlNameCount = store.getString("sqlNameCount").trim();
				 String sqlNameExists = store.getString("sqlNameExists").trim();
				 
				 for(TableItem ti : viewer.getTable().getItems()){
					 
					 if(tableMap.containsKey(ti.getText()+"-0")){
						 ti.setForeground(0, Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
					 }
					 
					 if(ti.getText().startsWith(sqlNameCreate)){
						 tableMap.put(ti.getText()+"-1",ti.getText());
						 ti.setImage(1, CHECKED);
						 
					 } else if(ti.getText().startsWith(sqlNameUpdate)){
						 tableMap.put(ti.getText()+"-2",ti.getText());
						 ti.setImage(2, CHECKED);
						 
					 } else if(ti.getText().startsWith(sqlNameDelete)){
						 tableMap.put(ti.getText()+"-3",ti.getText());
						 ti.setImage(3, CHECKED);
						 
					 } else if(ti.getText().startsWith(sqlNameGet)){
						 tableMap.put(ti.getText()+"-4",ti.getText());
						 ti.setImage(4, CHECKED);
						 
					 } else if(ti.getText().startsWith(sqlNameExists)){
						 tableMap.put(ti.getText()+"-5",ti.getText());
						 ti.setImage(5, CHECKED);
						 
					 } else if(ti.getText().startsWith(sqlNameCount)){
						 tableMap.put(ti.getText()+"-6",ti.getText());
						 ti.setImage(6, CHECKED);
						 
					 } else if(ti.getText().startsWith(listNaming)){
						 tableMap.put(ti.getText()+"-7",ti.getText());
						 ti.setImage(7, CHECKED);
						 
					 }
					 
					
				 }
				
				//checkedInput(selectedIdx);
				
				
				setPageComplete(true);
			}
			
		} else {
			setPageComplete(false);
		}
		
		super.setVisible(visible);
		
	}
	
	
	public void createControl(Composite parent) {
		
		
		 container = new Composite(parent, SWT.NULL);
	     container.setLayout(new FormLayout());
	     setControl(container);
		
		 viewer = new TableViewer(container, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
	     viewer.setContentProvider(new FormContentProvider());
	     viewer.setLabelProvider(new FormLabelProvider());
	     //getSite().setSelectionProvider(viewer);
	    
	     Table table = viewer.getTable();
	     table.setHeaderVisible(true);
	     table.setLinesVisible(true);
	    
	    
	      final FormData formData = new FormData();
	      formData.bottom = new FormAttachment(100, 0);
	      formData.right = new FormAttachment(100, 0);
	      formData.top = new FormAttachment(0, 0);
	      formData.left = new FormAttachment(0, 0);
	      table.setLayoutData(formData);

	      final TableColumn tableColumn1 = new TableColumn(table, SWT.NONE);
	      tableColumn1.setWidth(200);
	      tableColumn1.setText("name");
	      
	      tableColumn1.addSelectionListener(new SelectionAdapter() {
	    	  
				public void widgetSelected(SelectionEvent e){
					selectedAll();
				}
	    	  
	      });
	      
	     
	      final TableColumn tableColumn8 = new TableColumn(table, SWT.NONE);
	      tableColumn8.setWidth(50);
	      tableColumn8.setText("create");
	      
	      final TableColumn tableColumn7 = new TableColumn(table, SWT.NONE);
	      tableColumn7.setWidth(50);
	      tableColumn7.setText("update");
	      
	      final TableColumn tableColumn9 = new TableColumn(table, SWT.NONE);
	      tableColumn9.setWidth(50);
	      tableColumn9.setText("delete");
	      
	      final TableColumn tableColumn2 = new TableColumn(table, SWT.NONE);
	      tableColumn2.setWidth(50);
	      tableColumn2.setText("get");
	      
	      final TableColumn tableColumn11 = new TableColumn(table, SWT.NONE);
	      tableColumn11.setWidth(50);
	      tableColumn11.setText("exists");
	      
	      final TableColumn tableColumn10 = new TableColumn(table, SWT.NONE);
	      tableColumn10.setWidth(50);
	      tableColumn10.setText("count");
	      
	      final TableColumn tableColumn6 = new TableColumn(table, SWT.NONE);
	      tableColumn6.setWidth(50);
	      tableColumn6.setText("list");
	      
	      final TableColumn tableColumn12 = new TableColumn(table, SWT.CENTER);
	      tableColumn12.setWidth(50);
	      tableColumn12.setText("idAuto");
	      
	      final TableColumn tableColumn3 = new TableColumn(table, SWT.CENTER);
	      tableColumn3.setWidth(50);
	      tableColumn3.setText("Paging");
	      
	      final TableColumn tableColumn4 = new TableColumn(table, SWT.CENTER);
	      tableColumn4.setWidth(50);
	      tableColumn4.setText("File");
	      
	      final TableColumn tableColumn5 = new TableColumn(table, SWT.CENTER);
	      tableColumn5.setWidth(50);
	      tableColumn5.setText("Editer");
	      
	      final TableColumn tableColumn13 = new TableColumn(table, SWT.CENTER);
	      tableColumn13.setWidth(100);
	      tableColumn13.setText("ParamObject");
	      
	      table.addMouseListener(new MouseAdapter() {
	    	    public void mouseDown(MouseEvent e) {
	    	        ViewerCell viewerCell = viewer.getCell(new Point(e.x,e.y));
	    	        
	    	        if(viewerCell != null){
	    	        	
	    	        	 if(viewerCell.getColumnIndex() > 0){
	    	        		 
	    	        		 
	    	        		// boolean isExists = isCellExists(viewerCell);
	    	        		 
	    	        		 //if(isExists){
	    	        			 boolean isChk = chkCell(viewerCell);
	    	        			 
	    	        			 if(isChk){
		    	        			 viewerCell.setImage(CHECKED);
		    	        		 } else {
		    	        			 viewerCell.setImage(UNCHECKED);
		    	        		 }
	    	        		// }
	    	        		 
	    	        		 
	    	        	 } else {
	    	        		 
	    	        		 boolean isChk = chkCell(viewerCell);
	    	        		 
	    	        		 if(isChk){
	    	        			 viewerCell.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
	    	        		 } else {
	    	        			 viewerCell.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
	    	        		 }
	    	        	 }
	    	        	 
	    	        	// FormModel element = (FormModel)viewerCell.getElement();
	 	    	        //System.out.println("Cell clicked: " + viewerCell.getColumnIndex()+","+element.name);
	    	        	
	    	        }
	    	       
	    	    }
	    	});
		
		setPageComplete(false);

	}
	
	
	public Shell getShell(){
		
		return this.container.getShell();
	}
	
	private void selectedAll(){
		
		 Table table = viewer.getTable();
		 
		 for(TableItem ti : table.getItems()){
			 
			 String key = ti.getText()+"-0";
			 tableMap.remove(key);
			 
			 if(allChk){
				 ti.setForeground(0,Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
				 
				 /*
				 for(int i=1; i<10; i++){
					 tableMap.remove(ti.getText()+"-"+i);
					 ti.setImage(i,UNCHECKED);
				 }
				 */
				 
				 
			 } else {
				 tableMap.put(key, ti.getText());
				 ti.setForeground(0,Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
				
			 }
		 }
		 
		 if(allChk){
			 allChk = false;
		 } else {
			 allChk = true;
		 }
	}
	
	private boolean isCellExists(ViewerCell viewerCell){
		
		FormModel element = (FormModel)viewerCell.getElement();
		
		return tableMap.containsKey(element.name+"-0");
	
	}
	
	private boolean chkCell(ViewerCell viewerCell){
		
		boolean isChk = false;
		
		FormModel element = (FormModel)viewerCell.getElement();
		
		String key = element.name+"-"+viewerCell.getColumnIndex();
		
		String val = tableMap.get(key);
		
		if(StringUtils.isEmpty(val)){
			
			if(viewerCell.getColumnIndex() == 1 || viewerCell.getColumnIndex() == 2 || viewerCell.getColumnIndex() == 3 
					|| viewerCell.getColumnIndex() == 4 || viewerCell.getColumnIndex() == 5 || viewerCell.getColumnIndex() == 6 || viewerCell.getColumnIndex() == 7){
				 Table table = viewer.getTable();
				 for(TableItem ti : table.getItems()){
					 if(ti.getText().equals(element.name)){
						 
						 for(int i=1; i<8; i++){
							 tableMap.remove(ti.getText()+"-"+i);
							 ti.setImage(i,UNCHECKED);
						 }
						 
					 }
				 }
			}
			
			tableMap.put(key, element.name);
			isChk = true;
			
		} else {
			tableMap.remove(key);
			isChk = false;
			
			/*
			if(viewerCell.getColumnIndex() == 0){
				 Table table = viewer.getTable();
				 for(TableItem ti : table.getItems()){
					 if(ti.getText().equals(element.name)){
						 
						 for(int i=1; i<10; i++){
							 tableMap.remove(ti.getText()+"-"+i);
							 ti.setImage(i,UNCHECKED);
						 }
						 
					 }
				 }
			}
			*/
			
		}
		
		return isChk;
		
	}
	
	public Map<String, String> setChecked(){
		
		return tableMap;
		 
	}
	
	
	
	/**
	 * 체크된 데이터들
	 * @return
	 */
	public List<String> getChecked() {
		
		return checkList;
	}
	
	/**
	 * 체크박스 ALL
	 * @param text
	 */
	private boolean isItemCheck = false;
	
	private void allChack() {
		
		 TableItem[] tableItem = checkboxTableViewer.getTable().getItems();
		 if(isItemCheck){
			 isItemCheck = false;
		 } else {
			 isItemCheck = true;
		 }
		 
		 for(TableItem t :tableItem){
			 t.setChecked(isItemCheck);
		 }
		 
	}
	
	/**
	 * 최초 미리 선택
	 * @param idx
	 */
	private void checkedInput(Integer idx) {
		
		if(idx == null) return;
		
		 TableItem[] tableItem = checkboxTableViewer.getTable().getItems();
		 
		 int i=0;
		 for(TableItem t :tableItem){
			 
			 if(i == idx){
				 t.setChecked(true);
			 }
			 i++;
		 }
		 
	}
	
	
}
