package zom.dong.sourceconverter.wizard;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;

public class FormContentProvider extends ArrayContentProvider implements ITreeContentProvider {

	public Object[] getChildren(Object parentElement) {
		FormModel formModel = (FormModel) parentElement;
		return formModel.children;
	}

	public Object getParent(Object element) {
		FormModel formModel = (FormModel) element;
		return formModel.parent;
	}

	public boolean hasChildren(Object element) {
		FormModel formModel = (FormModel) element;
		return formModel.children.length > 0;
	}

}
