package zom.dong.sourceconverter.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jface.preference.IPreferenceStore;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.constants.Constants;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.model.Param;
import zom.dong.sourceconverter.model.SqlMapVo;

public class UtilUnit {
	
	/**
	 * 파일 존재 하는지
	 * @param fileUrl
	 * @return
	 */
	public static boolean isExistFile(String fileUrl){
		
		File file = new File(fileUrl);
		
		return file.exists();
	}

	/**
	 * java파일 메소드리스트 가져오기
	 * @param fileUrl
	 * @return
	 */
	public static List<String> getMethodJava(String fileUrl){
		
		List<String> methodList = new ArrayList<String>();
		
		if(isExistFile(fileUrl)){		
			
			//파일이 존재하면
			ICompilationUnit IUnit = getICompliationUnit(fileUrl);
			
			try {
				IType type = null;
				IType[] allTypes; 
				allTypes = IUnit.getAllTypes();
				 
				for (int t = 0; t < allTypes.length; t++) {
					if (Flags.isPublic((allTypes[t].getFlags()))) {
						type = allTypes[t];
					}
				}
				IMethod[] methods = type.getMethods();
				Pattern pt = Pattern.compile("NAMESPACE.?[+].?\"[.]?(.+)\".?,?", Pattern.DOTALL);
				
				for(IMethod iMethod : methods){
					
	       			Matcher mt = pt.matcher(iMethod.getSource());
	       			
	       			while (mt.find()) {
	       				String sqlName = mt.group(1);
	       				methodList.add(sqlName);
	       			}
				}
			
			} catch (JavaModelException e) {
				System.err.println("not file :"+fileUrl);
				e.printStackTrace();
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return methodList;
	}
	
	/**
	 * 파일 읽어서 ICompliationUnit 으로 변경
	 * @param fileUrl
	 * @return
	 */
	private static ICompilationUnit getICompliationUnit(String fileUrl){
		
		IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
		IPath path = new Path(fileUrl);
		IFile[] files = workspace.getRoot().findFilesForLocation(path);
		
		IFile javaFile = null;
		for(IFile file : files ){
			javaFile = file;
		}
		
		return (ICompilationUnit) JavaCore.createCompilationUnitFrom(javaFile);
	}

	
	/**
	 * 매소드를 이용한 java 파일 생성
	 * @param fileUrl
	 * @param mClass
	 */
	public static void makeJavaFile(String fileUrl, MClass mClass){

		try{

			File file = new File(fileUrl);
			String fileContent = "";
			
			if(file.exists()){
				
				FileReader fr= new FileReader(file);
				BufferedReader br = new BufferedReader(fr, 1024);

				int b;

				while ((b = br.read()) != -1) {
					fileContent += (char)b;
				}	
				
				br.close();
				fr.close();
				
				fileContent = fileContent.substring(0,fileContent.lastIndexOf("}"));
			}
			
			String classContents = mClass.make();
			
			if(classContents.trim().length() > 0){
				
				fileContent += classContents;
				
				if(file.exists()){
					fileContent += "\n}";
				}
				
				makeFile(fileUrl, fileContent);
				
			}
	
		}catch(Exception ex){

			ex.printStackTrace();

		}
	}
	
	
	
	/**
	 * 파일 생성
	 * @param fileUrl
	 * @param mClass
	 */
	public static void makeFile(String fileUrl, String fileContent){

		try{
				
			// 파일을 읽어 옵니다.
			FileWriter fw = new FileWriter(fileUrl);
	
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileUrl), 1024);
	
			// 타이틀을 파일에 저장 합니다.
			bw.write(fileContent);
	
			bw.close();
			fw.close();
	
		}catch(Exception ex){

			ex.printStackTrace();

		}
	}
	
	/**
	 * 첫글자만 대문자로 변경
	 * @param text
	 * @return
	 */
	public static String firstUpper(String text){
		
		if(StringUtils.isEmpty(text)){
			return "";
		}
		
		return Character.toUpperCase(text.charAt(0)) + text.substring(1);
	}
	
	/**
	 * 첫글자만 소문자로 변경
	 * @param text
	 * @return
	 */
	public static String firstLower(String text){
		
		if(StringUtils.isEmpty(text)){
			return "";
		}
		
		return Character.toLowerCase(text.charAt(0)) + text.substring(1);
	}
	
	
	
	/**
	 * 모델 필드 객체 가져오기
	 * @param modelFildsMap
	 * @param elementName
	 * @param modelName
	 * @return
	 * @throws IllegalArgumentException
	 * @throws JavaModelException
	 */
	public static String getFiledType(Map<String, IField[]> modelFildsMap, String modelName, String elementName) {
		
		IField[] fields = modelFildsMap.get(modelName);
		
		boolean isChk = false;
		String type = "";
		
		if(fields != null){
			for(IField filed : fields){
				
				if(filed.getElementName().equals(elementName)){
					String elementType = "String";
					try {
						elementType = Signature.toString(filed.getTypeSignature());
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (JavaModelException e) {
						e.printStackTrace();
					}
					type = elementType;
					isChk = true;
					break;
				}
			}
		}
		
		if(!isChk){
			type = UtilUnit.getGuessParamType(elementName);
		}
		
		return type;
		
	}
	
	
	/**
	 * 파라미터 map이나 object로 생성
	 * 
	 * object = new Object();
	 * object.setId(id);
	 * object.setDate(date);
	 * 
	 * @param sqlMapVo
	 * @param sb
	 * @param objectName
	 * @return
	 */
	public static String parseResultParam(SqlMapVo sqlMapVo, StringBuffer sb){
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		 String insertNaming = store.getString("sqlNameCreate").trim();

		String resultParam = "";
		
		String paramType = sqlMapVo.getParamType();
		
		int chk = 0;
		for(String param : sqlMapVo.getParamList()){	//페이징 처리 된 쿼리이면 객체로 파라미터 받는다.
			if(param.indexOf("endRow") > -1 || param.indexOf("startRow") > -1){
				chk++;
			}
		}
		
		if(paramType != null){
			
			String paramTypeCase = UtilUnit.firstLower(paramType);
			
			//sql에 파라미터를 받고 그 파라미터 개수가 여러개일 경우
			if(!sqlMapVo.getId().equals(insertNaming) && chk <= 1 && sqlMapVo.getParamList() != null && sqlMapVo.getParamList().size() <= Constants.PARAMETER_COUNT && sqlMapVo.getParamList().size() > 0){
				
				if(sqlMapVo.getParamType().toLowerCase().indexOf("map") > -1){
					
					sb.append("\t\tMap<String, Object> map = new HashMap<String, Object>();\n");
					
					for(String param : sqlMapVo.getParamList()){
						sb.append("\t\tmap.put(\"" + param + "\", " + param + ");\n");
					}
					sb.append("\n");
							
					resultParam = "map";
					
				} else if( sqlMapVo.getParamList().size() == 1){
					
					resultParam = sqlMapVo.getParamList().get(0);
					
				} else if(sqlMapVo.getParamList().size() != chk) {
					
					sb.append("\t\t"+paramType+" "+paramTypeCase+" = new "+paramType+"();\n");
					
					for(String param : sqlMapVo.getParamList()){
						String paramCase = firstUpper(param);
						sb.append("\t\t"+paramTypeCase+".set"+paramCase+"("+param+");\n");
					}
					sb.append("\n");
					
					resultParam = paramTypeCase;
					
				} 
				
			} else if(sqlMapVo.getParamList().size() == 0){
				resultParam = "";
			} else {
					
				if(sqlMapVo.getParamType().toLowerCase().indexOf("map") > -1){
					resultParam = "map";
				} else if(sqlMapVo.getParamType().toLowerCase().equalsIgnoreCase("list") ){
					resultParam = "list";
				} else {
					resultParam = paramTypeCase;
				}
			}
		}
		return resultParam;
	}
	
	/**
	 * import 생성
	 * @param sourceInfo
	 * @param sqlXml
	 * @param dmClass
	 */
	public void addModelImport(GetSourceInfo sourceInfo,  ParseSqlXml sqlXml, MClass clazz ){
		
		for(SqlMapVo sqlMapVo : sqlXml.getVoList()){
			
			//paramClass에서 가져오기
			if(sqlMapVo.getParamType() != null  && !(sqlMapVo.getParamType().indexOf("java.util") > -1 || sqlMapVo.getParamType().indexOf("java.lang") > -1 || sqlMapVo.getParamType().equalsIgnoreCase("string") || sqlMapVo.getParamType().equals("int") || sqlMapVo.getParamType().equals("Integer") || sqlMapVo.getParamType().equals("double") || sqlMapVo.getParamType().equalsIgnoreCase("map")) ){
				
				String importPath = sourceInfo.getModelPackage()+"." + firstUpper(sqlMapVo.getParamType());
				
				if(!clazz.getImportList().contains(importPath)){
					clazz.addImportList(importPath);
				}
			}
			
			if(sqlMapVo.getReturnType() != null){
				//resultClass에서 가져오기
				if(sqlMapVo.getReturnType().equals(Constants.RESULT_TYPE_CLASS) && !(sqlMapVo.getResultObject().indexOf("java.util") > -1 || sqlMapVo.getResultObject().indexOf("java.lang") > -1 || sqlMapVo.getResultObject().equalsIgnoreCase("string") || sqlMapVo.getResultObject().equals("int") || sqlMapVo.getResultObject().equals("double") || sqlMapVo.getResultObject().equalsIgnoreCase("map"))){
					
					String importPath = sourceInfo.getModelPackage()+"." + firstUpper(sqlMapVo.getResultObject());
					
					if(!clazz.getImportList().contains(importPath)){
						clazz.addImportList(importPath);
					}
					
				//resultmap에서 클래스 가져오기
				} else if(sqlMapVo.getReturnType().equals(Constants.RESULT_TYPE_MAP)){
					
					String object = (String)(sqlXml.getResultMap().get(sqlMapVo.getResultObject())).get("clazz");
					String importPath = sourceInfo.getModelPackage() + "." + firstUpper(object);
							
					if(!clazz.getImportList().contains(importPath) && !object.equalsIgnoreCase("map")){
						clazz.addImportList(importPath);
					}
				}
			}
		}
	}
	
	/**
	 * 체크목록에 메소드가 존재하는지 검사
	 * @param fileType
	 * @param checkList
	 * @param sqlVo
	 * @return
	 */
	public boolean isExistMethod(String fileType, List<String> checkList, SqlMapVo sqlVo){
		
		boolean existMethod = false;
		for(String checkMethod : checkList){
			if(checkMethod.indexOf("/"+fileType+"/"+sqlVo.getId()+"/")>-1){
				existMethod = true;
			}
		}
		
		return existMethod;
	}
	
	/**
	 * 폴더 생성
	 * @param iFile
	 */
	public static void makeDir(String dirUrl){

		try{

			File fileDir = new File(dirUrl);
			
			if(!fileDir.exists()){
				fileDir.mkdirs();
			} 
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	/**
	 * xml 엘리먼트로 변환하여 가져오기
	 * @param file
	 * @returU
	 * @throws Exception
	 */
	public static Element getXml(File file) throws Exception{
		
		SAXBuilder saxBuilder = new SAXBuilder();
	 	Document jdomDocument = new Document();
	 
		FileInputStream inputStream = new FileInputStream(file);
		jdomDocument = saxBuilder.build(inputStream);
		
		Element root = jdomDocument.getRootElement();
		return root;
		
	}
	
	/**
	 * 구분자를 이용해 마지막 단어 가져오기
	 * @param contents
	 * @param prx
	 * @return
	 */
	public static String lastWord(String contents, String prx){
		
		if(contents == null){
			return "";
		}
		
		if(contents.indexOf(prx)==-1){
			return contents;
		}
		
		return contents.substring(contents.lastIndexOf(prx)+1, contents.length());
		
	}
	
	/**
	 * 파라미터 추측 타입
	 * @param paramType
	 * @return
	 */
	public static String getGuessParamType(String paramType){
		String result = "String";
		
		if(paramType.toLowerCase().contains("count") || paramType.toLowerCase().contains("size") || paramType.toLowerCase().contains("num") || paramType.toLowerCase().contains("index")){
			result = "int";
		} else if(paramType.toLowerCase().contains("date")){
			result = "Date";
		}
		
		return result;
	}
	
}
