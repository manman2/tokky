package zom.dong.sourceconverter.util;

import java.util.List;

public class CheckMethod {

	private boolean isDao = false;
	
	private boolean isDaoImpl= false;
	
	private boolean isService= false;
	
	private boolean isServiceImpl= false;
	
	private boolean isWeb= false;
	
	private boolean isHtml= false;
	
	private boolean isDaoTest= false;
	
	private boolean isServiceTest= false;
	
	public CheckMethod(List<Integer> checkList){
		
		 if(checkList.contains(0)){
			 isDaoImpl = true;
         }
        	
         //dao make
         if(checkList.contains(1)){
        	 isDao = true;
         }
         
         //dao test make
         if(checkList.contains(2)){
        	 isDaoTest = true;
         }
         
         //serviceImpl make
         if(checkList.contains(3)){
        	 isServiceImpl = true;
         }
         
         //service make
         if(checkList.contains(4)){
        	 isService = true;
         }
         
         //service test make
         if(checkList.contains(5)){
        	 isServiceTest = true;
         }
         
         //controller make
         if(checkList.contains(6)){
        	 isWeb = true;
         }
         
         
         //html make
         if(checkList.contains(8)){
        	 isHtml = true;
         }
         
	}

	public boolean isDao() {
		return isDao;
	}

	public void setDao(boolean isDao) {
		this.isDao = isDao;
	}

	public boolean isDaoImpl() {
		return isDaoImpl;
	}

	public void setDaoImpl(boolean isDaoImpl) {
		this.isDaoImpl = isDaoImpl;
	}

	public boolean isService() {
		return isService;
	}

	public void setService(boolean isService) {
		this.isService = isService;
	}

	public boolean isServiceImpl() {
		return isServiceImpl;
	}

	public void setServiceImpl(boolean isServiceImpl) {
		this.isServiceImpl = isServiceImpl;
	}

	public boolean isWeb() {
		return isWeb;
	}

	public void setWeb(boolean isWeb) {
		this.isWeb = isWeb;
	}

	public boolean isHtml() {
		return isHtml;
	}

	public void setHtml(boolean isHtml) {
		this.isHtml = isHtml;
	}

	public boolean isDaoTest() {
		return isDaoTest;
	}

	public void setDaoTest(boolean isDaoTest) {
		this.isDaoTest = isDaoTest;
	}

	public boolean isServiceTest() {
		return isServiceTest;
	}

	public void setServiceTest(boolean isServiceTest) {
		this.isServiceTest = isServiceTest;
	}
	

}
