
package zom.dong.sourceconverter.constants;

/**
 * TODO Javadoc주석작성
 *
 * @author 이동희 (loverfairy@gmail.com)
 * @version $Id: Constants.java 115 2013-02-18 08:15:42Z loverfairy@gmail.com $
 */
public final class Constants {
	
	private Constants() {}
	 
	/**
	 * Dao Path
	 */
	public static String DAO_PATH = "";
    
	/**
	 * DAOIMPL_PATH
	 */
	public static String DAOIMPL_PATH = "";
    
    /**
     * 파라미터 개수 이상되면 오브젝트로 만듬. 메소드에 개수 이상 오브젝트로 생성.
     */
	public static int PARAMETER_COUNT = 4;
	
	public static String FORMCONTENT = "";
	
	public static String VIEWCONTENT = "";
	
	public static String LISTCONTENT = "";
	
	/**
	 * sql result 타입
	 * map
	 */
	public static String RESULT_TYPE_MAP = "map";
	
	/**
	 * class
	 */
	public static String RESULT_TYPE_CLASS = "class";
	
	
}
