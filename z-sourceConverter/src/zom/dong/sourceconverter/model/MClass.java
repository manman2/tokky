package zom.dong.sourceconverter.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class MClass {
	
	private String packageName;
	
	private List<String> importList = new LinkedList<String>();
	
	private String className;
	
	private String classType = "class";
	
	private String extendsClass;
	
	private String implementsClass;
	
	private String classAnotation;
	
	private List<MType> methodList = new ArrayList<MType>();
	
	private List<MType> filedList = new ArrayList<MType>();

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public List<String> getImportList() {
		return importList;
		
	}

	public void setImportList(List<String> importList) {
		this.importList = importList;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getExtendsClass() {
		return extendsClass;
	}

	public void setExtendsClass(String extendsClass) {
		this.extendsClass = extendsClass;
	}

	public String getImplementsClass() {
		return implementsClass;
	}

	public void setImplementsClass(String implementsClass) {
		this.implementsClass = implementsClass;
	}

	public String getClassAnotation() {
		return classAnotation;
	}

	public void setClassAnotation(String classAnotation) {
		this.classAnotation = classAnotation;
	}

	public List<MType> getMethodList() {
		return methodList;
	}

	public void setMethodList(List<MType> methodList) {
		this.methodList = methodList;
	}

	public List<MType> getFiledList() {
		return filedList;
	}
	
	public void setFiledList(List<MType> filedList) {
		this.filedList = filedList;
	}

	
	public void addFiledList(MType mType) {
		filedList.add(mType);
	}
	
	public void addMethodList(MType mType) {
		methodList.add(mType);
	}
	
	public void addImportList(String importUrl) {
		importList.add(importUrl);
	}
	
	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}
	
	public String make(){
		
		StringBuffer result = new StringBuffer();
		
		if(this.packageName != null){
			result.append("package "+this.packageName+";\n\n");
		}

		if(this.importList.size() > 0){
			for(String importUrl : this.importList){
				result.append("import "+importUrl+";\n");
			}
			result.append("\n");
		}
		
		
		if(this.classAnotation != null){
			result.append("\n"+this.classAnotation+"\n");
		}
		
		if(this.className != null){
			result.append("public "+this.classType+" "+this.className);
		}
		
		if(this.extendsClass != null){
			result.append(" extends "+this.extendsClass);
		}
		
		if(this.implementsClass != null){
			result.append(" implements "+this.implementsClass);
		}
		
		if(this.className != null){
			result.append(" {\n\n");
		}
		
		//변수 만들기
		if(this.filedList.size() > 0){
			for(MType mType : this.filedList){
				if(mType.getAnnotation() != null){
					result.append("\t"+mType.getAnnotation()+"\n");
				}
				
				String finalText = (mType.isFinal())? " final" :"";
				
				result.append("\t"+mType.getDataProperty() + finalText + " " + mType.getDataType()+ " " + mType.getName());
				
				if(mType.getContents() != null){
					if(mType.getDataType().equals("String")){
						result.append(" = \""+mType.getContents() +"\";\n");
					} else {
						result.append(" = "+mType.getContents() +";\n");
					}
				} else {
					result.append(";\n");
				}
				result.append("\n");
			}
			result.append("\n");
		}
		
		//함수 만들기
		for(MType method : this.methodList){
			
			result.append("\t/**\n\t*\n\t*\n\t*/\n");
			if(method.getAnnotation() != null){
				result.append("\t"+method.getAnnotation()+"\n");
			}
			
			if(method.getDataProperty() != null){
				result.append("\t"+method.getDataProperty()+" ");
			} else {
				result.append("\t");
			}
			
			if(method.getDataType() != null){
				result.append(method.getDataType()+" ");
			} 
			
			result.append(method.getName()+"(");
			
			int i = 0;
			for(String param : method.getParameterList()){
				if(i != 0) result.append(", ");
				result.append(param);
				i++;
			}
			
			result.append(")");
			
			if(method.getContents() != null){
				result.append("{\n\n");
				result.append(method.getContents());
				result.append("\n\t}\n\n");
			} else {
				result.append(";\n\n");
			}
			
			
		}
		
		if(this.className != null){
			result.append("}");
		}
		
		return result.toString();
	}


}
