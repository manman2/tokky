package zom.dong.sourceconverter.model;

import java.util.ArrayList;
import java.util.List;

public class MType {

	//데이터 속성 - public, private
	private String dataProperty;
	
	//final 속성
	private boolean isFinal = false;
	
	//data type, 함수일땐 리턴값 - String, int
	private String dataType;
	
	//이름
	private String name;
	
	//변수들
	private List<String> parameterList = new ArrayList<String>();
	
	//필드일때 값, 함수일때 내용(로직)
	private String contents;
	
	//어노테이션
	private String annotation;
	
	//crud 중 몬지
	private String type;
	
	private boolean isPaging;
	
	private boolean isFile;
	
	private boolean isEditer;
	
	private boolean isIdAuto;
	
	private boolean isParamObject;
	
	private String methodType;
	
	private List<String> throwsList = new ArrayList<String>();

	public String getDataProperty() {
		return dataProperty;
	}

	public void setDataProperty(String dataProperty) {
		this.dataProperty = dataProperty;
	}

	public boolean isFinal() {
		return isFinal;
	}

	public void setFinal(boolean isFinal) {
		this.isFinal = isFinal;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getParameterList() {
		return parameterList;
	}
	
	public List<String> getThrowsList() {
		return throwsList;
	}

	public void setParameterList(List<String> parameterList) {
		this.parameterList = parameterList;
	}
	
	public void setThrowsList(List<String> throwsList) {
		this.throwsList = throwsList;
	}
	
	public void addParameterList(String parameter) {
		parameterList.add(parameter);
	}
	
	public void addThrowsList(String throwss) {
		throwsList.add(throwss);
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isPaging() {
		return isPaging;
	}

	public void setPaging(boolean isPaging) {
		this.isPaging = isPaging;
	}

	public boolean isFile() {
		return isFile;
	}

	public void setFile(boolean isFile) {
		this.isFile = isFile;
	}

	public boolean isEditer() {
		return isEditer;
	}

	public void setEditer(boolean isEditer) {
		this.isEditer = isEditer;
	}

	public boolean isIdAuto() {
		return isIdAuto;
	}

	public void setIdAuto(boolean isIdAuto) {
		this.isIdAuto = isIdAuto;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public boolean isParamObject() {
		return isParamObject;
	}

	public void setParamObject(boolean isParamObject) {
		this.isParamObject = isParamObject;
	}
	
	
}
