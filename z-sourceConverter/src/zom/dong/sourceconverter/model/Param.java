package zom.dong.sourceconverter.model;


public class Param {

	//속성
	private String type;
	
	//이름
	private String name;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
