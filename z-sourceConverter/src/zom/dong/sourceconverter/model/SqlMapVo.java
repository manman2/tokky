package zom.dong.sourceconverter.model;

import java.util.ArrayList;
import java.util.List;

public class SqlMapVo {

	/**
	 * 결과 타입이 map인지 class인지
	 */
	private String returnType;
	
	/**
	 * parameterClass
	 */
	private String paramType;
	
	private String paramName;
	
	/**
	 * id
	 */
	private String id;
	
	/**
	 * CRUD 중 어느건지
	 */
	private String type;
	
	/**
	 * 쿼리에 넘어가는 파라미터들 예) #abc#
	 */
	private List<String> paramList;
	
	/**
	 * 결과 객체
	 */
	private String resultObject;
	
	/**
	 * iterater 목록
	 */
	private List<String> iterateList;
	
	/**
	 * 인서트 구문에 selecKey가 있는지. 
	 */
	private boolean isInsertKey = false;
	
	/**
	 * 결과 필드
	 */
	private List<String> resultField = new ArrayList<String>();
	
	//create, update, delete, get, exists, count, list
	private String methodType;
	
	private boolean isPaging;
	
	private boolean isFile;
	
	private boolean isEditer;
	
	private boolean isIdAuto;
	
	private boolean isParamObject;
	
	public void addResultField(String Fielde){
		resultField.add(Fielde);
	}
	
	
	public List<String> getResultField() {
		return resultField;
	}
	
	public void setResultField(List<String> resultField) {
		this.resultField = resultField;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getParamList() {
		return paramList;
	}

	public void setParamList(List<String> paramList) {
		this.paramList = paramList;
	}

	public String getResultObject() {
		return resultObject;
	}

	public void setResultObject(String resultObject) {
		this.resultObject = resultObject;
	}

	public List<String> getIterateList() {
		return iterateList;
	}

	public void setIterateList(List<String> iterateList) {
		this.iterateList = iterateList;
	}

	public boolean isInsertKey() {
		return isInsertKey;
	}

	public void setInsertKey(boolean isInsertKey) {
		this.isInsertKey = isInsertKey;
	}


	public String getParamName() {
		return paramName;
	}


	public void setParamName(String paramName) {
		this.paramName = paramName;
	}


	public String getMethodType() {
		return methodType;
	}


	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}


	public boolean isPaging() {
		return isPaging;
	}


	public void setPaging(boolean isPaging) {
		this.isPaging = isPaging;
	}


	public boolean isFile() {
		return isFile;
	}


	public void setFile(boolean isFile) {
		this.isFile = isFile;
	}


	public boolean isEditer() {
		return isEditer;
	}


	public void setEditer(boolean isEditer) {
		this.isEditer = isEditer;
	}


	public boolean isIdAuto() {
		return isIdAuto;
	}


	public void setIdAuto(boolean isIdAuto) {
		this.isIdAuto = isIdAuto;
	}


	public boolean isParamObject() {
		return isParamObject;
	}


	public void setParamObject(boolean isParamObject) {
		this.isParamObject = isParamObject;
	}


	
}
