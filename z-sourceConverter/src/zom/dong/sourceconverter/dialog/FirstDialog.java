package zom.dong.sourceconverter.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import zom.dong.sourceconverter.model.SqlMapVo;

public class FirstDialog extends TitleAreaDialog {

	private Button[] buttons;
	private List<String> checkList = new ArrayList<String>();
	private List<SqlMapVo> daoMethodList;
	private List<Integer> selectPath;

	public FirstDialog(Shell parentShell, List<SqlMapVo> daoMethodList, List<Integer> selectPath) {
		super(parentShell);
		this.daoMethodList = daoMethodList;
		this.selectPath = selectPath;
	}

	@Override
	public void create() {
		super.create();
		// Set the title
		setTitle("Check Source");
		// Set the message
		setMessage("생성할 소스 체크해 주십시오.", IMessageProvider.INFORMATION);

	}
	
	/**
	 * TODO Javadoc주석작성
	 * @param table
	 * @param text
	 */
	private void viewTableColumn(Table table, String text){
		
		TableColumn column = new TableColumn(table, SWT.CENTER);
		if(text.equals("")){
			 column.setWidth(100);
		} else {
			 column.setWidth(80);
		}
        column.setText(text);
     
        column.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				TableColumn t = (TableColumn)event.getSource();
				allChack(t.getText()+"/");
			}
		});
	        
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		
		Table table = new Table(parent, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
	    table.setHeaderVisible(true);
	    table.setLinesVisible(true);
	    table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true)); 
	    
	    viewTableColumn(table, "");
	    viewTableColumn(table, "dao");
	    viewTableColumn(table, "service");
	    viewTableColumn(table, "daoTest");
	    viewTableColumn(table, "serviceTest");
	    viewTableColumn(table, "web");
	    viewTableColumn(table, "html");
        
        int idSize = daoMethodList.size();
        
        for (int i = 0; i < idSize; i++) {
            new TableItem(table, SWT.NONE);
          }
        
        TableItem[] items = table.getItems();
        
        buttons = new Button[(items.length)*(table.getColumns().length-1)];
        int z = 0;
        for (int i = 0; i < items.length; i++) {
        	
        	TableEditor editor = new TableEditor(table);
        	Button text = new Button(table, SWT.PUSH);
	        text.setText(daoMethodList.get(i).getId());
	        
	        text.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent event) {
					Button t = (Button)event.getSource();
					allChack("/"+t.getText()+"/");
				}
			});

	        editor.grabHorizontal = true;
	        //editor.minimumHeight = label.getSize().y;
	        editor.minimumWidth = text.getSize().x;
	        editor.setEditor(text, items[i], 0);
	        
	        for(int j=1; j < table.getColumns().length; j++){
	        	
	        	buttons[z] = new Button(table, SWT.CHECK);
	        	buttons[z].setData("/"+table.getColumns()[j].getText()+"/"+daoMethodList.get(i).getId()+"/");
	        	
	        	if(!selectPath.contains(j) || daoMethodList.get(i).getId().equals("ALL") || table.getColumns()[j].getText().equals("ALL") ){
	        		buttons[z].setSelection(false);
	        	} else {
	        		buttons[z].setSelection(true);
	        	}
	        	
	        	buttons[z].pack();
	        	
	 	        editor = new TableEditor(table);
	 	        editor.minimumWidth = buttons[z].getSize().x;
	 	        editor.horizontalAlignment = SWT.CENTER;

	 	        editor.setEditor(buttons[z], items[i], j);
	 	        
	 	        z++;
	        }
	      }

		return parent;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 3;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = SWT.CENTER;

		parent.setLayoutData(gridData);
		/*
		Button preButton = createButton(parent, 3, "Prev", false);
		// Add a SelectionListener
		preButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setReturnCode(3);
				close();
			}
		});
		*/
		// Create Add button
		// Own method as we need to overview the SelectionAdapter
		createOkButton(parent, OK, "Next", true);
		// Add a SelectionListener

		// Create Cancel button
		Button cancelButton = createButton(parent, CANCEL, "Cancel", false);
		// Add a SelectionListener
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setReturnCode(CANCEL);
				close();
			}
		});
	}

	protected Button createOkButton(Composite parent, int id, String label, boolean defaultButton) {
		
		// increment the number of columns in the button bar
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (isValidInput()) {
					okPressed();
				}
			}
		});
		
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		
		setButtonLayoutData(button);
		return button;
	}

	private boolean isValidInput() {
		boolean valid = true;
		/*
		if (firstNameText.getText().length() == 0) {
			setErrorMessage("Please maintain the first name");
			valid = false;
		}
		if (lastNameText.getText().length() == 0) {
			setErrorMessage("Please maintain the last name");
			valid = false;
		}
		*/
		return valid;
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}

	/**
	 * TODO Javadoc주석작성
	 *
	 */
	private void saveInput() {
		
		for(Button button : buttons){
			
			if(button.getSelection()){
				checkList.add(button.getData().toString());
			}
		}
	}
	

	@Override
	protected void okPressed() {
		saveInput();
		super.okPressed();
	}

	/**
	 * 체크된 데이터들
	 * @return
	 */
	public List<String> getChecked() {
		return checkList;
	}
	
	/**
	 * 체크박스 ALL
	 * @param text
	 */
	private void allChack(String text) {
		
		int chk = 0;
		boolean sel = false;
		for(Button b : buttons){
			
			if(b.getData().toString().indexOf(text) > -1 ){
				
				if(chk == 0 && b.getSelection()){
					sel = true;
				}
				
				b.setSelection(!sel);
				
				++chk;
			}
		}
	}
}

