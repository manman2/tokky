package zom.dong.sourceconverter.handler;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

import zom.dong.sourceconverter.handler.make.MakeDao;
import zom.dong.sourceconverter.handler.make.MakeDaoImpl;
import zom.dong.sourceconverter.handler.make.MakeDaoTest;
import zom.dong.sourceconverter.handler.make.MakeHtml;
import zom.dong.sourceconverter.handler.make.MakeService;
import zom.dong.sourceconverter.handler.make.MakeServiceImpl;
import zom.dong.sourceconverter.handler.make.MakeController;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.CheckMethod;
import zom.dong.sourceconverter.util.UtilUnit;
import zom.dong.sourceconverter.wizard.FormWizard;
import zom.dong.sourceconverter.wizard.WizardResultModel;

public class HotSource {
	
	GetSourceInfo sourceInfo;
	ParseSqlXml sqlXml;
	List<SqlMapVo> actionMethodList = new ArrayList<SqlMapVo>();
	List<WizardResultModel> checkList;
	CheckMethod checkFile;
	
	HotSource(GetSourceInfo sourceInfo, FormWizard formWizard){
		this.sourceInfo = sourceInfo;
		this.sqlXml = formWizard.getSqlXml();
		this.checkList = formWizard.getCheckList();
		
		//생성할 파일 검사 
		checkFile = new CheckMethod(formWizard.getFileSelectList());
		
		//생성하기 위해 선택된 메소드들
		for(WizardResultModel rm : checkList){
			for(SqlMapVo sqlMapVo : formWizard.getNewMethodList()){
				if(rm.name.equals(sqlMapVo.getId())){
					
					sqlMapVo.setMethodType(rm.type);
					sqlMapVo.setPaging(rm.isPaging);
					sqlMapVo.setFile(rm.isFile);
					sqlMapVo.setEditer(rm.isEditer);
					sqlMapVo.setIdAuto(rm.isIdAuto);
					sqlMapVo.setParamObject(rm.isParamObject);
					
					actionMethodList.add(sqlMapVo);
				}
			}
		}
		
		sourceInfo.setPkName(sqlXml.getPkName());
		
		/*
		String pkName = "";
		
		IField[] fields = sqlXml.getModelFields().get(sqlXml.getFirstModel());
		
		int i=0;
		for(IField filed : fields){
			
			if(i == 1){
				pkName = filed.getElementName();
			}
			
			i++;
		}
		
		sourceInfo.setPkName(pkName);
		*/
	}
	
	public MClass makeDaoImpl(){
		
		MClass m = null;
		if(checkFile.isDao()){
			m = MakeDaoImpl.make(sourceInfo, sqlXml, actionMethodList); 
		}
		return m; 
	}
	
	public MClass makeDao(MClass daoImplClass){
		MClass m = null;
		
		if(checkFile.isDao()){
			m = MakeDao.make(sourceInfo, sqlXml, daoImplClass);
		}
		
		return m; 
	}
	
	public void makeDaoTest(MClass daoClass){
		 if(checkFile.isDaoTest()){
			 MakeDaoTest.make(sourceInfo, daoClass, actionMethodList, sqlXml, "dao");
		 }
	}
	
	public MClass makeServiceImpl(MClass daoClass){
		MClass m = null;
		
		if(checkFile.isService()){
			m = MakeServiceImpl.make(sourceInfo, sqlXml, daoClass);
		}
		return m; 
	}
	
	public MClass makeService(MClass serviceImplClass){
		MClass m = null;
		
		if(checkFile.isService()){
			m = MakeService.make(sourceInfo, sqlXml, serviceImplClass);
		}
		return m; 
	}
	
	public void makeServiceTest(MClass serviceClass){
	   if(checkFile.isServiceTest()){
		   MakeDaoTest.make(sourceInfo, serviceClass, actionMethodList, sqlXml, "service");
	   }
	}
	
	public void makeWeb(MClass serviceClass){
		 if(checkFile.isWeb()){
			 MakeController.make(sourceInfo, sqlXml, actionMethodList, serviceClass);
		 }
	}

	public void makeHtml(){
		  if(checkFile.isHtml()){
			  MakeHtml.make(sourceInfo, actionMethodList);
		  }
	}
}
