// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   HandlerChain.java

package zom.dong.sourceconverter.handler;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.wizard.FormWizard;

// Referenced classes of package zom.dong.sourceconverter.handler:
//            GetSourceInfo, HotSource

public class HandlerChain
{

    public HandlerChain()
    {
    }

    public static void action(IFile handlerChainFile, Shell shell, String selectedText)
    {
        IProject iproject = handlerChainFile.getProject();
        if(handlerChainFile.getFileExtension().equalsIgnoreCase("xml"))
        {
            GetSourceInfo sourceInfo = new GetSourceInfo(handlerChainFile);
            FormWizard formWizard = new FormWizard();
            formWizard.init(sourceInfo, handlerChainFile, selectedText);
            WizardDialog wizardDialog = new WizardDialog(shell, formWizard);
            
            if(wizardDialog.open() != 0)
                return;
            
            HotSource source = new HotSource(sourceInfo, formWizard);
            
            MClass daoImplClass = source.makeDaoImpl();
            MClass daoClass = source.makeDao(daoImplClass);
            source.makeDaoTest(daoClass);
            
            MClass serviceImplClass = source.makeServiceImpl(daoClass);
            MClass serviceClass = source.makeService(serviceImplClass);
            source.makeServiceTest(serviceClass);
            
            source.makeWeb(serviceClass);
            source.makeHtml();
            
            try
            {
                iproject.refreshLocal(2, null);
                getIProject(sourceInfo.getHtmlPath()).refreshLocal(2, null);
            }
            catch(CoreException e)
            {
                e.printStackTrace();
            }
        } else
        {
            MessageDialog.openInformation(shell, "Information", "sql xml 파일을 선택해주십시오.");
        
        }
    }

    private static IProject getIProject(String fileUrl)
    {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        IPath path = new Path(fileUrl);
        IContainer files[] = workspace.getRoot().findContainersForLocation(path);
        IProject project = null;
        IContainer aicontainer[];
        int j = (aicontainer = files).length;
        for(int i = 0; i < j; i++)
        {
            IContainer container = aicontainer[i];
            project = container.getProject();
        }

        return project;
    }
}
