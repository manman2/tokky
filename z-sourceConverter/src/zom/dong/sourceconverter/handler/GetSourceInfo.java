package zom.dong.sourceconverter.handler;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.util.UtilUnit;

public class GetSourceInfo {

	private IFile file;
	
	private String moduleName;	//모듈 이름
	private String objectName;	//해당 객체 이름
	private String htmlPath;	
	private String function;	//기능
	private String pkName;		//pk 이름
	
	
	private Map<String, String> allMap = new HashMap<String, String>();
	
	GetSourceInfo(IFile handlerChainFile){
		
		file = handlerChainFile;
		
		String[] funcs = file.getName().split("[_]");
		
		function = funcs[0];
		
		if(funcs.length > 2){
			
			for(int i=1; i<funcs.length-1; i++){
	
				function += UtilUnit.firstUpper(funcs[i]);
			}
			
		}

		
		//function = file.getName().substring(0, file.getName().indexOf("_")) ;
		
		getPath("daoPath");
		getPath("daoImplPath");
		getPath("servicePath");
		getPath("serviceImplPath");
		getPath("modelPath");
		getPath("webPath");
		getPath("daoTestPath");
		getPath("serviceTestPath");
		
		String path = allMap.get("daoPath");
		
		setHtmlPath(path.substring(0, path.lastIndexOf("/src"))+"/WebContent/WEB-INF/view/"+UtilUnit.firstLower(getProjectName()) + "/"+UtilUnit.firstLower(getModuleName()));
		
	}
	
	public String getModulePath(){
		String path = allMap.get("daoPath");
		
		return path.substring(0, path.lastIndexOf("/"));
	}
	
	/**
	 * 프로젝트 이름 가져오기
	 * @return
	 */
	public String getProjectName(){
		
		String mPackage = getPackagePath("daoPath");
		String[] moduleNames = mPackage.split("[.]");  
		String projectName = moduleNames[moduleNames.length-3];
		
		return projectName;
	}
		
	/**
	 * 모듈 이름 기져오기
	 * @return
	 */
	public String getModuleName(){
		
		String mPackage = getPackagePath("daoPath");
		String[] moduleNames = mPackage.split("[.]");  
		String moduleName = moduleNames[moduleNames.length-2];
		
		return moduleName;
	}
	
	public String getObjectName(){
		return objectName;
	}
	
	//모듈 패키지
	public String getPackage(){
		String mPackage = getPackagePath("daoPath");
		mPackage = mPackage.substring(0, mPackage.lastIndexOf("."));
		return mPackage;
	}
		
	// Dao 경로
	public String getDaoPath() {
		return allMap.get("daoPath");
	}
	
	public void setDaoPath(String path) {
		allMap.put("daoPath", path.replaceAll("[\\\\]", "/"));
	}

	//Dao 패키지 경로
	public String getDaoPackage(){
		return getPackagePath("daoPath");
	}
	
	//Dao 패키지 경로
	public String getDaoFile(){
		return  allMap.get("daoFile");
	}
	
	public void setDaoFile(String file){
		allMap.put("daoFile", file);
	}
	
	// DaoImpl 경로
	public String getDaoImplPath() {
		return allMap.get("daoImplPath");
	}
	
	public void setDaoImplPath(String path) {
		allMap.put("daoImplPath", path.replaceAll("[\\\\]", "/"));
	}

	//DaoImpl 패키지 경로
	public String getDaoImplPackage(){
		return getPackagePath("daoImplPath");
	}
	
	//DaoImpl 패키지 경로
	public String getDaoImplFile(){
		return  allMap.get("daoImplFile");
	}
	
	public void setDaoImplFile(String file){
		allMap.put("daoImplFile", file);
	}
	
	// Service 경로
	public String getServicePath() {
		return allMap.get("servicePath");
	}
	
	public void setServicePath(String path) {
		allMap.put("servicePath", path.replaceAll("[\\\\]", "/"));
	}

	//Service 패키지 경로
	public String getServicePackage(){
		return getPackagePath("servicePath");
	}
	
	//Service 패키지 경로
	public String getServiceFile(){
		return  allMap.get("serviceFile");
	}
	
	public void setServiceFile(String file){
		allMap.put("serviceFile", file);
	}
	
	// ServiceImpl 경로
	public String getServiceImplPath() {
		return allMap.get("serviceImplPath");
	}
	
	public void setServiceImplPath(String path) {
		allMap.put("serviceImplPath", path.replaceAll("[\\\\]", "/"));
	}

	//ServiceImpl 패키지 경로
	public String getServiceImplPackage(){
		return getPackagePath("serviceImplPath");
	}
	
	//ServiceImpl 패키지 경로
	public String getServiceImplFile(){
		return  allMap.get("serviceImplFile");
	}
	
	public void setServiceImplFile(String file){
		allMap.put("serviceImplFile", file);
	}
	
	//Model 경로
	public String getModelPath() {
		return allMap.get("modelPath");
	}
	
	public void setModelPath(String path) {
		allMap.put("modelPath", path.replaceAll("[\\\\]", "/"));
	}

	//Model 패키지 경로
	public String getModelPackage(){
		return getPackagePath("modelPath");
	}
	
	//Model 패키지 경로
	public String getModelFile(){
		return  moduleName;
	}
	
	//web 경로
	public String getControllerPath() {
		return allMap.get("webPath");
	}
	
	public void setWebPath(String path) {
		allMap.put("webPath", path.replaceAll("[\\\\]", "/"));
	}

	//web 패키지 경로
	public String getWebPackage(){
		return getPackagePath("webPath");
	}
	
	//web 패키지 경로
	public String getControllerFile(){
		return  allMap.get("webFile");
	}
	
	public void setWebFile(String file){
		allMap.put("webFile", file);
	}
	
	//TestDao 경로
	public String getDaoTestPath() {
		
		String path = allMap.get("daoPath").replace("/src/main/", "/src/test/").replace("/src/com/", "/src/test/");
		
		return path;
	}
	
	public void setDaoTestPath(String path) {
		allMap.put("daoTestPath", path.replaceAll("[\\\\]", "/"));
	}

	//TestDao 패키지 경로
	public String getDaoTestPackage(){
		return getPackagePath("daoTestPath");
	}
	
	//TestDao 패키지 경로
	public String getDaoTestFile(){
		String path = allMap.get("daoTestFile").replace("/src/main/", "/src/test/").replace("/src/com/", "/src/test/");
		
		return  path;
	}
	
	public void setDaoTestFile(String file){
		allMap.put("daoTestFile", file);
	}
	
	//TestService 경로
	public String getServiceTestPath() {
		
		String path = allMap.get("servicePath").replace("/src/main/", "/src/test/").replace("/src/com/", "/src/test/");
		
		return path;
	}
	
	public void setServiceTestPath(String path) {
		allMap.put("serviceTestPath", path.replaceAll("[\\\\]", "/"));
	}

	//TestService 패키지 경로
	public String getServiceTestPackage(){
		return getPackagePath("serviceTestPath");
	}
	
	//TestService 패키지 경로
	public String getServiceTestFile(){
		return  allMap.get("serviceTestFile");
	}
	
	public void setServiceTestFile(String file){
		allMap.put("serviceTestFile", file);
	}
		
	
	//파일 경로
	private void getPath(String pathName){
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		 String filePath = store.getString(pathName).trim();
		 String preName = pathName.replace("Path", "");
		 
	     String[] pathItem = filePath.split("[/]");
	      
	      IContainer container = file.getParent() ;
	      String folderPath = "";
	      
	      for(String item : pathItem){
	    	  if(item.equals("..")){
	    		  container = container.getParent();
	    	  } else {
	    		 
	    		  if(folderPath.equals("")){
	    			  folderPath = container.getLocation().toString() +"/" + item;
	    		  } else {
	    			  folderPath += "/" + item;
	    		  }
	    		  
	    	  }
	      }
	      
	      allMap.put(preName+"Path", folderPath);
	      
	      String fixFleName = store.getString(preName+"File").trim();
	      
	      if(StringUtils.isNotEmpty(fixFleName)){
	    	  String fileName = function+fixFleName;
	    	  allMap.put(preName+"File", fileName);
	      }
	      
	}
	
	
	
	/**
	 * 패키지 조합해서 가져오기
	 * @param unitName
	 * @return
	 */
	private String getPackagePath(String path){
		
		String projectPath = file.getProject().getLocation().toString();
		String packagePath = allMap.get(path).replace(projectPath+"/src/", "").replace("main/java/","").replace("test/java/","");
		return packagePath.replaceAll("[/]", ".");
	}
	
	public String getHtmlPath() {
		return htmlPath;
	}

	public void setHtmlPath(String htmlPath) {
		this.htmlPath = htmlPath;
	}

	public String getPkName() {
		return pkName;
	}

	public void setPkName(String pkName) {
		this.pkName = pkName;
	}

}
