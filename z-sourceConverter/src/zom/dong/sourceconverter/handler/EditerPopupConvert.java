// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EditerPopupConvert.java

package zom.dong.sourceconverter.handler;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.*;
import zom.dong.sourceconverter.Activator;

// Referenced classes of package zom.dong.sourceconverter.handler:
//            HandlerChain

public class EditerPopupConvert   implements IObjectActionDelegate{

    public EditerPopupConvert()
    {
    }

    public void setActivePart(IAction action, IWorkbenchPart targetPart)
    {
        shell = targetPart.getSite().getShell();
    }

    public void run(IAction action)
    {
        try
        {
            IEditorPart editorPart = Activator.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
            String selectedText = null;
            IEditorSite iEditorSite = editorPart.getEditorSite();
            if(iEditorSite != null)
            {
                ISelectionProvider selectionProvider = iEditorSite.getSelectionProvider();
                if(selectionProvider != null)
                {
                    ISelection iSelection = selectionProvider.getSelection();
                    if(!iSelection.isEmpty())
                        selectedText = ((ITextSelection)iSelection).getText();
                    IEditorInput input = editorPart.getEditorInput();
                    IFile handlerChainFile = (IFile)input.getAdapter(IFile.class);
                    HandlerChain.action(handlerChainFile, shell, selectedText);
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void selectionChanged(IAction iaction, ISelection iselection)
    {
    }

    private Shell shell;
}
