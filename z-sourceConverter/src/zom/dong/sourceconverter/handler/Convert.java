package zom.dong.sourceconverter.handler;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.handlers.HandlerUtil;

import zom.dong.sourceconverter.dialog.FirstDialog;
import zom.dong.sourceconverter.dialog.SecondDialog;
import zom.dong.sourceconverter.handler.make.MakeDao;
import zom.dong.sourceconverter.handler.make.MakeDaoImpl;
import zom.dong.sourceconverter.handler.make.MakeDaoTest;
import zom.dong.sourceconverter.handler.make.MakeHtml;
import zom.dong.sourceconverter.handler.make.MakeService;
import zom.dong.sourceconverter.handler.make.MakeServiceImpl;
import zom.dong.sourceconverter.handler.make.MakeController;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.CheckMethod;
import zom.dong.sourceconverter.util.UtilUnit;
import zom.dong.sourceconverter.wizard.FormWizard;


/**
 * 쿼리 결과가 list형식을 경우 id명은 list로 시작하여야 함.예) listQna
 * daoImpl에서 객체파라미터 sql에 넘길때 map은 최대한 쓰지말고 객체로 바인딩 하여 쓴다.
 * 	- 자동으로 변수형 잡기 힘듬. 맵 만들때 숫자형을 인식하기위해 변수가 line, count가 들어가면 int로 인식함.
 * 
 * @author 이동희
 *
 */
public class Convert extends AbstractHandler {
	
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection)HandlerUtil.getActiveMenuSelection(event);
        Object firstElement = selection.getFirstElement();
        if(firstElement instanceof IFile)
        {
            IFile handlerChainFile = (IFile)firstElement;
            HandlerChain.action(handlerChainFile, HandlerUtil.getActiveShell(event), null);
        } else
        {
            MessageDialog.openInformation(HandlerUtil.getActiveShell(event), "Information", "SQl xml \uD30C\uC77C\uC744 \uC120\uD0DD\uD574\uC8FC\uC138\uC694.");
        }
        
        /*
        MessageConsole myConsole = findConsole("System Output");
		   MessageConsoleStream out = myConsole.newMessageStream();
		   out.println("Hello from Generic console sample action");
		   */
        return null;
			 
			 
			 
	        
/*
  				//refresh
					CommonNavigator view = (CommonNavigator)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView("org.eclipse.ui.navigator.ProjectExplorer");
					CommonViewer viewer = view.getCommonViewer();
					viewer.refresh();

	  		WorkspaceView wv=(WorkspaceView)PlatformUI.getWorkbench(). 
				    			getActiveWorkbenchWindow().getActivePage().findView("org.opendatafoundation.dext.view.WorkspaceView");
							wv.refresh();
							
			if (directory != null && directory.length() > 0) {
				newDirectory = !(MessageDialog.openQuestion(HandlerUtil.getActiveShell(event), "Question","Use the previous output directory?"));
			}
			if (newDirectory) {
				directory = fileDialog.open();

			}
			if (directory != null && directory.length() > 0) {
				analyze(cu);
				setPersistentProperty(res, path, directory);
				write(directory, cu);
			}
*/

	}
	

	/**
	 * 파일 읽어서 ICompliationUnit 으로 변경
	 * @param fileUrl
	 * @return
	 */
	private ICompilationUnit getICompliationUnit(String fileUrl){
		
		IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
		IPath path = new Path(fileUrl);
		IFile[] files = workspace.getRoot().findFilesForLocation(path);
		
		IFile javaFile = null;
		for(IFile file : files ){
			javaFile = file;
		}
		
		return (ICompilationUnit) JavaCore.createCompilationUnitFrom(javaFile);
	}
	
	
	private IProject getIProject(String fileUrl){
		
		IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
		IPath path = new Path(fileUrl);
		IContainer[] files = workspace.getRoot().findContainersForLocation(path);
		
		IProject project = null;
		
		for(IContainer container : files ){
			project = container.getProject();
		}
		
		return project;
	}

	protected String getPersistentProperty(IResource res, QualifiedName qn) {
		try {
			return (String) res.getPersistentProperty(qn);
		} catch (CoreException e) {
			return "";
		}
	}


	protected void setPersistentProperty(IResource res, QualifiedName qn,
			String value) {
		try {
			res.setPersistentProperty(qn, value);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}


	//콘솔 색깔 설정
	private void settingMessgeConsole() {
		//콘솔창
		 MessageConsole console = new MessageConsole("Console", null);  
		 
		 MessageConsoleStream new_out = console.newMessageStream();
		 new_out.setColor(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		 System.setOut(new PrintStream(new_out));

		 // 표준 에러는 붉은색으로 
		 MessageConsoleStream new_err = console.newMessageStream();
		 new_err.setColor(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
		 System.setErr(new PrintStream(new_err));

		 ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { console }); 
		
	}
	
	 private MessageConsole findConsole(String name) {
	      ConsolePlugin plugin = ConsolePlugin.getDefault();
	      IConsoleManager conMan = plugin.getConsoleManager();
	      IConsole[] existing = conMan.getConsoles();
	      for (int i = 0; i < existing.length; i++)
	         if (name.equals(existing[i].getName()))
	            return (MessageConsole) existing[i];
	      //no console found, so create a new one
	      MessageConsole myConsole = new MessageConsole(name, null);
	      conMan.addConsoles(new IConsole[]{myConsole});
	      return myConsole;
	   }
	
}
