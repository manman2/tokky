package zom.dong.sourceconverter.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.jdom.Element;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.model.Param;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

/**
 * Sql Xml 파싱
 * @author lee
 *
 */
public class ParseSqlXml {
	
	private List<SqlMapVo> voList = new ArrayList<SqlMapVo>();							//sql resultMap 이름: 클래스명 저장
	private Map<String, IField[]> modelfieldsMap =  new  HashMap<String, IField[]>();	//모델 객체
	private String namespace;															//네임스페이스
	private  Map<String, Map<String, Object>> sqlResultMap = new HashMap<String, Map<String, Object>>();	//resultMap 저장
	private String firstModel;	//처음 모델이름
	private Map<String, String> typeAliasClassMap = new HashMap<String, String>();	//typeAlias Class 저장
	//private Map<String, String> typeAliasList = new HashMap<String, String>();					//typeAlias List
	private IFile handlerChain;
	private String searchModelName;		//search 모델 이름
	
	Map<String, Object> includeMap = new HashMap<String, Object>();	//include 파라미터 저장
	
	int pkCnt = 10;
	String pkName = "";

	public ParseSqlXml(IFile handlerChainFile, String modelPath) throws Exception {
		
		this.handlerChain = handlerChainFile;
		
		Element root =  UtilUnit.getXml(handlerChainFile.getLocation().toFile());
		
        namespace =  root.getAttribute("namespace").getValue();
       
        @SuppressWarnings("unchecked")
		 List<Element> children = root.getChildren();
       
        //Set<String> typeAliasSet = new HashSet<String>();	//중복 제거하기 위해 set 사용
        for(Element element : children){
       	
	       	if (element.getValue() != null){
	       		
	       		if(element.getName().equals("insert") ||element.getName().equals("select") || element.getName().equals("update") || element.getName().equals("delete")  || element.getName().equals("procedure")){
	       			
	       			String id = element.getAttributeValue("id");
	       			
	       			//sqlMap 파싱
	       			SqlMapVo sqlMapVo = new SqlMapVo();
	       			
	       			sqlMapVo.setId(id);
	       			
	       			String pClass = element.getAttributeValue("parameterClass");
	       			pClass = UtilUnit.lastWord(pClass, ".");
	       			/*
	       			if(pClass != null && pClass.indexOf(".")>-1 && pClass.indexOf("String")==-1 && pClass.indexOf("Integer")==-1){
	       				//typeAliasSet.add(pClass);
	       				pClass = UtilUnit.lastWord(pClass, ".");
       				}
       				*/
	       			
	       			
	       			sqlMapVo.setParamName(pClass);
	       			
	       			if(typeAliasClassMap.get(pClass) != null){
	       				pClass = UtilUnit.lastWord(typeAliasClassMap.get(pClass), ".");
	       			}
	       			
	       			//파라미터가 있으면 클래스 저장
	       			sqlMapVo.setParamType(pClass);
	       			
	       			
	       			String resultObject = "";
	       			
	       			String value = element.getText();
	       			
	       			String resultMap = element.getAttributeValue("resultMap");
	       			String resultClass = element.getAttributeValue("resultClass");
	       			
	       			//resultMap이 있으면
	       			if(StringUtils.isNotEmpty(resultMap)){
	       				
	       				Map<String, Object> rMap = sqlResultMap.get(resultMap);
	       				String clazz = (String)rMap.get("clazz");

	       				resultObject = UtilUnit.lastWord(clazz, "."); 
	       				
	       				//select colum 저장 - html 만들때 사용
	       				sqlMapVo.setResultField((List<String>)rMap.get("propertyList"));
	       				
	       				//resultMap에 extends가 있으면
	       				if(rMap.get("extends") != null){
	       					Map<String, Object> exMap = sqlResultMap.get( UtilUnit.lastWord((String)rMap.get("extends"), "."));
	       					List<String> propertyList = (List<String>)exMap.get("propertyList");
	       					
	       					for(String pro : propertyList){
	       						sqlMapVo.addResultField(pro);
	       					}
	       					
	       				}
						
	       			} else if(StringUtils.isNotEmpty(resultClass)){
	       				
	       				resultObject = UtilUnit.lastWord(resultClass, ".");
	       				
	       				if(typeAliasClassMap.get(resultObject) != null){
	       					resultObject = UtilUnit.lastWord(typeAliasClassMap.get(resultObject), ".");
		       			}
	       				
	       				//include가 있으면
	       				String includeId = "";
		       			List<Element> rr = element.getChildren();
		       			for(Element re : rr){
			       			if(re.getName().equals("include")){
		       		    		includeId = re.getAttributeValue("refid");
		       		    	} 
			       		}
		       			
		       			if(includeMap.get(includeId) != null){
		       				sqlMapVo.setResultField((List<String>)includeMap.get(includeId));
		       			}
	       				
		       			if(!resultObject.equalsIgnoreCase("int") && !resultObject.equalsIgnoreCase("Integer") && !resultObject.equalsIgnoreCase("string")){
		       				Pattern pt = Pattern.compile("(?i)SELECT(.*)(?i)FROM", Pattern.DOTALL);
			       			Matcher mt = pt.matcher(value);
			       			
			       			while (mt.find()) {
			       				String[] params = mt.group(1).split("(?i)FROM");
		       					for(String pr : params){
		       						
		       						int ind = pr.toLowerCase().indexOf("select ");
		       						if(ind > -1){
		       							pr = pr.substring(pr.toLowerCase().indexOf("select ")+7);
		       						}
		       						
		       						String[] fies = pr.split("[,]");
		       						
		       						for(String fie : fies){
		       							
		       							if(fie.toLowerCase().indexOf("rownum") == -1){
		       								String[] fi = fie.trim().split(" ");
			       							String field = fi[fi.length-1];
			       							
			       							if(!field.contains("*") ){
			       								sqlMapVo.addResultField(field.substring(field.indexOf(".")+1));
			       							}
		       							}
		       						}
			       				}
			       			}
		       			}
	       			}
	       			
	       			sqlMapVo.setResultObject(resultObject);
	       			
	       			sqlMapVo.setType(element.getName());
	       			
	       			List<String> paramList = new ArrayList<String>();
	       			Set<String> paramSet = new HashSet<String>();	//중복 제거하기 위해 사용
	       			List<String> iterateList = new ArrayList<String>();
	       			
	       			String selectKey = "";
	       			
	       			@SuppressWarnings("unchecked")
	       			List<Element> ce = element.getChildren();
	       			
	       			Pattern pattern = Pattern.compile("#[\\S]+#");
	       		 
	       		    for(Element ee : ce){
	       		    	
	       		    	if(ee.getName().equals("include")) continue;
	       		    	
	       		    	String property = ee.getAttributeValue("property");
	       		    	if(property == null) property = ee.getAttributeValue("compareProperty");
	       		    	
	       		    	if(ee.getName().equals("iterate")){
	       		    		
	       		    		String iterateVal = "";
	       		    		
	       		    		if(property != null){
	       		    			iterateVal = property;
	       		    		} else {
	       		    			
	       		       			Matcher matcher2 = pattern.matcher(ee.getText());
	       		       			
		       		       		while (matcher2.find()) {
		       		       			iterateVal = matcher2.group().replaceAll("#", "");
		       		       			break;
		    	       			}
	       		    		}
	       		    		
	       		    		paramSet.add(iterateVal.replace("[]",""));
	       		    		iterateList.add(iterateVal.replace("[]",""));
	       		    		
	       		    	} else {
	       				
	       		    		if(property != null) paramSet.add(property);
		       				value += ee.getText();
		       				
		       				if(ee.getName().equals("selectKey")){
			       				sqlMapVo.setInsertKey(true);
			       				selectKey = ee.getAttributeValue("keyProperty");
			       			}
	       		    	}
	       		    	
		       	    }
		       		  
		       		sqlMapVo.setIterateList(iterateList);
	       			
	       			Matcher matcher = pattern.matcher(value);
	       			
	       			while (matcher.find()) {
	       				String sqlParam = matcher.group().replaceAll("#", "");
	       				/*
	       				boolean isChk = false;
	       				for(Param p : paramList){
	       					if(p.getName().equals(sqlParam)){
	       						isChk = true; break;
	       					}
	       				}
	       				
	       				if(!isChk){
	       					Param param= new Param();
		       				param.setName(sqlParam);
		       				paramList.add(param);
	       				}
	       				*/
	       				paramSet.add(sqlParam);
	       			}
	       			
	       			paramSet.remove(selectKey);	//selectKey는 쿼리에서 생성하기때문에 파라미터에선 삭제
	
	       			paramList.addAll(paramSet);
	       			
	       			
	       			if(pkCnt > paramList.size() && paramList.size() > 0){
	       				pkName = paramList.get(0);
	       			}
	       				
	       			sqlMapVo.setParamList(paramList);
	       			
	       			setVoList(sqlMapVo);
	       			
	   			} else if(element.getName().equals("resultMap")) {
	   				
	   				String rClass = element.getAttributeValue("class");
	   				
	   				Map<String, Object> resultMap = new HashMap<String, Object>();
	   				resultMap.put("clazz", rClass);
	   				
	   				//select 변수
	   				List<Element> resultElement = element.getChildren();
	   	       		 
	   				List<String> list = new ArrayList<String>();
		       		for(Element ee : resultElement){
		       			 String property = ee.getAttributeValue("property");
		       			list.add(property);
		       		}
		       		
		       		resultMap.put("propertyList", list);
		       		
		       		resultMap.put("extends", element.getAttributeValue("extends"));
		       		
		       		//resultMap 저장
		       		sqlResultMap.put(element.getAttributeValue("id"), resultMap);
	   				
	   			} else if(element.getName().equals("typeAlias")){
	   				
	   				String type = element.getAttributeValue("type");
	   				String alias = element.getAttributeValue("alias");
	   				//typeAliasSet.add(type);
	   				
	   				typeAliasClassMap.put(alias, type);
	   				
	   				String model = UtilUnit.lastWord(type, ".");
	   				
	   				//처음 객체 저장 - 대표 객체로 사용
	   				if(StringUtils.isEmpty(firstModel)){
	   					firstModel = model;
	   				}
	   				
	   				
	   				//모델 이클립스 객체로 저장
	   				IField[] fields = modelfields(type.replaceAll("[.]", "/") + ".java");
	   				modelfieldsMap.put(alias, fields);
	   				
	   				
	   				//서치 모델경로 저장
	   				if(alias.indexOf("SearchCondition") > -1){
	   					searchModelName = alias;
	   				}
	   				
	   			}  else if (element.getName().equals("sql")){
	   				
	   				List<String> list = new ArrayList<String>();
	   				
	   				String[] params =element.getText().split("[,]");
       				for(String pr : params){
       					String[] pp = pr.split(" ");
       					if(pp.length > 1){
       						list.add(pr.split(" ")[1]);
       					}
       				}
	   				
	   				String id = element.getAttributeValue("id");
	   				
	   				if(list.size() > 0){
	   					includeMap.put(id, list);
	   				}
	   				
	   				
	   			}
	       		
	       	}
       }
        
       // typeAliasList.addAll(typeAliasSet);
	}
	
	
	private void setVoList(SqlMapVo sqlMapVo){
		voList.add(sqlMapVo);
	}
	
	public List<SqlMapVo> getVoList(){
		return voList;
	}
	
	public String getPkName(){
		return pkName;
	}
	
	/*
	public List<String> getTypeAliasList(){
		return typeAliasList;
	}
	*/
	
	/**
	 * 모델 필드들 넣기
	 * @param id
	 * @param fields
	 */
	/*
	private void setModelfields(String id, IField[] fields){
		modelfieldsMap.put(id, fields);
	}
	*/
	
	public Map<String, String> getTypeAliasClassMap(){
		return this.typeAliasClassMap;
	}
	
	
	
	
	
	/**
	 * 모델 필드들 가져오기
	 * @return
	 */
	public Map<String, IField[]> getModelFields(){
		return modelfieldsMap;
	}
	
	public String getNamespace(){
		return namespace;
	}
	
	public String getFirstModel(){
		return firstModel;
	}
	
	
	/**
	 * 모델객체에서 필드만 뽑아옴. 변수의 타입 알아오기 위해
	 */
	private IField[] modelfields(String fileUrl){
		
		ICompilationUnit  icu =  getICompliationUnit(fileUrl);
		
		IField[] fields = null;
	
		try {
			
			IType type = null;
			IType[] allTypes;
			allTypes = icu.getAllTypes();
			
			for (int t = 0; t < allTypes.length; t++) {
				if (Flags.isPublic((allTypes[t].getFlags()))) {
					type = allTypes[t];
					break;
				}
			}

			fields = type.getFields();
			
		} catch (JavaModelException e) {
			
			// MessageDialog.openInformation(HandlerUtil.getActiveShell(gEvent),
			//			"Information", "not file Model File : \n"+fileUrl);
			System.err.println("not file :"+fileUrl);
			e.printStackTrace();
		}
		
		
		return fields;
	}
	
	/**
	 * 파일 읽어서 ICompliationUnit 으로 변경
	 * @param fileUrl
	 * @return
	 */
	private ICompilationUnit getICompliationUnit(String fileUrl){
		
		 IProject iproject = handlerChain.getProject();
		
		//IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
		//IPath path = new Path(fileUrl);
		
		//IFile[] files = workspace.getRoot().findFilesForLocation(path);
		
		IFile javaFile = iproject.getFile("/src/"+fileUrl);
		/*
		for(IFile file : files ){
			javaFile = file;
		}
		*/
		return (ICompilationUnit) JavaCore.createCompilationUnitFrom(javaFile);
	}
	
	public Map<String, Map<String,Object>> getResultMap(){
		return  sqlResultMap;
	}
	
	public String getSearchModelName(){
		
		return this.searchModelName;
	}
	
}
