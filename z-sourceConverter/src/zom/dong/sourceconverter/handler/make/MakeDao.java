package zom.dong.sourceconverter.handler.make;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeDao {

	/**
	 * 
	 * @param daoImplClass
	 * @param iFile
	 */
	public static MClass make(GetSourceInfo sourceInfo, ParseSqlXml sqlXml, MClass daoImplClass){
		
		System.out.println("-- Make Dao Start");
		
		UtilUnit.makeDir(sourceInfo.getDaoPath());
		
		//존재여부
		String fileUrl = sourceInfo.getDaoPath() + "/" + sourceInfo.getDaoFile() + ".java";
		
		MClass daoClass = new MClass();
		
		
		//기존 파일 없어 새로 등록
		if(!UtilUnit.isExistFile(fileUrl)){
		
			daoClass.setPackageName(sourceInfo.getDaoPackage());
			
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			
			 String importClass = store.getString("daoImportClass").trim();
			 
			 String[] imports = importClass.split("[;]");
			 
			 for(String imp : imports){
				 if(StringUtils.isNotEmpty(imp)){
					 daoClass.addImportList(imp);
				 }
			 }
			
			//import 생성
			for(String typeAlias : sqlXml.getTypeAliasClassMap().values()){
				 daoClass.addImportList(typeAlias);
			}
			
			daoClass.setClassType("interface");
			daoClass.setClassName(sourceInfo.getDaoFile());
			
			String classExtends = store.getString("daoClassExtends").trim();
			classExtends = classExtends.replace("{model}", sqlXml.getFirstModel());
			
			daoClass.setExtendsClass(classExtends);
		
		}
		
		//메소드 생성
		for(MType method : daoImplClass.getMethodList()){
			
			MType iMethod = new MType();
			iMethod.setDataType(method.getDataType());
			iMethod.setName(method.getName());
			iMethod.setParameterList(method.getParameterList());
			
			daoClass.addMethodList(iMethod);
			
		}
		
		//dao impl 파일 생성
		UtilUnit.makeJavaFile(fileUrl, daoClass);
		
		System.out.println("-- Make Dao OK");
		
		return daoImplClass;
	}
}
