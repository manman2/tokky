package zom.dong.sourceconverter.handler.make;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeHtml {

	/**
	 * web make
	 * @param sqlMapVoList
	 * @param iFile
	 * @param namespace
	 */
	public static void make(GetSourceInfo sourceInfo, List<SqlMapVo> newMethodList) {
		
		System.out.println("-- Make Html Start");
		

		UtilUnit.makeDir(sourceInfo.getHtmlPath());
		
		
		//기존 메소드 가져오기
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		 String listNaming = store.getString("sqlNameList").trim();
		 String sqlNameGet = store.getString("sqlNameGet").trim();
		 String sqlNameCreate = store.getString("sqlNameCreate").trim();
		
		//함수 생성
		for(SqlMapVo sqlMapVo : newMethodList){
			
			String resultObjectLower = (sqlMapVo.getResultObject().toLowerCase().equals("hashmap") || sqlMapVo.getResultObject().toLowerCase().equals("map")) ? "item" : UtilUnit.firstLower(sqlMapVo.getResultObject());
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<%@ page language=\"java\" contentType=\"text/html; charset=UTF-8\" pageEncoding=\"UTF-8\"%>\n");
			sb.append("<%@ taglib uri=\"http://www.springframework.org/tags\" prefix=\"spring\" %>\n");
			sb.append("<%@ taglib uri=\"http://www.springframework.org/tags/form\" prefix=\"form\" %>\n");
			sb.append("<%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\" %>\n");
			sb.append("<%@ taglib uri=\"http://java.sun.com/jsp/jstl/fmt\" prefix=\"fmt\" %>\n");
			sb.append("<%@ taglib uri=\"http://java.sun.com/jsp/jstl/functions\" prefix=\"fn\" %>\n\n");
			
			String fileName = "";
			if(sqlMapVo.getId().startsWith(sqlNameGet)){ 
			
				
				sb.append("\t<div>\n");
				sb.append("\t\t<table>\n");
					
				for(String field : sqlMapVo.getResultField()){
					sb.append("\t\t\t<tr>\n");
					sb.append("\t\t\t\t<td>"+field+"</td><td><input type=\"text\" name=\""+field+"\" value=\"${"+UtilUnit.firstLower(sqlMapVo.getResultObject())+"."+field+"}\"/></td>\n");
					sb.append("\t\t\t</tr>\n");
				}
					
				sb.append("\t\t</table>\n");
				sb.append("\t</div>\n");
				
				fileName = sqlMapVo.getId();
				
			} else if(sqlMapVo.getId().startsWith(sqlNameCreate) ){ 
				

				sb.append("\t<div>\n");
				sb.append("\t\t<form name=\""+UtilUnit.firstLower(sqlMapVo.getParamType())+"Form\" method=\"post\" action=\"<c:url value='/"+sourceInfo.getProjectName()+"/"+sourceInfo.getModuleName()+"/"+sqlMapVo.getId()+".do'/>\" >\n");
				sb.append("\t\t\t<table>\n");
					
				for(String field : sqlMapVo.getParamList()){
					sb.append("\t\t\t\t<tr>\n");
					sb.append("\t\t\t\t\t<td>"+field+"</td><td><input type=\"text\" name=\""+field+"\" value=\"\"/></td>\n");
					sb.append("\t\t\t\t</tr>\n");
				}
					
				sb.append("\t\t\t</table>\n");
				sb.append("\t\t\t<div><input type=\"submit\" value=\"submit\"/></div>\n");
				sb.append("\t\t</form>\n");
				sb.append("\t</div>\n");
				
				fileName = sqlMapVo.getId()+"Form";
				
			} else if(sqlMapVo.getId().startsWith(listNaming)){		
				//list

				sb.append("\t<div>\n");
				sb.append("\t\t<table>\n");
				sb.append("\t\t<c:forEach var=\""+resultObjectLower+"\" items=\"${"+UtilUnit.firstLower(sqlMapVo.getResultObject())+"List}\" varStatus=\"loop\">\n");
				
				for(String field : sqlMapVo.getResultField()){
					sb.append("\t\t\t<tr>\n");
					sb.append("\t\t\t\t<td>${"+resultObjectLower+"."+field+"}</td>\n");
					sb.append("\t\t\t</tr>\n");
				}
				
				sb.append("\t\t</c:forEach>\n");
				sb.append("\t\t</table>\n");
				sb.append("\t</div>\n");
				
				fileName = sqlMapVo.getId();
			}
			
			if(StringUtils.isNotEmpty(fileName)){
				String fileUrl = sourceInfo.getHtmlPath() + "/" + sqlMapVo.getId() + ".jsp";
				UtilUnit.makeFile(fileUrl, sb.toString());
			}
			
		}
		
		System.out.println("-- Make Html OK");
			
	}
	
}
