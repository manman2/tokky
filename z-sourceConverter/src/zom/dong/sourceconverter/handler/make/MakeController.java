package zom.dong.sourceconverter.handler.make;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeController {

	/**
	 * Controller make
	 * @param sqlMapVoList
	 * @param iFile
	 * @param namespace
	 */
	public static MClass make(GetSourceInfo sourceInfo, ParseSqlXml sqlXml, List<SqlMapVo> newMethodList,  MClass serviceClass) {
		
		System.out.println("-- Make Controller Start");
		
		UtilUnit.makeDir(sourceInfo.getControllerPath());
		
		String fileUrl = sourceInfo.getControllerPath() + "/" + sourceInfo.getControllerFile() + ".java";
		
		//기존 메소드 가져오기
		
		String modelUpper = UtilUnit.firstUpper(sqlXml.getFirstModel());
		String modelLower = UtilUnit.firstLower(sqlXml.getFirstModel());
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		 String serviceNameList = store.getString("serviceNameList").trim();
		 String serviceNameGet = store.getString("serviceNameGet").trim();
		 String serviceNameCreate = store.getString("serviceNameCreate").trim();
		 String serviceNameUpdate = store.getString("serviceNameUpdate").trim();
		 String serviceNameDelete = store.getString("serviceNameDelete").trim();
		 String serviceNameCount = store.getString("serviceNameCount").trim();
		 
		 String listNaming = store.getString("sqlNameList").trim();
		 String sqlNameGet = store.getString("sqlNameGet").trim();
		 String sqlNameCreate = store.getString("sqlNameCreate").trim();
		 String sqlNameUpdate = store.getString("sqlNameUpdate").trim();
		 String sqlNameDelete = store.getString("sqlNameDelete").trim();
		 String sqlNameCount = store.getString("sqlNameCount").trim();
		 
		 //PK List 가져오기
		 List<String> pkList = new ArrayList<String>();
		 String prePk = "";
		
		for(MType mv : serviceClass.getMethodList()){
			if(mv.getMethodType().equals("get")){
				pkList = mv.getParameterList();
				break;
			}
		}
			
		 
		//클래스 생성
		MClass webClass = new MClass();
		
		if(!fileUrl.isEmpty()){
		
			webClass.setPackageName(sourceInfo.getWebPackage());
			
			 String importClass = store.getString("webImportClass").trim();
			 
			 String[] imports = importClass.split("[;]");
			 
			 for(String imp : imports){
				 webClass.addImportList(imp);
			 }
			
			webClass.addImportList(sourceInfo.getServicePackage() + "." + sourceInfo.getServiceFile());
			
			//import 생성
			for(String typeAlias : sqlXml.getTypeAliasClassMap().values()){
				webClass.addImportList(typeAlias);
			}
			
			
			webClass.setClassAnotation("@Controller\n@RequestMapping(value = \""+sourceInfo.getProjectName()+"/"+sourceInfo.getModuleName()+"\")\n@SessionAttributes(\""+modelLower+"Form\")");
			
			webClass.setClassType("class");
			webClass.setClassName(sourceInfo.getControllerFile());
			
			String classExtends = store.getString("webClassExtends").trim();
			classExtends = classExtends.replace("{model}", sqlXml.getFirstModel());
			
			webClass.setExtendsClass(classExtends);
			
			MType filed = new MType();
			filed.setAnnotation("@Autowired");
			filed.setDataProperty("private");
			filed.setDataType(sourceInfo.getServiceFile());
			filed.setName(UtilUnit.firstLower(sourceInfo.getServiceFile()));
			
			webClass.addFiledList(filed);
			
			
			boolean isFileChk = false;
			 for(MType dMethod :  serviceClass.getMethodList()){
				 if(dMethod.isFile()){
					 isFileChk = true;
					break;
				}
			 }
			
			if(isFileChk){
				MType filed3 = new MType();
				filed3.setAnnotation("@Autowired");
				filed3.setDataProperty("private");
				filed3.setDataType("FileService");
				filed3.setName("fileService");
				
				webClass.addFiledList(filed3);
			}
			
		}
		
		//함수 생성
		for(MType mt : serviceClass.getMethodList()){
			
			MType method = new MType();
			
			if(mt.isFile()){
				method.addThrowsList("JsonGenerationException");
				method.addThrowsList("JsonMappingException");
				method.addThrowsList("IOException");
			}
			
			//if(mt.getName().equals(serviceNameCreate)){ //create
			if(mt.getMethodType().equals("create")){
				
				//form
				method.setAnnotation("@RequestMapping(\"/form"+modelUpper+".do\")");
				method.setDataProperty("public");
				method.setDataType("ModelAndView");
				method.setName("form"+modelUpper);
				method.setParameterList(mt.getParameterList());
				
				StringBuffer formSb = new StringBuffer();
				
				formSb.append("\t\tModelAndView mav = new ModelAndView(\""+sourceInfo.getProjectName()+"/"+sourceInfo.getModuleName()+"/form"+modelLower+"\");\n\n");
				//formSb.append("\t\t"+modelUpper+" "+modelLower+"Form = new "+modelUpper+"();\n\n");
				
				formSb.append("\t\tUser user = (User) getRequestAttribute(\"ikep.user\");\n\n");
				
			//	formSb.append("\t\tString pk = "+modelLower+".get"+UtilUnit.firstUpper(prePk)+"();\n\n");
				formSb.append("\t\tif (StringUtils.isNotEmpty("+modelLower+".get"+UtilUnit.firstUpper(sourceInfo.getPkName())+"())) {\n\n");
				
				
				formSb.append("\t\t\t"+modelLower+"Form = "+UtilUnit.firstLower(sourceInfo.getServiceFile())+".get"+modelUpper+"(");
				
				String pkParam = "";
				for(String pk : pkList){
					pkParam += ", "+modelLower+".get"+UtilUnit.firstUpper(pk)+"()";
				}
				formSb.append(pkParam.substring(2));
				
				
				if(mt.isFile()){
					formSb.append("\t\t\t//첨부파일 가져오기\n");
					formSb.append("\t\t\tObjectMapper mapper = new ObjectMapper();\n");
					formSb.append("\t\t\tString fileDataListJson = mapper.writeValueAsString("+modelLower+".getFileDataList());\n");
					formSb.append("\t\t\tmav.addObject(\"fileDataListJson\", fileDataListJson);\n\n");

				}
				
				formSb.append("\t\t);\n\n");
				
				

				formSb.append("\t\t}\n\n");
				
				formSb.append("\t\tmav.addObject(\""+modelLower+"Form\", "+modelLower+"Form);\n\n");
				
				if(mt.isEditer()){
					formSb.append("\t\t// ActiveX editor 사용여부 확인\n");
					formSb.append("\t\tProperties commonprop = PropertyLoader.loadProperties(\"/configuration/common-conf.properties\");\n");
					formSb.append("\t\tString useActiveX = \"N\";\n");
					formSb.append("\t\tuseActiveX = commonprop.getProperty(\"ikep4.activex.editor.use\");\n");
					formSb.append("\t\tmav.addObject(\"useActiveX\", useActiveX);\n\n");
					
				}
				
				if(mt.isFile()){
					formSb.append("\t\t// 파일 업로드 설정 관련\n");
					formSb.append("\t\tProperties fileprop = PropertyLoader.loadProperties(\"/configuration/fileupload-conf.properties\");\n");
					formSb.append("\t\tString maxTotalSize = fileprop.getProperty(\"ikep4.support.fileupload.innorix.upload_size\");\n");
					formSb.append("\t\tString maxFileCount = fileprop.getProperty(\"ikep4.support.fileupload.innorix.upload_cnt\");\n");
					formSb.append("\t\tString extFilterExclude = fileprop.getProperty(\"ikep4.support.fileupload.innorix.ext_filter_exclude\");\n");
					formSb.append("\t\tString innorixLanguage = \"\";\n");
					
					formSb.append("\t\tif(\"ko\".equals(user.getLocaleCode())){\n");
					formSb.append("\t\t\tinnorixLanguage = \"ko\";\n");
					formSb.append("\t\t}else{\n");
					formSb.append("\t\t\tinnorixLanguage = \"en\";\n");
					formSb.append("\t\t}\n");
					
					formSb.append("\t\tString backgroundImageURL = fileService.getInnorixImageURL();\n");
					
					formSb.append("\t\tmav.addObject(\"searchConditionString\", vocVoice.getSearchConditionString());\n");
					formSb.append("\t\tmav.addObject(\"maxTotalSize\", maxTotalSize);\n");
					formSb.append("\t\tmav.addObject(\"maxFileCount\", maxFileCount);\n");
					formSb.append("\t\tmav.addObject(\"extFilterExclude\", extFilterExclude);\n");
					formSb.append("\t\tmav.addObject(\"innorixLanguage\", innorixLanguage);\n");
					formSb.append("\t\tmav.addObject(\"backgroundImageURL\", backgroundImageURL);\n");
				}
				
				
				formSb.append("\t\treturn this.bindResult(mav);\n");
				
				method.setContents(formSb.toString());
				
				webClass.addMethodList(method);
				
				
				//create, update 
				method = new MType();
				
				method.setAnnotation("@RequestMapping(value = \"/"+mt.getName()+".do\", method = RequestMethod.POST)");
				method.setDataProperty("public");
				method.setDataType("ModelAndView");
				method.setName(mt.getName());
				method.addParameterList("@ValidEx  "+modelUpper+" "+modelLower+"Form");
				method.addParameterList("BindingResult result");
				method.addParameterList("SessionStatus status");
				method.addParameterList("HttpServletRequest request");
				
				StringBuffer submitSb = new StringBuffer();
				submitSb.append("\t\tif (result.hasErrors()) {\n");
				submitSb.append("\t\t\tthis.setErrorAttribute(\""+modelLower+"Form\", "+modelLower+"Form, result);\n\n");
				submitSb.append("\t\t\treturn new ModelAndView(\"forward:"+modelLower+"Form.do\").addObject(\""+prePk+"\", "+modelLower+"Form.get"+UtilUnit.firstUpper(prePk)+"());\n\n");
				submitSb.append("\t\t}\n\n");
				
				
				if(mt.isEditer()){
					submitSb.append("\t\t//ActiveX editor 사용여부 확인\n");
					submitSb.append("\t\tProperties commonprop = PropertyLoader.loadProperties(\"/configuration/common-conf.properties\");\n");
					submitSb.append("\t\tString useActiveX = \"N\";\n");
					submitSb.append("\t\tuseActiveX = commonprop.getProperty(\"ikep4.activex.editor.use\");\n");
					submitSb.append("\t\tPortal portal = (Portal) getRequestAttribute(\"ikep.portal\");\n");
					submitSb.append("\t\t//ActiveX Editor 사용 여부 확인\n");
					submitSb.append("\t\tif(\"Y\".equals(useActiveX) && "+modelLower+"Form.getMsie() == 1 && !StringUtil.isEmpty("+modelLower+"Form.getContents())) {\n");
					submitSb.append("\t\t//사용자 브라우저가 IE인 경우\n");
					submitSb.append("\t\t\ttry {	\n");
					submitSb.append("\t\t\t\t//현재 포탈 포트 가져오기\n");
					submitSb.append("\t\t\t\tString port = commonprop.getProperty(\"ikep4.activex.editor.port\");\n");
					submitSb.append("\t\t\t\tMimeUtil util = new MimeUtil(fileService, portal.getPortalHost(), port);\n");
					submitSb.append("\t\tutil.setMimeValue("+modelLower+"Form.getContents());\n");
					submitSb.append("\t\t//Mime 데이타 decoding\n");
					submitSb.append("\t\t\t\tutil.processDecoding();\n");
					submitSb.append("\t\t\t\t//editor 첨부된 이미지 확인\n");
					submitSb.append("\t\t\t\tif(util.getFileLinkList() != null && util.getFileLinkList().size()>0){\n");
					submitSb.append("\t\t\t\t\tList<FileLink> newFileLinkList = new ArrayList<FileLink>();\n");
					submitSb.append("\t\t\t\t\t//기존 등록된 파일 처리\n");
					submitSb.append("\t\t\t\t\tif(voiceForm.getEditorFileLinkList() != null){\n");
					submitSb.append("\t\t\t\t\t\tfor (int i = 0; i < "+modelLower+"Form.getEditorFileLinkList().size(); i++) {\n");
					submitSb.append("\t\t\t\t\t\t\tFileLink fLink = (FileLink) "+modelLower+"Form.getEditorFileLinkList().get(i);\n");
					submitSb.append("\t\t\t\t\t\t\tnewFileLinkList.add(fLink);\n");
					submitSb.append("\t\t\t\t\t\t}\n");
					submitSb.append("\t\t\t\t\t}\n");
					submitSb.append("\t\t\t\t\t//새로 등록된 파일 처리\n");
					submitSb.append("\t\t\t\t\tfor (int i = 0; i < util.getFileLinkList().size(); i++) {\n");
					submitSb.append("\t\t\t\t\t\tFileLink fileLink = (FileLink)util.getFileLinkList().get(i);	\n");						
					submitSb.append("\t\t\t\t\t\tnewFileLinkList.add(fileLink);\n");
					submitSb.append("\t\t\t\t\t}\n");
					submitSb.append("\t\t\t\t\t"+modelLower+"Form.setEditorFileLinkList(newFileLinkList);\n");
					submitSb.append("\t\t\t\t}\n");
					submitSb.append("\t\t\t\t//내용 가져오기\n");
					submitSb.append("\t\t\t\tString content = util.getDecodedHtml(false);	\n");	
					submitSb.append("\t\t\t\tcontent = content.trim();\n");
					submitSb.append("\t\t\t\t//내용세팅\n");
					submitSb.append("\t\t\t\t"+modelLower+"Form.setContents(content);\n");
					submitSb.append("\t\t\t} catch (Exception e) {\n");
					submitSb.append("\t\t\t\t	throw new IKEP4ApplicationException(\"\", e);\n");
					submitSb.append("\t\t\t}\n");
					submitSb.append("\t\t}\n\n");
					
				}
				
				if(mt.isFile()){
					submitSb.append("\t\t//Innorix 파일 업로드 추가 부분 시작                                                                      \n");
					submitSb.append("\t\tString[] origfilename = request.getParameterValues(\"_innorix_origfilename\"); 		// 원본 파일명   \n");
					submitSb.append("\t\tString[] savefilename = request.getParameterValues(\"_innorix_savefilename\"); 		// 저장 파일명   \n");
					submitSb.append("\t\tString[] filesize = request.getParameterValues(\"_innorix_filesize\"); 			// 파일 사이즈       \n");
					submitSb.append("\t\tString[] customvalue = request.getParameterValues(\"_innorix_customvalue\"); 		// 개발자 정의값  \n");
					submitSb.append("\t\tList<FileData> uploadList = new ArrayList<FileData>();                                         \n\n");
					submitSb.append("\t\tif (origfilename != null) {                                                                    \n");
					submitSb.append("\t\t\t	for (int i = 0; i < origfilename.length; i++) {                                             \n");
					submitSb.append("\t\t\t\t		FileData fileData = new FileData();                                                     \n");
					submitSb.append("\t\t\t\t		fileData.setFilePath(customvalue[i]);                                                   \n");
					submitSb.append("\t\t\t\t		fileData.setFileName(savefilename[i]);                                                  \n");
					submitSb.append("\t\t\t\t		fileData.setFileRealName(origfilename[i]);                                              \n");
					submitSb.append("\t\t\t\t		fileData.setFileSize(Integer.parseInt(filesize[i]));                                    \n");
					submitSb.append("\t\t\t\t		uploadList.add(fileData);                                                               \n");
					submitSb.append("\t\t\t	}                                                                                           \n");
					submitSb.append("\t\t}                                                                                              \n");
					submitSb.append("\t\tString[] strExistID = request.getParameterValues(\"_innorix_exist_id\"); 			// 존재하는 파일ID\n");		
					submitSb.append("\t\tString[] strDeletedID = request.getParameterValues(\"_innorix_deleted_id\");		// 삭제된 파일ID     \n");
					submitSb.append("\t\t"+modelLower+"Form.setFileDataList(uploadList);                                                         \n");
					submitSb.append("\t\t"+modelLower+"Form.setExistFileId(strExistID);                                                          \n");
					submitSb.append("\t\t"+modelLower+"Form.setDeleteFileId(strDeletedID);                                                       \n");
					submitSb.append("\t\t// Innorix 파일 업로드 추가 부분 끝                                                                      \n");
				}
				
				
				SqlMapVo mapVo = new SqlMapVo();
				
				for(SqlMapVo mv : newMethodList){
					String methodName = mv.getId().replace(listNaming, serviceNameList).replace(sqlNameGet, serviceNameGet).replace(sqlNameCreate, serviceNameCreate)
							.replace(sqlNameUpdate, serviceNameUpdate).replace(sqlNameDelete, serviceNameDelete).replace(sqlNameCount, serviceNameCount);
					
					if(methodName.equals(mt.getName())){
						mapVo = mv;
						break;
					}
				}
				
				submitSb.append("\t\t"+modelLower+"Form.setPortalId(user.getPortalId());\n");
				submitSb.append("\t\t"+modelLower+"Form.setRegisterId(user.getUserId());\n");
				submitSb.append("\t\t"+modelLower+"Form.setRegisterName(user.getUserName());\n");
				submitSb.append("\t\t"+modelLower+"Form.setUpdaterId(user.getUserId());\n");
				submitSb.append("\t\t"+modelLower+"Form.setUpdaterName(user.getUserName());\n\n");
				
				submitSb.append("\n\t\tif (StringUtils.isEmpty("+modelLower+"Form.get"+UtilUnit.firstUpper(sourceInfo.getPkName())+"())) {\n\n");
				
				submitSb.append("\t\t\t"+modelLower+"Form.set"+UtilUnit.firstUpper(sourceInfo.getPkName())+"("+UtilUnit.firstLower(sourceInfo.getServiceFile())+".create"+modelUpper+"("+modelLower+"Form));\n\n");
				submitSb.append("\t\t} else {\n\n");
				submitSb.append("\t\t\t"+UtilUnit.firstLower(sourceInfo.getServiceFile())+".update"+modelUpper+"("+modelLower+"Form);\n\n");
				submitSb.append("\t\t}\n\n");
				
				submitSb.append("\t\tstatus.setComplete();\n\n");
				submitSb.append("\t\treturn new ModelAndView(\"redirect:"+serviceNameGet+".do?"+sourceInfo.getPkName()+"=\" + "+modelLower+"Form.get"+UtilUnit.firstUpper(sourceInfo.getPkName())+"() );\n\n");
				
				method.setContents(submitSb.toString());
				
			//기타 메소드
			} else {
				
				method.setAnnotation("@RequestMapping(value = \"/"+mt.getName()+".do\")");
				method.setDataProperty("public");
				method.setDataType("ModelAndView");
				method.setName(mt.getName());
				method.setParameterList(mt.getParameterList());
				
				StringBuffer listSb = new StringBuffer();
				listSb.append("\t\tModelAndView mav = new ModelAndView(\""+sourceInfo.getProjectName()+"/"+sourceInfo.getModuleName()+"/"+mt.getName()+"\");\n\n");
				
				
				SqlMapVo mapVo = new SqlMapVo();
				
				for(SqlMapVo mv : newMethodList){
					String methodName = mv.getId().replace(listNaming, serviceNameList).replace(sqlNameGet, serviceNameGet).replace(sqlNameCreate, serviceNameCreate)
							.replace(sqlNameUpdate, serviceNameUpdate).replace(sqlNameDelete, serviceNameDelete).replace(sqlNameCount, serviceNameCount);
					
					if(methodName.equals(mt.getName())){
						mapVo = mv;
						break;
					}
				}
				
				/*
				//list 일때 rowindex가 있는지 검사
				if(mt.getName().startsWith(serviceNameList)){
					
					for(String param : mapVo.getParamList()){
						if(param.toLowerCase().indexOf("rowindex") > -1){
							
							listSb.append("\t\t"+UtilUnit.firstLower(mapVo.getParamType())+".setStartRowIndex(0);\n\n");
							listSb.append("\t\t"+UtilUnit.firstLower(mapVo.getParamType())+".setEndRowIndex(10);\n\n");
							
							break;
						}
					}
				}
				*/
				
				String dataTypeName = mt.getName();
				
				if(mt.getDataType().toLowerCase().equals("int") || mt.getDataType().toLowerCase().equals("integer")){
					dataTypeName = "count";
				} else if(!mt.getDataType().toLowerCase().equals("string")) {
					dataTypeName = UtilUnit.firstLower(mapVo.getParamType());
				}
				
				if(!mt.getDataType().equals("void")) {
					listSb.append("\t\t"+mt.getDataType()+" "+dataTypeName+" = ");
				} else {
					listSb.append("\t\t");
				}
				
				listSb.append( UtilUnit.firstLower(sourceInfo.getServiceFile())+"."+mt.getName()+"(");
				
				String mParam = "";
				for(String param : mt.getParameterList()){
					
					String[] pr = param.split(" ");
					if(pr.length > 2){
						mParam += ", "+pr[1];
					}
					
				}
				if(mParam.length() > 3){
					listSb.append(mParam.substring(2));
				}
				
				listSb.append(");\n\n");
				
				if(mt.isFile()){
					listSb.append("\t\t\t//첨부파일 가져오기\n");
					listSb.append("\t\t\tObjectMapper mapper = new ObjectMapper();\n");
					listSb.append("\t\t\tString fileDataListJson = mapper.writeValueAsString("+modelLower+".getFileDataList());\n");
					listSb.append("\t\t\tmav.addObject(\"fileDataListJson\", fileDataListJson);\n\n");
				}
				
				
				if(mt.isPaging()){
					listSb.append("\t\tmav.addObject(\"searchResult\", searchResult);\n");
					listSb.append("\t\tmav.addObject(\""+modelLower+"SearchCondition\", searchResult.getSearchCondition());\n");
					
				} else if(mt.getMethodType().equals("list")){
					listSb.append("\t\tmav.addObject(\""+UtilUnit.firstLower(mapVo.getResultObject())+"List\", "+UtilUnit.firstLower(mapVo.getResultObject())+"List);\n\n");
					
				} else if(!mt.getDataType().equals("void")) {
					listSb.append("\t\tmav.addObject(\""+dataTypeName+"\", "+dataTypeName+");\n\n");
				}
				
				
				
				listSb.append("\t\treturn mav;\n\n");
				
				method.setContents(listSb.toString());
			}
			
			if(method.getName() != null){
				webClass.addMethodList(method);
			}
		}
		
		
		//controller 파일 생성
		UtilUnit.makeJavaFile(fileUrl, webClass);
		
		System.out.println("-- Make Web OK");
		
		return webClass;
			
	}
	
	
}
