package zom.dong.sourceconverter.handler.make;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.constants.Constants;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeDaoTest {

	/**
	 * test dao make
	 * @param daoClass
	 * @param iFile
	 * @param sqlMapVoList
	 * @throws JavaModelException 
	 * @throws IllegalArgumentException 
	 */
	public static void  make(GetSourceInfo sourceInfo, MClass daoClass, List<SqlMapVo> sqlMapVoList, ParseSqlXml sqlXml, String type){
		
		String fileUrl = "", testPath="", testFile = "", packageName="", importClassName=""
				, objectPackage = "", objectFile = "", testClassExtends=""
				, objectNameList="",objectNameGet = "" ,objectNameCreate ="",objectNameUpdate  = ""
				 ,objectNameDelete ="", objectNameCount = "";
				 
	   IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		 
		if(type.equals("dao")){
			System.out.println("-- Make DaoTest Start");
			testPath = sourceInfo.getDaoTestPath();
			testFile = sourceInfo.getDaoTestFile();
			fileUrl = testPath + "/" + testFile + ".java";
			packageName=sourceInfo.getDaoTestPackage();
			importClassName="daoTestImportClass";
			objectPackage = sourceInfo.getDaoPackage();
			objectFile = sourceInfo.getDaoFile();
			testClassExtends="daoTestClassExtends";
			
			objectNameList = store.getString("daoNameList").trim();
			objectNameGet = store.getString("daoNameGet").trim();
			objectNameCreate = store.getString("daoNameCreate").trim();
			objectNameUpdate = store.getString("daoNameUpdate").trim();
			objectNameDelete = store.getString("daoNameDelete").trim();
			objectNameCount = store.getString("daoNameCount").trim();
		} else {
			System.out.println("-- Make SericeTest Start");
			testPath = sourceInfo.getServiceTestPath();
			testFile = sourceInfo.getServiceTestFile();
			fileUrl = testPath + "/" + testFile + ".java";
			packageName=sourceInfo.getServiceTestPackage();
			importClassName="serviceTestImportClass";
			objectPackage = sourceInfo.getServicePackage();
			objectFile = sourceInfo.getServiceFile();
			testClassExtends="serviceTestClassExtends";
			
			objectNameList = store.getString("serviceNameList").trim();
			objectNameGet = store.getString("serviceNameGet").trim();
			objectNameCreate = store.getString("serviceNameCreate").trim();
			objectNameUpdate = store.getString("serviceNameUpdate").trim();
			objectNameDelete = store.getString("serviceNameDelete").trim();
			objectNameCount = store.getString("serviceNameCount").trim();
		}
		
		String listNaming = store.getString("sqlNameList").trim();
		 String sqlNameGet = store.getString("sqlNameGet").trim();
		 String sqlNameCreate = store.getString("sqlNameCreate").trim();
		 String sqlNameUpdate = store.getString("sqlNameUpdate").trim();
		 String sqlNameDelete = store.getString("sqlNameDelete").trim();
		 String sqlNameCount = store.getString("sqlNameCount").trim();
		
		UtilUnit.makeDir(testPath);
				
		MClass testClass = new MClass();
		
		String modelUpper = UtilUnit.firstUpper(sqlXml.getFirstModel());
		String modelLower = UtilUnit.firstLower(sqlXml.getFirstModel());
		
		if(!UtilUnit.isExistFile(fileUrl)){
			
			testClass.setPackageName(packageName);
			
			 String importClass = store.getString(importClassName).trim();
			 
			 String[] imports = importClass.split("[;]");
			 
			 for(String imp : imports){
				 testClass.addImportList(imp);
			 }
			
			testClass.addImportList(objectPackage+"." + objectFile);
			
			for(String typeAlias : sqlXml.getTypeAliasClassMap().values()){
				testClass.addImportList(typeAlias);
			}
			
			testClass.setClassType("class");
			testClass.setClassName(testFile);
			
			String classExtends = store.getString(testClassExtends).trim();
			classExtends = classExtends.replace("{model}", sqlXml.getFirstModel());
			
			testClass.setExtendsClass(classExtends);
			
			//@Autowired private QnaDao qnaDao;
			MType filed1 = new MType();
			filed1.setAnnotation("@Autowired");
			filed1.setDataProperty("private");
			filed1.setDataType(objectFile);
			filed1.setName( UtilUnit.firstLower(objectFile));
			
			MType filed2 = new MType();
			filed2.setDataProperty("private");
			filed2.setDataType(modelUpper);
			filed2.setName(modelLower);
			
			MType filed3 = new MType();
			filed3.setDataProperty("private");
			filed3.setDataType("String");
			filed3.setName("pk");
			filed3.setContents("20");
			
			List<MType> filedList = new ArrayList<MType>();
			filedList.add(filed1);
			filedList.add(filed2);
			filedList.add(filed3);
			
			testClass.setFiledList(filedList);
			
		}
		
		
		List<MType> methodList = new ArrayList<MType>();	//메소드 리스트
		
		// String insertNaming = store.getString("sqlNameCreate").trim();
		
		 
		
		 for(MType method : daoClass.getMethodList()){
			
			String methodName = method.getName();
			String nameCase = UtilUnit.firstUpper(methodName);
			String testMethodName = "test"+nameCase;
			
			MType testMethod = new MType();
			StringBuffer  methodContent = new StringBuffer();
			
			if(methodName.equals(sqlNameCreate)){		//test before
				testMethod.setAnnotation("@Before");
			} else {
				testMethod.setAnnotation("@Test");
			}
			
			testMethod.setDataProperty("public");
			testMethod.setDataType("void");
			
			testMethod.setName(testMethodName);
			
			String paramFull = "";
			
			if(method.getParameterList().size() == 1 && !method.getParameterList().get(0).contains("String") && !method.getParameterList().get(0).toLowerCase().contains("int")){ //파리미터가 모델이면
					
				String objectId = "";
				String returnObject = method.getParameterList().get(0).split(" ")[0];
					
				if(methodName.equals(sqlNameCreate)){
					objectId = modelLower;
					methodContent.append("\t\t"+objectId+" = new "+returnObject+"();\n\n");
				} else {
					objectId = "object";
					methodContent.append("\t\t"+returnObject+" "+objectId+" = new "+returnObject+"();\n\n");
				}
				
				
				SqlMapVo mapVo = new SqlMapVo();
				
				for(SqlMapVo mv : sqlMapVoList){
					String mName = mv.getId().replace(listNaming, objectNameList).replace(sqlNameGet, objectNameGet).replace(sqlNameCreate, objectNameCreate)
							.replace(sqlNameUpdate, objectNameUpdate).replace(sqlNameDelete, objectNameDelete).replace(sqlNameCount, objectNameCount);
					
					if(mName.equals(method.getName())){
						mapVo = mv;
						break;
					}
				}
				
				
				for(String param : mapVo.getParamList()){
					String paramCase = UtilUnit.firstUpper(param);
					
					String elementType = UtilUnit.getFiledType(sqlXml.getModelFields(), mapVo.getParamType(), param );
					
					if(elementType.equals("Integer")){
						methodContent.append("\t\t"+objectId+".set"+paramCase+"("+"1"+");\n");
					} else if(elementType.equals("Date")){
						methodContent.append("\t\t"+objectId+".set"+paramCase+"(new Date());\n");
					} else {
						methodContent.append("\t\t"+objectId+".set"+paramCase+"(\""+param+"\");\n");
					}
				}
				
				methodContent.append("\n");
				
				paramFull = objectId;
				
			} else {
				
				int i = 0;
				for(String paramName : method.getParameterList()){
					
					if(i != 0){
						paramFull += ", ";
					}
					
					String tmp = "";
					/*
					if(daoMethod.getParameterTypes().equals("String")){
						tmp = "\""+paramName+"\"";
					} else{
						tmp = paramName;
					}
					*/
					tmp = modelLower+".get"+UtilUnit.firstUpper(paramName.split(" ")[1])+"()";
					
					paramFull += tmp;
					i++;
						
				}
			}
			
			String contentTemp = "";
			
			if(methodName.startsWith(objectNameCount)){
				contentTemp = "int result = ";
			} else if(methodName.startsWith(objectNameGet)){
				contentTemp = modelUpper+" result = ";
			} else if(methodName.startsWith(listNaming)){
				contentTemp = "List<"+modelUpper+"> result = ";
			}
			
			if(!methodName.equals(sqlNameCreate)){
				methodContent.append("\t\ttry{\n");
			}
			
			methodContent.append("\t\t\t"+contentTemp+UtilUnit.firstLower(objectFile)+"."+methodName+"("+paramFull+");\n");
			
			if(!methodName.equals(sqlNameCreate)){
				
				methodContent.append("\n\t\t\tassertTrue(true);");
				methodContent.append("\n\n\t\t} catch(Exception e){");
				methodContent.append("\n\t\t\tassertTrue(false);");
				methodContent.append("\n\t}");
				
				/*
				if(methodName.startsWith(sqlNameUpdate)){
					methodContent.append("\t\t"+modelUpper+" asser = "+modelLower+daoFile+".get(pk);\n");
					methodContent.append("\n\t\tassertNotNull(asser);");
					
				} else if(methodName.startsWith(sqlNameDelete)){
					methodContent.append("\t\t"+modelUpper+" asser = "+modelLower+daoFile+".get(pk);\n");
					methodContent.append("\n\t\tassertNull(asser);");
						
				} else if(methodName.startsWith(listNaming)){
					methodContent.append("\n\t\tassertTrue(result.size() > 0);");
					
				} else {
					methodContent.append("\n\t\tassertNotNull(result);");
				}
				*/
			}
			
			testMethod.setContents(methodContent.toString());
			
			methodList.add(testMethod);
			
		}
		
		testClass.setMethodList(methodList);
		
		//dao impl 파일 생성
		UtilUnit.makeJavaFile(fileUrl, testClass);
		
		if(type.equals("dao")){
			System.out.println("-- Make DaoTest OK");
		} else {
			System.out.println("-- Make ServiceTest OK");
		}
	}
	
}
