package zom.dong.sourceconverter.handler.make;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.handler.make.sourceMake.MakeMethod;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeDaoImpl {

	public static MClass make(GetSourceInfo sourceInfo, ParseSqlXml sqlXml, List<SqlMapVo> newMethodList) {
		
		//System.out.println("-- Make DaoImpl Start");
		UtilUnit utilUnit = new UtilUnit();
		
		
		String fileUrl = sourceInfo.getDaoImplPath() + "/" + sourceInfo.getDaoImplFile() + ".java";
		
		MClass dmClass = new MClass();
		//기존 파일 없어 새로 등록
		if(!UtilUnit.isExistFile(fileUrl)){
		
			dmClass.setPackageName(sourceInfo.getDaoImplPackage());
			
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			
			 String importClass = store.getString("daoImplImportClass").trim();
			 
			 String[] imports = importClass.split("[;]");
			 
			 for(String imp : imports){
				 dmClass.addImportList(imp);
			 }
			
			dmClass.addImportList(sourceInfo.getDaoPackage() + "." + sourceInfo.getDaoFile());
			
			//import 생성
			for(String typeAlias : sqlXml.getTypeAliasClassMap().values()){
				dmClass.addImportList(typeAlias);
			}
			
			//utilUnit.addModelImport(sourceInfo,  sqlXml, dmClass );
			
			dmClass.setClassAnotation("@Repository");
			
			dmClass.setClassType("class");
			dmClass.setClassName(sourceInfo.getDaoImplFile());
			
			String classExtends = store.getString("daoImplClassExtends").trim();
			classExtends = classExtends.replace("{model}", sqlXml.getFirstModel());
			
			dmClass.setExtendsClass(classExtends);
			dmClass.setImplementsClass(sourceInfo.getDaoFile());
			
			MType filed = new MType();
			filed.setDataProperty("public");
			filed.setFinal(true);
			filed.setDataType("String");
			filed.setName("NAMESPACE");
			filed.setContents(sqlXml.getNamespace()+".");
			
			dmClass.addFiledList(filed);
		}
		
		//메소드 생성
		for(SqlMapVo sqlVo : newMethodList){
 			
			MType method = parseSelect(sqlVo, utilUnit, sqlXml);
			method.setPaging(sqlVo.isPaging());
			method.setFile(sqlVo.isFile());
			method.setEditer(sqlVo.isEditer());
			method.setIdAuto(sqlVo.isIdAuto());
			//method.setParamObject(sqlVo.isParamObject());
			
			dmClass.addMethodList(method);
		}
		
		//dao impl 파일 생성
		UtilUnit.makeJavaFile(fileUrl, dmClass);
		
		System.out.println("-- Make DaoImple OK");
		
		return dmClass;
			
	}


	/**
	 * select 파싱
	 * @param type
	 * @param method
	 * @param sqlVo
	 * @param utilUnit
	 * @param sqlXml
	 */
	private static MType parseSelect(SqlMapVo sqlVo, UtilUnit utilUnit, ParseSqlXml sqlXml){
		
		MType method = new MType();
		method.setDataProperty("public");
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		 String listNaming = store.getString("sqlNameList").trim();
		 String sqlNameGet = store.getString("sqlNameGet").trim();
		 String sqlNameCreate = store.getString("sqlNameCreate").trim();
		 String sqlNameUpdate = store.getString("sqlNameUpdate").trim();
		 String sqlNameDelete = store.getString("sqlNameDelete").trim();
		 String sqlNameCount = store.getString("sqlNameCount").trim();
		 
		 String daoNameList = store.getString("daoNameList").trim();
		 String daoNameGet = store.getString("daoNameGet").trim();
		 String daoNameCreate = store.getString("daoNameCreate").trim();
		 String daoNameUpdate = store.getString("daoNameUpdate").trim();
		 String daoNameDelete = store.getString("daoNameDelete").trim();
		 String daoNameCount = store.getString("daoNameCount").trim();
		 
		 String sqlList = store.getString("daoImplSqlSelectForList").trim();
		 String sqlListForObject = store.getString("daoImplSqlSelectForListOfObject").trim();
		 String sqlObject = store.getString("daoImplSqlSelectForObject").trim();
		 String sqlInsert = store.getString("daoImplSqlInsert").trim();
		 String sqlUpdate = store.getString("daoImplSqlUpdate").trim();
		 String sqlDelete = store.getString("daoImplSqlDelete").trim();
		 
		 //dateType 구하기
		 MakeMethod.methodDateType(method, sqlVo);
		
		String methodName = sqlVo.getId().replace(listNaming, daoNameList).replace(sqlNameGet, daoNameGet).replace(sqlNameCreate, daoNameCreate)
				.replace(sqlNameUpdate, daoNameUpdate).replace(sqlNameDelete, daoNameDelete).replace(sqlNameCount, daoNameCount);
		
		method.setName(methodName);
		
		//변수 만들기 (String id, Object object)
		MakeMethod.parseMethodParam(sqlVo, method, sqlXml.getModelFields());
		
		//내용 만들기
		StringBuffer sb = new StringBuffer();
		String resultParam = MakeMethod.parseDaoImplContents(sqlVo.getParamType(), sb, method);
		
		if(StringUtils.isNotEmpty(resultParam)){
			resultParam = ", "+resultParam;
		}
		
		//대표 객체이면
		String rSql="", returnText="";
		
		if(!method.getDataType().equals("void") && !sqlVo.isInsertKey()){
			returnText = "return";
		}
		
		if(sqlVo.getMethodType().equals("list")){
			if(sqlVo.getResultObject().equals(sqlXml.getFirstModel())){	
				rSql = sqlList;
			} else {
				rSql = "(List)"+sqlListForObject;
			}
		} else if(sqlVo.getMethodType().equals("get")){
			rSql = "("+method.getDataType().replace("[<String, Object>]", "")+")"+sqlObject;
			
		} else if(sqlVo.getMethodType().equals("count")){
			rSql = "(Integer)"+sqlObject;
			
		} else if(sqlVo.getMethodType().equals("exists")){
			returnText = "int chk =";
			rSql = "(Integer)"+sqlObject;
			
		}else if(sqlVo.getMethodType().equals("update")){
			rSql = sqlUpdate;
			
		} else if(sqlVo.getMethodType().equals("delete")){
			rSql = sqlDelete;
			
		} else if(sqlVo.getMethodType().equals("create")){
			rSql = sqlInsert;
		}
		
		
		/*
		if(sqlVo.getType().equals("select")) {
			
			if(sqlVo.getId().startsWith(listNaming)){
				if(sqlVo.getResultObject().equals(sqlXml.getFirstModel())){	
					rSql = sqlList;
				} else {
					rSql = "(List)"+sqlListForObject;
				}
			} else {
				String rType = (sqlVo.getResultObject().equals("int")) ?  "Integer" : sqlVo.getResultObject().replace("java.lang.","");
				rSql = "("+rType.replace("HashMap","Map")+")"+sqlObject;
			}
			returnText = "return";
		} else {	//insert, update, delete
			
			if(sqlVo.getType().equals("update")){
				rSql = sqlUpdate;
				
			} else if(sqlVo.getType().equals("delete")){
				rSql = sqlDelete;
				
			} else if(sqlVo.getType().equals("insert")){
				if(sqlVo.isInsertKey()){
					rSql = "return (String)"+sqlInsert;
				} else {
					rSql = sqlInsert;
				}
				
			} else if(sqlVo.getType().equals("procedure")){
				
				if(sqlVo.getId().startsWith(listNaming)){
					rSql = sqlList;
				} else if(sqlVo.getId().startsWith(sqlNameGet)) {
					String rType = (sqlVo.getResultObject().equals("int")) ?  "Integer" : sqlVo.getResultObject().replace("java.lang.","");
					rSql = "("+rType+")"+sqlObject;
				
				} else if(sqlVo.getId().startsWith(sqlNameUpdate)) {
					rSql = sqlUpdate;
				} else if(sqlVo.getId().startsWith(sqlNameDelete)) {
					rSql = sqlDelete;
				} else if(sqlVo.getId().startsWith(sqlNameCreate)) {
					rSql = "return (String)"+sqlInsert;
				} else {
					rSql = sqlUpdate;
				}
			}
		}
		*/
		
		sb.append("\t\t"+returnText+" "+rSql+"(NAMESPACE + \""+sqlVo.getId()+"\""+resultParam+");\n");
		
		if(sqlVo.getMethodType().equals("exists")){
			sb.append("\t\treturn chk > 0;\n");
		}
		
		if(sqlVo.isInsertKey()){
			sb.append("\t\treturn "+resultParam.replace(",", "")+".get"+UtilUnit.firstUpper(sqlXml.getPkName())+"();\n");
		}
		
		method.setContents(sb.toString());
		method.setMethodType(sqlVo.getMethodType());
		
		return method;
	}
	
	
	
}
