package zom.dong.sourceconverter.handler.make.sourceMake;

import java.util.Map;

import org.eclipse.jdt.core.IField;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.constants.Constants;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeMethod {

	public static void methodDateType(MType method, SqlMapVo sqlVo){
		
		//IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		//String listNaming = store.getString("sqlNameList").trim();
		//String sqlNameGet = store.getString("sqlNameGet").trim();
		String resultObject = UtilUnit.firstUpper(sqlVo.getResultObject());
		
		if(sqlVo.getMethodType().equals("create")){
			
			if(sqlVo.isInsertKey()){
				method.setDataType("String");
			} else {
				method.setDataType("void");
			}
		} else if(sqlVo.getMethodType().equals("list")){
			
			method.setDataType("List<"+resultObject+">");
			
		} else if(sqlVo.getMethodType().equals("get")){
			
			method.setDataType(resultObject.replace("HashMap", "Map<String, Object>"));
			
		} else if(sqlVo.getMethodType().equals("count")){
			
			method.setDataType("Integer");
			
		} else if(sqlVo.getMethodType().equals("exists")){
			
			method.setDataType("boolean");
			
			/*
		} else if(sqlVo.getType().equals("procedure")) {
			
			if(sqlVo.getId().startsWith(listNaming)){
				method.setDataType("List<"+UtilUnit.firstUpper(sqlVo.getResultObject())+">");
			} else if(sqlVo.getId().startsWith(sqlNameGet)) {
				method.setDataType(sqlVo.getResultObject().replace("java.lang.", "").replace("java.util.", ""));
			} else {
				method.setDataType("void");
			}
			*/
		} else {
			method.setDataType("void");
		}
	}
	
	
	/**
	 * 변수 만들기 - (String id, Object object)
	 * @param sqlMapVo
	 * @param mType
	 */
	public static void parseMethodParam(SqlMapVo sqlMapVo, MType mType, Map<String, IField[]> modelFildsMap){
		
		if(sqlMapVo.getParamList().size() == 1 ){	//파라미터가 1개 있을경우
			
			
			String paramName =sqlMapVo.getParamList().get(0);
			
			if(sqlMapVo.getParamType() == null){
				
				mType.addParameterList(UtilUnit.getGuessParamType(paramName)+" "+paramName);
				
			} else {
				
				if(sqlMapVo.getIterateList().size() > 0 && sqlMapVo.getIterateList().get(0).equals(paramName)){
					mType.addParameterList("List<"+UtilUnit.getGuessParamType(paramName)+"> " + paramName);
				} else {
					if(sqlMapVo.getParamType().equalsIgnoreCase("map")){
						mType.addParameterList(UtilUnit.getGuessParamType(paramName) + " " + paramName);
					} else {
						mType.addParameterList((UtilUnit.firstUpper(UtilUnit.lastWord(sqlMapVo.getParamType(), "."))+" ").replace("Int ", "int ")+paramName);	
						
					}
				}
			}
			
			//(String a, int b, String c, List d) 파라미터가 1개 이상 4개 이하, 4개 이상은 객체나 맵으로 변경
		} else if(!sqlMapVo.isParamObject() && !sqlMapVo.isPaging() && sqlMapVo.getParamList().size() <= Constants.PARAMETER_COUNT && sqlMapVo.getParamList().size() > 1) { 
			
			//파라미터 타입이 map 이면
			if(sqlMapVo.getParamType().toLowerCase().contains("map") || sqlMapVo.getParamType().indexOf("java.util") > -1) {
				
				for(String param : sqlMapVo.getParamList()){
					
					boolean isChk = false;
					
					//iterate 면
					if(sqlMapVo.getIterateList().size() > 0 ){
						
						for(String iterate : sqlMapVo.getIterateList()){
							if(iterate.equals(param)){
								mType.addParameterList("List<"+UtilUnit.getGuessParamType(param)+"> " + param);
								isChk = true;
								break;
							}
						}
					}
					
					if(!isChk){
						mType.addParameterList(UtilUnit.getGuessParamType(param)+" "+param);
					}
					
				}
				
			} else {
				
				//모델 필드를 모두 가져와 비교
				IField[] fields = modelFildsMap.get(sqlMapVo.getParamName());
				
				if(fields == null || fields.length == 0){
					System.err.println("not model :"+sqlMapVo.getParamType() + " id:"+sqlMapVo.getId());
				}
				
				//메소드 파라미터 만들기
				for(String param : sqlMapVo.getParamList()){
					
					String elementType = UtilUnit.getFiledType(modelFildsMap, sqlMapVo.getParamName(), param );
					mType.addParameterList(elementType+" "+param);
				}
					
			}
			
		} else if(sqlMapVo.getParamList().size() == 0){
			
			//mType.addParameterList("");
			/*
			if(sqlMapVo.getParamType() == null){
				mType.addParameterList("Map<String, Object> map");
			} else {
				
				if(sqlMapVo.getParamList().size() > 0){
					mType.addParameterList(sqlMapVo.getParamType()+" "+UtilUnit.firstLower(sqlMapVo.getParamType()));
				} 
				
			}
			*/
		} else {	//파라미터가 많거나 페이징 이거나 create면
			
			if(sqlMapVo.getParamType().toLowerCase().contains("map") || sqlMapVo.getParamType().indexOf("java.util") > -1){	//리턴타입이 객체이면
				
				mType.addParameterList("Map<String, Object> map");
			//} else if(sqlMapVo.getParamType().equalsIgnoreCase("list") || sqlMapVo.getParamType().equals("java.util.ArrayList") || sqlMapVo.getParamType().equals("java.util.List")){	//리턴타입이 객체이면
				
			//	mType.addParameterList("List<String> list");
				
			} else {
				
				mType.addParameterList(sqlMapVo.getParamType()+" "+UtilUnit.firstLower(sqlMapVo.getParamType()));
			}
		}
	}
	
	
	/**
	 * 파라미터 map이나 object로 생성
	 * 
	 * object = new Object();
	 * object.setId(id);
	 * object.setDate(date);
	 * 
	 * @param sqlMapVo
	 * @param sb
	 * @param objectName
	 * @return
	 */
	public static String parseDaoImplContents(String paramType, StringBuffer sb, MType method){
		
		//IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		 //String insertNaming = store.getString("sqlNameCreate").trim();

		String resultParam = "";
		
		
		/*
		int chk = 0;
		for(String param : sqlMapVo.getParamList()){	//페이징 처리 된 쿼리이면 객체로 파라미터 받는다.
			if(param.indexOf("endRow") > -1 || param.indexOf("startRow") > -1){
				chk++;
			}
		}
		*/
		
		if(method.getParameterList().size() > 1){
			
			if(paramType.toLowerCase().indexOf("map") > -1){
				
				sb.append("\t\tMap<String, Object> map = new HashMap<String, Object>();\n");
				
				for(String param : method.getParameterList()){
					String vp = param.split(" ")[1];
					sb.append("\t\tmap.put(\"" + vp + "\", " +vp + ");\n");
				}
				sb.append("\n");
						
				resultParam = "map";
				
			} else {
				
				String paramTypeCase = UtilUnit.firstLower(paramType);
				
				sb.append("\t\t"+paramType+" "+paramTypeCase+" = new "+paramType+"();\n");
				
				for(String param : method.getParameterList()){
					String vp = param.split(" ")[1];
					String paramCase = UtilUnit.firstUpper(vp);
					sb.append("\t\t"+paramTypeCase+".set"+paramCase+"("+vp+");\n");
				}
				sb.append("\n");
				
				resultParam = paramTypeCase;
			}
		} else if(method.getParameterList().size() == 1) {
			if(paramType.toLowerCase().indexOf("map") > -1){
				resultParam = "map";
			} else {
				resultParam = method.getParameterList().get(0).split(" ")[1];
			}
			
		}
		
		/*
		if(paramType != null){
			
			String paramTypeCase = UtilUnit.firstLower(paramType);
			
			//sql에 파라미터를 받고 그 파라미터 개수가 여러개일 경우
			if(!sqlMapVo.getId().equals(insertNaming) && chk <= 1 && sqlMapVo.getParamList() != null && sqlMapVo.getParamList().size() <= Constants.PARAMETER_COUNT && sqlMapVo.getParamList().size() > 0){
				
				if(sqlMapVo.getParamType().toLowerCase().indexOf("map") > -1){
					
					sb.append("\t\tMap<String, Object> map = new HashMap<String, Object>();\n");
					
					for(String param : sqlMapVo.getParamList()){
						sb.append("\t\tmap.put(\"" + param + "\", " + param + ");\n");
					}
					sb.append("\n");
							
					resultParam = "map";
					
				} else if( sqlMapVo.getParamList().size() == 1){
					
					resultParam = sqlMapVo.getParamList().get(0);
					
				} else if(sqlMapVo.getParamList().size() != chk) {
					
					sb.append("\t\t"+paramType+" "+paramTypeCase+" = new "+paramType+"();\n");
					
					for(String param : sqlMapVo.getParamList()){
						String paramCase = UtilUnit.firstUpper(param);
						sb.append("\t\t"+paramTypeCase+".set"+paramCase+"("+param+");\n");
					}
					sb.append("\n");
					
					resultParam = paramTypeCase;
					
				} 
				
			} else if(sqlMapVo.getParamList().size() == 0){
				resultParam = "";
			} else {
					
				if(sqlMapVo.getParamType().toLowerCase().indexOf("map") > -1){
					resultParam = "map";
				} else if(sqlMapVo.getParamType().toLowerCase().equalsIgnoreCase("list") ){
					resultParam = "list";
				} else {
					resultParam = paramTypeCase;
				}
			}
		}
		*/
		
		return resultParam;
	}
	
}
