package zom.dong.sourceconverter.handler.make;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeServiceImpl {

	/**
	 * serviceImpl make
	 * @param sqlMapVoList
	 * @param iFile
	 * @param namespace
	 */
	public static MClass make(GetSourceInfo sourceInfo, ParseSqlXml sqlXml, MClass daoClass) {
		
		System.out.println("-- Make ServiceImpl Start");
		
		UtilUnit.makeDir(sourceInfo.getServiceImplPath());
		
		//존재여부
		String fileUrl = sourceInfo.getServiceImplPath() + "/" + sourceInfo.getServiceImplFile() + ".java";
		
		//클래스 생성
		MClass serviceImplClass = new MClass();
		
		String daoLower = UtilUnit.firstLower(sourceInfo.getDaoFile());
		
		if(!UtilUnit.isExistFile(fileUrl)){
		
			serviceImplClass.setPackageName(sourceInfo.getServiceImplPackage());
			
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			
			 String importClass = store.getString("serviceImplImportClass").trim();
			 
			 String[] imports = importClass.split("[;]");
			 
			 for(String imp : imports){
				 serviceImplClass.addImportList(imp);
			 }
			
			serviceImplClass.addImportList(sourceInfo.getServicePackage() + "." + sourceInfo.getServiceFile());
			serviceImplClass.addImportList(sourceInfo.getDaoPackage() + "." + sourceInfo.getDaoFile());
			
			//import 생성
			for(String typeAlias : sqlXml.getTypeAliasClassMap().values()){
				serviceImplClass.addImportList(typeAlias);
			}
			
			//import 생성
			//utilUnit.addModelImport(sourceInfo,  sqlXml, serviceImplClass );
			
			serviceImplClass.setClassAnotation("@Service");
			
			serviceImplClass.setClassType("class");
			serviceImplClass.setClassName(sourceInfo.getServiceImplFile());
			
			String classExtends = store.getString("serviceImplClassExtends").trim();
			classExtends = classExtends.replace("{model}", sqlXml.getFirstModel());
			
			serviceImplClass.setExtendsClass(classExtends);
			serviceImplClass.setImplementsClass(sourceInfo.getServiceFile());
			
			MType filed = new MType();
			filed.setAnnotation("@Autowired");
			filed.setDataProperty("private");
			filed.setDataType(sourceInfo.getDaoFile());
			filed.setName(daoLower);
			
			serviceImplClass.addFiledList(filed);
			
			boolean isAutoId = false;
			boolean isFileChk = false;
			
			 for(MType dMethod :  daoClass.getMethodList()){
				 if(!isAutoId && dMethod.isIdAuto()){
					isAutoId = true;
				}
				 if(!isFileChk && dMethod.isFile()){
					 isFileChk = true;
				}
			 }
			
			if(isAutoId){
			
				MType filed2 = new MType();
				filed2.setAnnotation("@Autowired");
				filed2.setDataProperty("private");
				filed2.setDataType("IdgenService");
				filed2.setName("idgenService");
				
				serviceImplClass.addFiledList(filed2);
			}
			
			if(isFileChk){
				MType filed3 = new MType();
				filed3.setAnnotation("@Autowired");
				filed3.setDataProperty("private");
				filed3.setDataType("FileService");
				filed3.setName("fileService");
				
				serviceImplClass.addFiledList(filed3);
			}
			
		}
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		 
		String listNaming = store.getString("sqlNameList").trim();
		 String sqlNameGet = store.getString("sqlNameGet").trim();
		 String sqlNameCreate = store.getString("sqlNameCreate").trim();
		 String sqlNameUpdate = store.getString("sqlNameUpdate").trim();
		 String sqlNameDelete = store.getString("sqlNameDelete").trim();
		 String sqlNameCount = store.getString("sqlNameCount").trim();
		 
		 String serviceNameList = store.getString("serviceNameList").trim();
		 String serviceNameGet = store.getString("serviceNameGet").trim();
		 String serviceNameCreate = store.getString("serviceNameCreate").trim();
		 String serviceNameUpdate = store.getString("serviceNameUpdate").trim();
		 String serviceNameDelete = store.getString("serviceNameDelete").trim();
		 String serviceNameCount = store.getString("serviceNameCount").trim();
		 
		 
		 for(MType dMethod :  daoClass.getMethodList()){
			 
				MType sMethod = new MType();
				sMethod.setDataProperty("public");
				
				//리턴타입 
				if(dMethod.isPaging()){
					sMethod.setDataType("SearchResult<"+sqlXml.getFirstModel()+">");
				} else {
					sMethod.setDataType(dMethod.getDataType());
				}
				
				
				String methodName = dMethod.getName().replace(listNaming, serviceNameList).replace(sqlNameGet, serviceNameGet).replace(sqlNameCreate, serviceNameCreate)
						.replace(sqlNameUpdate, serviceNameUpdate).replace(sqlNameDelete, serviceNameDelete).replace(sqlNameCount, serviceNameCount);
				
				sMethod.setName(methodName);
				
				//파라미터
				if(dMethod.isPaging()){
					sMethod.addParameterList(UtilUnit.firstUpper(sqlXml.getSearchModelName())+" "+sqlXml.getSearchModelName());
				} else {
					sMethod.setParameterList(dMethod.getParameterList());
				}
				
				
				StringBuffer sb = new StringBuffer();
				
				
				//페이징 이면
				if(dMethod.isPaging()){
					
					sb.append("\t\tint listCount = "+daoLower+"."+ dMethod.getName().replace("list", "count")+"("+sqlXml.getSearchModelName()+");\n");
					sb.append("\t\t"+sqlXml.getSearchModelName()+".terminateSearchCondition(listCount);\n\n");

					sb.append("\t\tSearchResult<"+sqlXml.getFirstModel()+"> searchResult = null;\n");
					sb.append("\t\tif("+sqlXml.getSearchModelName()+".isEmptyRecord()){\n");
					sb.append("\t\t\tsearchResult = new SearchResult<"+sqlXml.getFirstModel()+">("+sqlXml.getSearchModelName()+");\n");
					sb.append("\t\t}else{\n");
					sb.append("\t\t\tList<"+sqlXml.getFirstModel()+"> sList = "+daoLower+"."+dMethod.getName()+"("+sqlXml.getSearchModelName()+");\n"); 
					sb.append("\t\t\tsearchResult = new SearchResult<"+sqlXml.getFirstModel()+">(sList, "+sqlXml.getSearchModelName()+");\n");			
					sb.append("\t\t}\n\n");
					
					sb.append("\t\treturn searchResult;\n");
					
				} else {
					
					String modelName = "";
					
					if(sMethod.getParameterList().size() > 0){
						modelName = sMethod.getParameterList().get(0).split(" ")[1];
					}
					
					if(dMethod.isIdAuto() && dMethod.getMethodType().equals("create")){
						
						sb.append("\t\tString "+sourceInfo.getPkName()+" = idgenService.getNextId();\n");
						sb.append("\t\t"+modelName+".set"+UtilUnit.firstUpper(sourceInfo.getPkName())+"("+sourceInfo.getPkName()+");\n");
					}
					
					String resultParam = "";
					if(sMethod.getParameterList().size() > 0){
						
						for(int i=0; i < sMethod.getParameterList().size(); i++ ){
							String param = sMethod.getParameterList().get(i);
							String par = (param.indexOf("Map") > -1) ? "map":param.split(" ")[1];
							resultParam += ", "+par;
						}
						
						resultParam = resultParam.substring(2);
						
					}
					
					
					if(dMethod.isEditer() && (dMethod.getMethodType().equals("create") || dMethod.getMethodType().equals("update"))){
						
						
						sb.append("\n\t\t//ActiveX editor 사용여부 확인\n");
						sb.append("\t\tProperties commonprop = PropertyLoader.loadProperties(\"/configuration/common-conf.properties\");\n");
						sb.append("\t\tString useActiveX = \"N\";\n");
						sb.append("\t\tuseActiveX = commonprop.getProperty(\"ikep4.activex.editor.use\");\n");
						sb.append("\n\t\t//ActiveX Editor 사용 여부 확인\n");
						sb.append("\t\tif(\"Y\".equals(useActiveX)) {\n");
						sb.append("\t\t\t//사용자 브라우저가 IE인 경우\n");
						sb.append("\t\t\tif("+modelName+".getMsie() == 1){\n");
						sb.append("\t\t\t\ttry {	\n");
						sb.append("\t\t\t\t\t//현재 포탈 도메인 가져오기\n");
						sb.append("\t\t\t\t\tPortal portal = (Portal)RequestContextHolder.currentRequestAttributes().getAttribute(\"ikep.portal\", RequestAttributes.SCOPE_SESSION);\n");
						sb.append("\t\t\t\t\t//현재 포탈 포트 가져오기\n");
						sb.append("\t\t\t\t\tString port = commonprop.getProperty(\"ikep4.activex.editor.port\");\n");
						sb.append("\t\t\t\t\t//Tagfree ActiveX Editor Util => FileService, domain, port 생성자로 넘김 \n");
						sb.append("\t\t\t\t\tMimeUtil util = new MimeUtil(fileService, portal.getPortalHost(), port);\n");
						sb.append("\t\t\t\t\tutil.setMimeValue("+modelName+".getContents());\n");
						sb.append("\t\t\t\t\t//Mime 데이타 decoding\n");
						sb.append("\t\t\t\t\tutil.processDecoding();\n");
						sb.append("\t\t\t\t\t//editor 첨부된 이미지 확인\n");
						sb.append("\t\t\t\t\tif(util.getFileLinkList() != null && util.getFileLinkList().size()>0){\n");
						
						if(dMethod.getMethodType().equals("update")){
							
							sb.append("\t\t\t\t\t\tList<FileLink> newFileLinkList = new ArrayList<FileLink>();\n");
							sb.append("\t\t\t\t\t\t//기존 등록된 파일 처리\n");
							sb.append("\t\t\t\t\t\tif("+modelName+".getEditorFileLinkList() != null){\n");
							sb.append("\t\t\t\t\t\t\tfor (int i = 0; i < "+modelName+".getEditorFileLinkList().size(); i++) {\n");
							sb.append("\t\t\t\t\t\t\t\tFileLink fLink = (FileLink) "+modelName+".getEditorFileLinkList().get(i);\n");
							sb.append("\t\t\t\t\t\t\t\tnewFileLinkList.add(fLink);\n");
							sb.append("\t\t\t\t\t\t\t}\n");
							sb.append("\t\t\t\t\t\t}\n");
							sb.append("\t\t\t\t\t\t//새로 등록된 파일 처리\n");
							sb.append("\t\t\t\t\t\tfor (int i = 0; i < util.getFileLinkList().size(); i++) {\n");
							sb.append("\t\t\t\t\t\t\tFileLink fileLink = (FileLink)util.getFileLinkList().get(i);		\n");					
							sb.append("\t\t\t\t\t\t\tnewFileLinkList.add(fileLink);\n");
							sb.append("\t\t\t\t\t\t}\n");
							
							sb.append("\t\t\t\t\t\t"+modelName+".setEditorFileLinkList(newFileLinkList);\n");
							
						} else {
							sb.append("\t\t\t\t\t\t"+modelName+".setEditorFileLinkList(util.getFileLinkList());\n");
						}
						
						
						sb.append("\t\t\t\t\t}\n");
						sb.append("\t\t\t\t\t//내용 가져오기\n");
						sb.append("\t\t\t\t\tString content = util.getDecodedHtml(false);		\n");
						sb.append("\t\t\t\t\tcontent = content.trim();\n");
						sb.append("\t\t\t\t\t//내용세팅\n");
						sb.append("\t\t\t\t\t"+modelName+".setContents(content);\n");
						sb.append("\t\t\t\t} catch (Exception e) {\n");
						sb.append("\t\t\t\t\tthrow new IKEP4ApplicationException(\"\", e);\n");
						sb.append("\t\t\t\t}\n");
						sb.append("\t\t\t}\n");
						sb.append("\t\t}\n");
					}
					
					
					if(dMethod.isFile() && (dMethod.getMethodType().equals("create") || dMethod.getMethodType().equals("update"))){ 
						
							sb.append("\n\t\t// 첨부파일 등록\n");
							sb.append("\t\tif ("+modelName+".getFileDataList() != null && "+modelName+".getFileDataList().size() > 0) {\n");
							sb.append("\t\t\t"+modelName+".setAttachFileCount("+modelName+".getFileDataList().size());\n");
							sb.append("\t\t}\n");
					}
					
					
					String returnName = "";
					if(sMethod.getDataType().equals("void")){
						sb.append("\n\t\t"+daoLower+"."+sMethod.getName()+"("+resultParam+");\n");
						
					} else {
						
						if(sMethod.getDataType().equals("String")){
							returnName = "id";
						} else if(sMethod.getDataType().equals("Integer") || sMethod.getDataType().equals("int")){
							returnName = "val";
						} else if(sMethod.getDataType().equals("boolean")){
							returnName = "chk";
						} else if(sMethod.getDataType().indexOf("List") > -1){
							returnName = "list";
						} else {
							returnName = UtilUnit.firstLower(sMethod.getDataType());
						}
							
						sb.append("\n\t\t"+sMethod.getDataType()+" " + returnName +" = "+daoLower+"."+sMethod.getName()+"("+resultParam+");\n");
					}
					
					if(dMethod.isIdAuto() && dMethod.getMethodType().equals("create")){
						sMethod.setDataType("String");
						returnName = sourceInfo.getPkName();
					} 
					
					
					if(dMethod.isFile()){ 
						
						if(dMethod.getMethodType().equals("create")){
							sb.append("\n\t\t// 첨부파일 등록\n");
							sb.append("\t\tif ("+modelName+".getFileDataList() != null && "+modelName+".getFileDataList().size() > 0) {\n");
							sb.append("\t\t\tfileService.createFileInnorix("+modelName+".getFileDataList(), "+modelName+".get"+UtilUnit.firstUpper(sourceInfo.getPkName())+"(), "+UtilUnit.firstUpper(sourceInfo.getModuleName())+"Constants.ITEM_TYPE_CODE_"+sourceInfo.getModuleName().substring(0,2).toUpperCase()+", user);\n");
							sb.append("\t\t}\n");
						} else if(dMethod.getMethodType().equals("update")){
							
							sb.append("\n\t\t// 첨부파일 삭제\n");
							sb.append("\t\tif ("+modelName+".getDeleteFileId() != null && "+modelName+".getDeleteFileId().length > 0) {\n");
							sb.append("\t\t\tthis.fileService.deleteFileInnorix("+modelName+".getDeleteFileId());\n");
							sb.append("\t\t}\n\n");
							
							sb.append("\t\t// 첨부파일 등록\n");
							sb.append("\t\tif ("+modelName+".getFileDataList() != null && "+modelName+".getFileDataList().size() > 0) {\n");
							sb.append("\t\t\tfileService.createFileInnorix("+modelName+".getFileDataList(), "+modelName+".get"+UtilUnit.firstUpper(sourceInfo.getPkName())+"(), "+UtilUnit.firstUpper(sourceInfo.getModuleName())+"Constants.ITEM_TYPE_CODE_"+sourceInfo.getModuleName().substring(0,2).toUpperCase()+", user);\n");
							sb.append("\t\t}\n");
						} else if(dMethod.getMethodType().equals("get")){
							
							sb.append("\n\t\tif("+returnName+" != null){\n");
							sb.append("\t\t\t"+returnName+".setFileDataList(fileService.getItemFileAll("+sourceInfo.getPkName()+", \"N\"));\n");
							sb.append("\t\t}\n");
							
						}
						
					}
					
					if(dMethod.isEditer() && (dMethod.getMethodType().equals("create") || dMethod.getMethodType().equals("update"))){
						sb.append("\n\t\t//CKEDITOR내에 첨부된 이미지 파일의 링크 정보를 생성\n");
						sb.append("\t\tif("+modelName+".getEditorFileLinkList() != null){\n");
						sb.append("\t\t\tfileService.saveFileLinkForEditor("+modelName+".getEditorFileLinkList(), "+modelName+".get"+UtilUnit.firstUpper(sourceInfo.getPkName())+"(), "+UtilUnit.firstUpper(sourceInfo.getModuleName())+"Constants.ITEM_TYPE_CODE_"+sourceInfo.getModuleName().substring(0,2).toUpperCase()+", user);\n");
						sb.append("\t\t}\n");
					}
					
					if((dMethod.isFile() || dMethod.isEditer()) && dMethod.getMethodType().equals("delete")){
						sb.append("\t\tfileService.removeItemFile("+modelName+");\n");
					}
					
					
					
					if(!StringUtils.isEmpty(returnName)){
						sb.append("\n\t\treturn "+returnName+";\n");
					}
					
				
					if(dMethod.isFile()){
						sMethod.addParameterList("User user");
					}
					
				}
				
				sMethod.setContents(sb.toString());
				
				sMethod.setMethodType(dMethod.getMethodType());
				sMethod.setPaging(dMethod.isPaging());
				sMethod.setFile(dMethod.isFile());
					
				serviceImplClass.addMethodList(sMethod);
		 }
		 
		/*
		for(SqlMapVo sqlMapVo : newMethodList){
			
				
			MType method = new MType();
			method.setDataProperty("public");
			
			//dateType 구하기
			MakeMethod.methodDateType(method, sqlMapVo);
			
			String methodName = sqlMapVo.getId().replace(listNaming, serviceNameList).replace(sqlNameGet, serviceNameGet).replace(sqlNameCreate, serviceNameCreate)
					.replace(sqlNameUpdate, serviceNameUpdate).replace(sqlNameDelete, serviceNameDelete).replace(sqlNameCount, serviceNameCount);
			
			method.setName(methodName);
			
			//변수 만들기
			MakeMethod.parseMethodParam(sqlMapVo, method, sqlXml.getModelFields());
			
			StringBuffer sb = new StringBuffer();
			
			String resultParam = "";
			
			for(int i=0; i < method.getParameterList().size(); i++ ){
				String param = method.getParameterList().get(i);
				if(!StringUtils.isEmpty(param)){
					String[] params = param.split(" ");
					resultParam += params[1]+", ";
				}
			}
			
			if(StringUtils.isNotEmpty(resultParam)){
				resultParam = resultParam.substring(0,resultParam.length()-2);
				
				if(sqlMapVo.getType().equals("select")){
					sb.append("\t\treturn "+daoLower+"."+sqlMapVo.getId()+"("+resultParam+");\n");
				} else {
					sb.append("\t\t"+daoLower+"."+sqlMapVo.getId()+"("+resultParam+");\n");
				}
			} else {
				if(sqlMapVo.getType().equals("select")){
					sb.append("\t\treturn "+daoLower+"."+sqlMapVo.getId()+"();\n");
				} else {
					sb.append("\t\t"+daoLower+"."+sqlMapVo.getId()+"();\n");
				}
			}
			
			method.setContents(sb.toString());
			method.setType(sqlMapVo.getType());
				
			serviceImplClass.addMethodList(method);
		}
		*/
		
		//dao impl 파일 생성
		UtilUnit.makeJavaFile(fileUrl, serviceImplClass);
		
		System.out.println("-- Make ServiceImpl OK");
		
		return serviceImplClass;
			
	}

}
