package zom.dong.sourceconverter.handler.make;

import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeService {

	/**
	 * 
	 * @param service lClass
	 * @param iFile
	 */
	public static MClass make(GetSourceInfo sourceInfo, ParseSqlXml sqlXml, MClass serviceImplClass){
		
		System.out.println("-- Make Service Start");
		
		UtilUnit.makeDir(sourceInfo.getServicePath());
		
		String fileUrl = sourceInfo.getServicePath()+"/"+sourceInfo.getServiceFile()+".java";
		
		//클래스 생성
		MClass serviceClass = new MClass();
		
		if(!UtilUnit.isExistFile(fileUrl)){
		
			serviceClass.setPackageName(sourceInfo.getServicePackage());
			
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			
			 String importClass = store.getString("serviceImportClass").trim();
			 
			 String[] imports = importClass.split("[;]");
			 
			 for(String imp : imports){
				 serviceClass.addImportList(imp);
			 }
			
			//import 생성
			for(String typeAlias : sqlXml.getTypeAliasClassMap().values()){
				serviceClass.addImportList(typeAlias);
			}
			
			serviceClass.setClassAnotation("@Transactional");
			serviceClass.setClassType("interface");
			serviceClass.setClassName(sourceInfo.getServiceFile());
			
			String classExtends = store.getString("serviceClassExtends").trim();
			classExtends = classExtends.replace("{model}", UtilUnit.firstUpper(sqlXml.getFirstModel()));
			
			serviceClass.setExtendsClass(classExtends);
		}
		
		for(MType method : serviceImplClass.getMethodList()){
			
			MType iMethod = new MType();
			iMethod.setDataType(method.getDataType());
			iMethod.setName(method.getName());
			iMethod.setParameterList(method.getParameterList());
			iMethod.setType(method.getType());
			
			serviceClass.addMethodList(iMethod);
			
		}
		
		//dao impl 파일 생성
		UtilUnit.makeJavaFile(fileUrl, serviceClass);
		
		System.out.println("-- Make Service OK");
		
		return serviceImplClass;
	}
}
