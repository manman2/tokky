package zom.dong.sourceconverter.handler.make;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;

import zom.dong.sourceconverter.Activator;
import zom.dong.sourceconverter.constants.Constants;
import zom.dong.sourceconverter.handler.GetSourceInfo;
import zom.dong.sourceconverter.handler.ParseSqlXml;
import zom.dong.sourceconverter.model.MClass;
import zom.dong.sourceconverter.model.MType;
import zom.dong.sourceconverter.model.Param;
import zom.dong.sourceconverter.model.SqlMapVo;
import zom.dong.sourceconverter.util.UtilUnit;

public class MakeServiceTest {

	/**
	 * test dao make
	 * @param daoClass
	 * @param iFile
	 * @param sqlMapVoList
	 */
	public void  make(GetSourceInfo sourceInfo, MClass serviceClass, List<SqlMapVo> sqlMapVoList, ParseSqlXml sqlXml){
		
		System.out.println("-- Make ServiceTest Start");
		
		UtilUnit utilUnit = new UtilUnit();
		
		UtilUnit.makeDir(sourceInfo.getServiceTestPath());
		
		//존재여부
		String fileUrl = sourceInfo.getServiceTestPath()+"/"+sourceInfo.getServiceTestFile()+".java";
		
		//대표 모델
		String modelLower = UtilUnit.firstLower(sqlXml.getFirstModel());
		String modelUpper = UtilUnit.firstUpper(sqlXml.getFirstModel());
		
		String serviceName = UtilUnit.firstLower(sourceInfo.getServiceFile());
		
		MClass testServiceClass = new MClass();
		
		if(!UtilUnit.isExistFile(fileUrl)){
			
			testServiceClass.setPackageName(sourceInfo.getServiceTestPackage());
			
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			
			 String importClass = store.getString("serviceTestImportClass").trim();
			 
			 String[] imports = importClass.split("[;]");
			 
			 for(String imp : imports){
				 testServiceClass.addImportList(imp);
			 }
			 
			testServiceClass.addImportList(sourceInfo.getServicePath()+"." + sourceInfo.getServiceFile() );
			
			for(String typeAlias : sqlXml.getTypeAliasClassMap().values()){
				testServiceClass.addImportList(typeAlias);
			}
			
			testServiceClass.setClassType("class");
			testServiceClass.setClassName(sourceInfo.getServiceTestFile());
			
			String classExtends = store.getString("serviceTestClassExtends").trim();
			classExtends = classExtends.replace("{model}", sqlXml.getFirstModel());
			
			testServiceClass.setExtendsClass(classExtends);
			
			//@Autowired private QnaDao qnaDao;
			MType filed1 = new MType();
			filed1.setAnnotation("@Autowired");
			filed1.setDataProperty("private");
			filed1.setDataType(sourceInfo.getServiceFile());
			filed1.setName(serviceName);
			
			MType filed2 = new MType();
			filed2.setDataProperty("private");
			filed2.setDataType(modelUpper);
			filed2.setName(modelLower);
			
			MType filed3 = new MType();
			filed3.setDataProperty("private");
			filed3.setDataType("String");
			filed3.setName("pk");
			filed3.setContents("20");
			
			List<MType> filedList = new ArrayList<MType>();
			filedList.add(filed1);
			filedList.add(filed2);
			filedList.add(filed3);
			
			testServiceClass.setFiledList(filedList);
			
		}
		
		List<MType> methodList = new ArrayList<MType>();	//메소드 리스트
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		
		// String insertNaming = store.getString("sqlNameCreate").trim();
		 String countNaming = store.getString("sqlNameCount").trim();
		 String getNaming = store.getString("sqlNameGet").trim();
		 String listNaming = store.getString("sqlNameList").trim();
		 String daoFile = store.getString("daoFile").trim();
		 String sqlNameCreate = store.getString("sqlNameCreate").trim();
		 String sqlNameUpdate = store.getString("sqlNameUpdate").trim();
		 String sqlNameDelete = store.getString("sqlNameDelete").trim();
		 String insertNaming = store.getString("serviceNameCreate").trim();
		
		
		for(SqlMapVo sqlMapVo : sqlMapVoList){
			
			String methodName = sqlMapVo.getId();
			String nameCase = UtilUnit.firstUpper(methodName);
			String testNamaeCase = "test"+nameCase;
			
			MType method = new MType();
			StringBuffer  methodContent = new StringBuffer();
			
			if(methodName.equals(insertNaming)){		//test before
				method.setAnnotation("@Before");
			} else {
				method.setAnnotation("@Test");
			}
			
			method.setDataProperty("public");
			method.setDataType("void");
			
			method.setName(testNamaeCase);
			
			String paramFull = "";
			
			if(sqlMapVo.getParamList().size() >= Constants.PARAMETER_COUNT){ //파리미터가 모델이면
					
				String objectId = "";
				String returnObject = sqlMapVo.getParamType();
					
				if(methodName.equals(insertNaming)){
					objectId = modelLower;
					methodContent.append("\t\t"+objectId+" = new "+returnObject+"();\n\n");
				} else {
					objectId = "object";
					methodContent.append("\t\t"+returnObject+" "+objectId+" = new "+returnObject+"();\n\n");
				}
				
				for(String param : sqlMapVo.getParamList()){
					String paramCase = UtilUnit.firstUpper(param);
					
					String elementType = UtilUnit.getFiledType(sqlXml.getModelFields(), sqlMapVo.getParamType(), param );
					
					if(elementType.equals("Integer")){
						methodContent.append("\t\t"+objectId+".set"+paramCase+"("+"1"+");\n");
					} else if(elementType.equals("Date")){
						methodContent.append("\t\t"+objectId+".set"+paramCase+"(\"new Date()\");\n");
					} else {
						methodContent.append("\t\t"+objectId+".set"+paramCase+"(\""+param+"\");\n");
					}
				}
				
				methodContent.append("\n");
				
				paramFull = objectId;
				
			} else {
				
				int i = 0;
				for(String paramName : sqlMapVo.getParamList()){
					
					if(i != 0){
						paramFull += ", ";
					}
					
					String tmp = "";
					/*
					if(daoMethod.getParameterTypes().equals("String")){
						tmp = "\""+paramName+"\"";
					} else{
						tmp = paramName;
					}
					*/
					tmp = modelLower+".get"+UtilUnit.firstUpper(paramName)+"()";
					
					paramFull += tmp;
					i++;
						
				}
			}
			
			String contentTemp = "";
			
			if(methodName.startsWith(countNaming)){
				contentTemp = "int result = ";
			} else if(methodName.startsWith(getNaming)){
				contentTemp = modelUpper+" result = ";
			} else if(methodName.startsWith(listNaming)){
				contentTemp = "List<"+modelUpper+"> result = ";
			}
			
			methodContent.append("\t\t"+contentTemp+serviceName+"."+methodName+"("+paramFull+");\n");
			
			if(!methodName.equals(sqlNameCreate)){
				
				if(methodName.startsWith(sqlNameUpdate)){
					//methodContent.append("\t\t"+modelUpper+" result = "+serviceName+".get(pk);\n");
					methodContent.append("\n\t\tassertTrue(true);");
					
				} else if(methodName.startsWith(sqlNameDelete)){
					//methodContent.append("\t\t"+modelUpper+" result = "+serviceName+".get(pk);\n");
					methodContent.append("\n\t\tassertTrue(true);");
						
				} else if(methodName.startsWith(listNaming)){
					methodContent.append("\n\t\tassertTrue(result.size() > 0);");
					
				} else {
					methodContent.append("\n\t\tassertNotNull(result);");
				}
			}
			
			method.setContents(methodContent.toString());
			
			methodList.add(method);
			
		}
		
		
		testServiceClass.setMethodList(methodList);
		
		//service test 파일 생성
		UtilUnit.makeJavaFile(fileUrl, testServiceClass);
		
		System.out.println("-- Make ServiceTest OK");
		
	}
	
	
}
